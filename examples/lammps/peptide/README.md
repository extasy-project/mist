# peptide

Two LAMMPS input files are provided, one which uses the native verlet NVE integrator, and
one which uses `run_style mist` to select the MIST library verlet integrator

Requires LAMMPS compiled with the MOLECULE, RIGID and KSPACE packages

To run:

```
/path/to/lmp_executable < in.peptide
```

or:


```
/path/to/lmp_executable < in.peptide.mist
```

