# LJ benchmark

Two LAMMPS input files are provided, one which uses the native verlet NVE integrator, and
one which uses `run_style mist` to select the MIST library verlet integrator

To run:

```
/path/to/lmp_executable < in.lj
```

or:


```
/path/to/lmp_executable < in.lj.mist
```

