# Adenosine Diphosphate (ADP) Example, originally from Charlie Matthews
* `adp.config` - original NAMD-Lite configurations file.
* `adp.mist.config` - NAMD-Lite configurations file modified to use MIST.
* `adp.params` - a CHARMM forcefield parameter file.
* `adp.pdb` - a Protein Data Bank (PDB) coordinates file.
* `adp.psf` - a Protein Structure File.
* `mist.params` - MIST parameters file.
