# Water

In the mdp-file you will need to set the integrator (mist or md-vv), steps, how often 
you want output (nstxout, etc).

To run this use case you will first need to generate the tpr-file (the following
assumes that the GROMACS executables are in your search path):

```
gmx grompp  -f water.mdp -p water.top -c water.gro -o water.tpr
```

To run the actual simulation use:

```
gmx  mdrun -ntmpi 1 -ntomp X -s water.tpr -mp water.top
```

where X is the number of OpenMP threads you want to be used to run this simulation.

