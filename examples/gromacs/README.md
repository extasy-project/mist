To run each example, assumming that the GROMACS executables are in your search path, run:

```
grompp -f grompp.mdp -p topol.top -c input.gro -o mine.tpr
```
and then:
```
mdrun -s mine.tpr -mp topol.top
```