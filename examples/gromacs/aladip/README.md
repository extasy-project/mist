# Alanine Dipeptide Example - small system, 1 ps of MD
Assuming that you have the Gromacs executables in your path you first need to run:
```
gmx grompp -f grompp.mdp -p topol.top -c input.gro -o run.tpr
```
To run:
```
gmx mdrun -nt 1 -s run.tpr -mp topol.top
```
* `grompp.mdp` - Gromacs MD parameters file, from Gianpaolo Gobbo.
* `grompp.mist.mdp` - As above, modified to use MIST rather than Gromacs 'md' integrator by Iain Bethune.
* `input.gro` - Initial coordinates for alanine dipeptide in vacuo.
* `mist.params` - MIST parameters for Langevin dynamics at 300K.
* `topol.top` - alanine dipeptide topology file.
