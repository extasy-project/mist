# Water

How to run the code:
```
pmemd -O -i test.in -o test.out -p test.prmtop -c test.prmcrd -r test.rst -inf test.mdinfo
```

**Note**: To use MIST, there is an additional parameter in the in-file. For MIST off, 
it is `imist = 0`, for MIST on `imist = 1`. It is also necessary to have the 
`mist.params` file in the same directory.

