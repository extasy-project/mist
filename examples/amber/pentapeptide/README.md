# Pentapeptide

## Files in this directory

* `mist.params` - MIST parameters file, specifying the integrator to be used and any parameters.
* `new1nano.in` - input control data for the data.
* `penta.crd` - trajectory file.
* `penta.top` - molecule topology file.

## How to run the code

Once you have patched `pmemd` to use MIST you can run the code using:

```
pmemd -O -i new1nano.in -o amber14.out -p penta.top -c penta.crd -r amber14.rst -inf amber14.mdinfo
```

where:

* `-O` - overwrites files if they already exist.
* `-i` - input control file.
* `-p` - input molecular topology, force field, periodic box type, atom and residue names.
* `-c` - input initial coordintes and (optionally) velocities and periodic box size.
* `-r` - output final coordinates, velocity and box dimensions if any- for restarting a run.
* `-inf` - output latest mdout-format info.

The files in this directory will only work with a MIST patched version of
`pmemd` as there is an additional parameter included in the `new1nano.in`
file that will not be understoon by the standard `pmemd` application. To
switch MIST off, use `imist = 0`, to have MIST on use `imist =
1`. It is also necessary to have the `mist.params` file in this directory.

Note that if you are trying to use reproducible runs you will need to remove the `amber14.rst` 
restart file otherwise it will use that as the next set of initial conditions.

