/*******************************************************************************
*                                                                              *
*             MIST - Molecular Integration Simulation Toolkit                  *
*         A library of integration algorithms for Molecular Dynamics           *
*                                                                              *
*            Copyright (c) 2014-2020, The University of Edinburgh              *
*                            All rights reserved.                              *
*                     List of contributors in AUTHORS                          *
*                                                                              *
*            MIST is free software.  See LICENSE for details                   *
*                                                                              *
*******************************************************************************/

/**
 * @file AmberSystem.h
 * @brief Declaration of specific system class for Amber
 */

#ifndef MIST_AMBER_SYSTEM_H
#define MIST_AMBER_SYSTEM_H

#include "System.h"

/**
 * @brief A System class which provides accessors for particle data inside the
 * Amber code
 *
 * This is a specialisation of the System interface which is specific to the
 * Amber code.  It provides access to an Integrator to the particle data stored
 * in various arrays within Amber, and also the ability to compute updates
 * forces, calling Amber force evaluation function by means of a callback.
 *
 */

class AmberSystem : public System
{
  public:
    /**
     * @brief Default Constructor
     *
     * Sets the Amber-specific unit conversion factors:
     * - Energy in kJ/mol
     * - Time in ps
     * - Mass in AMU
     * - Length in Angstrom
     *
     */
    AmberSystem();
    /**
     * @brief Default Destructor
     *
     * Does nothing
     */
    ~AmberSystem();

    /**
     * @brief Setter for the position pointer
     *
     * Amber-specific implementation which stores a pointer to the particle,
     * position data and also the list of local particles.
     *
     * @param p pointer to position data
     */
    void SetPositionPtr(void *p);

    /**
     * @brief Setter for the cell pointer
     *
     * Amber-specific implementation which unpacks the provided data structure,
     * and stores individual pointers
     *
     * @param c pointer to cell data
     */
    void SetCellPtr(void *c);

    // Interface to Integrators
    Vector3 GetVelocity(int i) const;
    void SetVelocity(int i, Vector3 v);
    void SetMassPtr(void *m, int req);
    Vector3 GetPosition(int i) const;
    void SetPosition(int i, Vector3 p);
    void GetCell(Vector3 *a, Vector3 *b, Vector3 *c);
    void GetCellInverse(Vector3 *a, Vector3 *b, Vector3 *c);
    void SetCell(Vector3 a, Vector3 b, Vector3 c);

    Vector3 GetForce(int i) const;
    Vector3 GetForce(int i, force_component_t t) const;
    double GetMass(int i) const;
    double GetInverseMass(int i) const;
    double GetInverseSqrtMass(int i) const;
    double GetPressure() const;
    double GetPotentialEnergy() const;
    double GetPotentialEnergy(force_component_t t) const;
    char *GetKind(int i) const;

    int *local_atoms;                   /**< @brief List of local atoms */
    double *ucell;                      /**< @brief Lattice vectors */
    double *recip;                      /**< @brief Reciprocal lattic vectors */
    void (*update_cell_metadata)(void); /**< @brief Callback to update Amber
                                           internal state after cell change */
};

#endif
