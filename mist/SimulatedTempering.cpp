/*******************************************************************************
*                                                                              *
*             MIST - Molecular Integration Simulation Toolkit                  *
*         A library of integration algorithms for Molecular Dynamics           *
*                                                                              *
*            Copyright (c) 2014-2020, The University of Edinburgh              *
*                            All rights reserved.                              *
*                     List of contributors in AUTHORS                          *
*                                                                              *
*            MIST is free software.  See LICENSE for details                   *
*                                                                              *
*******************************************************************************/

/*
 * @file SimulatedTempering.cpp
 * @brief Implementation of Simulated Tempering Integrator
 */

#include "SimulatedTempering.h"
#include "ConstraintSolver.h"
#include "IO.h"
#include "System.h"

#include <math.h>
#include <stdlib.h>

SimulatedTempering::SimulatedTempering(Param *p)
    : LangevinIntegrator(p, "Simulated Tempering")
{
    // Default values for input parameters
    period = 1000;
    n_temperatures = 1;

    int err, i;

    // Read in parameters from mist.params file
    ReadIntParam(p, &period, "period", "");

    ParamArrayPackage *arr;
    err = getParamArray(p, "temperatures", &arr);
    if (err == ERR_INPUTVALUE || err == ERR_ARRAY)
    {
        temperatures = (double *)malloc(sizeof(double));
        avg_pe = (double *)malloc(sizeof(double));
        weights = (double *)malloc(sizeof(double));
        n_steps = (int *)malloc(sizeof(int));
        n_temperatures = 1;
        temperatures[0] = temp;
        avg_pe[0] = 0.0;
        n_steps[0] = 0;

        if (err == ERR_INPUTVALUE)
        {
            IO::Warning("No value provided for temperatures parameter, "
                        "defaulting to a single temperature of: %lf K\n",
                        temp);
        }
        if (err == ERR_ARRAY)
        {
            IO::Error("The temperatures array was malformed, "
                      "defaulting to a single temperature of: %lf K\n",
                      temp);
        }
    }
    else if (err == ERR_OK)
    {
        double *t = getArrayDoubleValues(arr);
        n_temperatures = arr->size;
        temperatures = (double *)malloc(n_temperatures * sizeof(double));
        avg_pe = (double *)malloc(n_temperatures * sizeof(double));
        weights = (double *)malloc(n_temperatures * sizeof(double));
        n_steps = (int *)malloc(n_temperatures * sizeof(int));

        IO::Info("Temperature states:\n");
        for (i = 0; i < n_temperatures; i++)
        {
            temperatures[i] = t[i];
            avg_pe[i] = 0.0;
            n_steps[i] = 0;
            IO::Info("%lf K\n", temperatures[i]);
        }
    }
    else
    {
        IO::Error(
            "Unknown error for getParamArray(), called for temperatures\n");
    }

    freeParamArray(arr);

    // Check which state we start in

    state = 0;
    weights[state] = 0.0;
    if (temp != temperatures[state])
    {
        IO::Warning("Selected temperature of %lf K does not match the first state!  "
                    "Will start simulation at %lf K.\n",
                    temp, temperatures[state]);
        temp = temperatures[state];
    }

    if (method != LANGEVIN_METHOD_BAOAB)
    {
        IO::Warning("Selected splitting method is not implemented - defaulting "
                    "to BAOAB!\n");
        method = LANGEVIN_METHOD_BAOAB;
    }

    // We need the potential energy
    features |= MIST_FEATURE_REQUIRES_ENERGY_WITH_FORCES;

    step = 0;
    rank = 0;
}

SimulatedTempering::~SimulatedTempering()
{
    if (rank == 0)
    {
        outfile.close();
    }
    free(avg_pe);
    free(temperatures);
    free(weights);
}

void SimulatedTempering::Step(double dt)
{

    if (step == 0)
    {
#ifdef __MIST_WITH_MPI
        MPI_Comm_rank(system->GetCommunicator(), &rank);
#endif
        if (rank == 0)
        {
            // Open file for writing weights and energy averages
            outfile.open("tempering.out");
        }
    }
    // Langevin BAOAB scheme: http://dx.doi.org/10.1063/1.4802990
    // Simulated Tempering with on-the-fly weights:
    // https://dx.doi.org/10.1063/1.4792046

    // Velocity half-step (B)
    VelocityStep(0.5 * dt);
    ResolveVelocityConstraints(0.5 * dt);

    // Position half step (A)
    PositionStep(0.5 * dt);
    ResolvePositionConstraints(0.5 * dt);
    ResolveVelocityConstraints(0.5 * dt);

    // Velocity update (O)
    OStep(dt);
    ResolveVelocityConstraints(dt);

    // Position half step (A)
    PositionStep(0.5 * dt);
    ResolvePositionConstraints(0.5 * dt);
    ResolveVelocityConstraints(0.5 * dt);

    system->UpdateForces();

    // Velocity half-step (B)
    VelocityStep(0.5 * dt);
    ResolveVelocityConstraints(0.5 * dt);

    step++;

    // End-of-step: accumulate the average PE in the current state
    n_steps[state]++;
    double pe = system->GetPotentialEnergy();
    avg_pe[state] += (pe - avg_pe[state]) / n_steps[state];

    if (step % period == 0)
    {
        // Update the weights
        int i;
        for (i = 1; i < n_temperatures; i++)
        {
            if (n_steps[i] != 0)
            {
                // Weighting based on avg PE observed in each state
                weights[i] =
                    weights[i - 1] +
                    (1.0 / (system->Boltzmann() * temperatures[i]) -
                     1.0 / (system->Boltzmann() * temperatures[i - 1])) *
                        (avg_pe[i] + avg_pe[i - 1]) * 0.5;
            }
            else
            {
                // If a state has not been visited, assume it has the same PE as
                // preceding state to increase transition probability (see
                // http://dx.doi.org/10.1021/acs.jpcb.5b03381 )
                weights[i] =
                    weights[i - 1] +
                    (1.0 / (system->Boltzmann() * temperatures[i]) -
                     1.0 / (system->Boltzmann() * temperatures[i - 1])) *
                        avg_pe[i - 1];
            }
        }

        // Master does the output
        if (rank == 0)
        {
            outfile << step << " " << temp << " ";
            for (i = 0; i < n_temperatures; i++)
            {
                outfile << weights[i] << " ";
            }

            for (i = 0; i < n_temperatures; i++)
            {
                outfile << avg_pe[i] << " ";
            }

            outfile << std::endl;
        }

        // Decide which direction to attempt a shift
        double x = r[0]->random_uniform();
        int shift = 0;
        if (x >= 0.5 && state + 1 < n_temperatures)
        {
            // Shift up a temp, if possible
            shift = 1;
        }
        if (x < 0.5 && state > 0)
        {
            // Shift down a temp, if possible
            shift = -1;
        }

#ifdef __MIST_WITH_MPI
        // Master decides which direction to shift
        MPI_Bcast(&shift, 1, MPI_INT, 0, system->GetCommunicator());
#endif

        if (shift != 0)
        {
            // Compute the probability of the shift
            double p = exp(
                (1.0 / (system->Boltzmann() * temperatures[state]) -
                 (1.0 / (system->Boltzmann() * temperatures[state + shift]))) *
                    pe -
                (weights[state] - weights[state + shift]));

            x = r[0]->random_uniform() - p;
#ifdef __MIST_WITH_MPI
            // Master decides if we transition or not
            MPI_Bcast(&x, 1, MPI_DOUBLE, 0, system->GetCommunicator());
#endif
            if (x < 0.0)
            {
                state += shift;
                temp = temperatures[state];
                kbt = temp * system->Boltzmann();

                // Rescale velocities to new temperature
                Vector3 v;
                int n = system->GetNumParticles();
                double scale = sqrt(temp / temperatures[state - shift]);

#pragma omp parallel for default(none) private(v) shared(n, scale)
                for (int i = 0; i < n; i++)
                {
                    v = system->GetVelocity(i);
                    system->SetVelocity(i, Vector3::Scale(scale, v));
                }

                IO::Info("Simulated Tempering: switched to %lf K\n",
                         temperatures[state]);
            }
        }
    }
}
