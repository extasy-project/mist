/*******************************************************************************
*                                                                              *
*             MIST - Molecular Integration Simulation Toolkit                  *
*         A library of integration algorithms for Molecular Dynamics           *
*                                                                              *
*            Copyright (c) 2014-2020, The University of Edinburgh              *
*                            All rights reserved.                              *
*                     List of contributors in AUTHORS                          *
*                                                                              *
*            MIST is free software.  See LICENSE for details                   *
*                                                                              *
*******************************************************************************/

/**
 * @file NoseHooverIntegrator.h
 * @brief Declaration of a NoseHoover Integrator class
 */

#ifndef MIST_NOSEHOOVERINTEGRATOR_H
#define MIST_NOSEHOOVERINTEGRATOR_H

#include "Integrator.h"

/**
 * @brief Declaration of the NoseHoover Integrator class
 *
 * An class which uses the Velocity Verlet algorithm, with a
 * Nose-Hoover chain thermostat.
 *
 */
class NoseHooverIntegrator : public Integrator
{
  public:
    /**
     * @brief Default Constructor
     *
     * After superclass constructor, simply prints a message.
     *
     * @param p a pointer to the parameters
     */
    NoseHooverIntegrator(Param *p);

    /**
     * @brief Default Destructor
     *
     * Does nothing
     */
    virtual ~NoseHooverIntegrator();

    /**
     * @brief Integrate positions and velocities over time dt
     *
     * Computes a half-step update of the thermostat, a
     * half-step update of velocities, a full-step update of
     * positions, updates forces and then a further half-step
     * update of velocities and half-step update of the thermostat.
     *
     * @param dt the timestep in internal units
     */
    void Step(double dt);

  protected:
    /**
     * @brief Integrate thermostats over a time dt
     *
     * Updates positions, velocities and accelerations of the
     * thermostats, and rescales particle velocities accordingly
     *
     * @param dt the timestep in internal units
     */
    void Thermostat(double dt);

    /**
     * @brief Integrate barostats NHC over a time dt
     *
     * Updates positions, velocities and accelerations of the
     * barostats chains, and rescales barostat velocity
     * accordingly
     *
     * @param dt the timestep in internal units
     */
    void BarostatNHC(double dt);

    /**
     * @brief Integrate the barostat velocity over a time dt
     *
     * Updates velocity of the barostat, based on the current
     * computed pressure
     *
     * @param dt the timestep in internal units
     */
    void BarostatVelocityStep(double dt);

    /**
     * @brief Rescales particle velocities due to the barostat
     *
     * Updates velocities of the particles, based on the current
     * barostat velocity
     *
     * @param dt the timestep in internal units
     */
    void BarostatRescaleVelocities(double dt);

    /**
     * @brief Isotropic rescaling of the cell
     *
     * Rescales the cell by a constant factor
     *
     * @param scale the scaling factor
     */
    void RescaleCell(double scale);

    double kbt;         /**< @brief Boltzmann Temperature factor */
    int dof;            /**< @brief Number of degrees of freedom */
    bool first_time;    /**< @brief Flag set after initialisation */
    double *temp_xi;         /**< @brief Positions of the thermostat chain */
    double *temp_xi_vel;     /**< @brief Velocities of the thermostat chain */
    double *temp_xi_acc;     /**< @brief Accelerations of the thermostat chain */
    double *temp_xi_mass;    /**< @brief Masses of the thermostat chain */
    double temp_target; /**< @brief Temperature target (in K) */
    int temp_chain_length; /**< @brief Length of thermostat N-H chain */
    int temp_nsteps_relax; /**< @brief Characteristic timescale for thermostat to relax */
    double *press_xi;         /**< @brief Positions of the barostat chain */
    double *press_xi_vel;     /**< @brief Velocities of the barostat chain */
    double *press_xi_acc;     /**< @brief Accelerations of the barostat chain */
    double *press_xi_mass;    /**< @brief Masses of the barostat chain */
    double press_target; /**< @brief Pressure target (in Atmospheres) */
    double eps_v; /**< @brief Barostat velocity **/
    double eps_acc; /**< @brief Barostat acceleration **/
    double eps_mass; /**< @brief Barostat mass **/
    int press_chain_length; /**< @brief Length of barostat N-H chain */
    int press_nsteps_relax; /**< @brief Characteristic timescale for barostat to relax */
    double to_pressure; /**< @brief Scaling factor to convert energy/volume to pressure units */
    bool box_quench; /**< @brief If box quench is enabled */
};

#endif
