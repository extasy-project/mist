/*******************************************************************************
*                                                                              *
*             MIST - Molecular Integration Simulation Toolkit                  *
*         A library of integration algorithms for Molecular Dynamics           *
*                                                                              *
*            Copyright (c) 2014-2020, The University of Edinburgh              *
*                            All rights reserved.                              *
*                     List of contributors in AUTHORS                          *
*                                                                              *
*            MIST is free software.  See LICENSE for details                   *
*                                                                              *
*******************************************************************************/

/**
 * @file ISST.cpp
 * @brief Implementation of the ISST integrator
 */

#include "ISST.h"
#include "ConstraintSolver.h"
#include "IO.h"
#include "System.h"
#include "legendre_rule_fast.h"
#include "strcasecmp.h"
#include "time.h"
#include <strings.h>

#include <math.h>
#ifdef _OPENMP
#include "omp.h"
#endif

/**
 * After superclass constructor, sets params based on user input
 * or defaults, and initialises the RNG
 */
ISST::ISST(Param *p)
    : LangevinIntegrator(p, "Infinite Switch Simulated Tempering (ISST)")
{
    features |= MIST_FEATURE_REQUIRES_ENERGY_WITH_FORCES;

    // Default values
    temp_min = 300.0;
    temp_max = 400.0;
    n_interpolation = 15;
    tau = 1.0;
    period = 1000;

    force_rescaling = 1.0;
    step = 0;
    beta_min = 0.0;
    beta_max = 0.0;

    gauss_weight = NULL;
    beta = NULL;
    beta_weight = NULL;
    partition_estimate = NULL;

    // Read in parameters from mist.params file
    ReadDoubleParam(p, &temp_min, "temperature_min", "K");
    ReadDoubleParam(p, &temp_max, "temperature_max", "K");
    ReadIntParam(p, (int *)&n_interpolation, "npts_interpolation", "");
    ReadDoubleParam(p, &tau, "learn_scaling", "ps^-1");
    ReadIntParam(p, &period, "period", "");

    massrequirements |= MIST_MASS_REQD;
}

/*
 * Close the output file and clean up arrays
 */
ISST::~ISST()
{
    if (gauss_weight != NULL)
    {
        delete[] gauss_weight;
        gauss_weight = NULL;
    }

    if (beta != NULL)
    {
        delete[] beta;
        beta = NULL;
    }

    if (beta_weight != NULL)
    {
        delete[] beta_weight;
        beta_weight = NULL;
    }

    if (partition_estimate != NULL)
    {
        delete[] partition_estimate;
        partition_estimate = NULL;
    }
    // if rank is zero
    outfile.close();
}

/**
 * Updates the beta weights and updates the force rescaling factor, which is
 * then used in the velocity update
 */
void ISST::ApplyForceRescaling(const double &dt)
{
    // update the weights
    UpdateBetaWeights(dt);

    // update the force rescaling factor
    UpdateForceRescaling();
}

/**
 * Normalise the beta weights
 */
void ISST::NormaliseBetaWeights()
{
    // weight normalisation
    double weight_norm = 0.0;

    for (unsigned i = 0; i < n_interpolation; i++)
        weight_norm += gauss_weight[i] * beta_weight[i];

    for (unsigned i = 0; i < n_interpolation; i++)
        beta_weight[i] /= weight_norm;
}

/**
 * Initialises variables, including the legendre polynomials and
 * sets the initial weights
 */
void ISST::SetSystem(System *s)
{
    LangevinIntegrator::SetSystem(s);

    beta_max = 1.0 / (temp_min * system->Boltzmann());
    beta_min = 1.0 / (temp_max * system->Boltzmann());

    // get the legendre polynominals basis for the interpolation points
    gauss_weight = new double[n_interpolation];
    beta = new double[n_interpolation];
    partition_estimate = new AverageObservable[n_interpolation];

    legendre_compute_glr(n_interpolation, beta, gauss_weight);
    rescale(beta_min, beta_max, n_interpolation, beta, gauss_weight);

    // set the initial weights
    beta_weight = new double[n_interpolation];

    for (unsigned i = 0; i < n_interpolation; i++)
        beta_weight[i] = 1.0;

    NormaliseBetaWeights();
}

/**
 * Infinite Switch Simulated Tempering algorithm
 * for details see https://arxiv.org/abs/1809.05066
 */
void ISST::Step(double dt)
{
    if (step == 0)
        // if(rank == 0)
        outfile.open("isst.out");

    if (method == LANGEVIN_METHOD_BAOAB)
    {
        // Velocity half-step (B)
        VelocityStep(0.5 * dt);
        ResolveVelocityConstraints(0.5 * dt);

        // Position half step (A)
        PositionStep(0.5 * dt);
        ResolvePositionConstraints(0.5 * dt);
        ResolveVelocityConstraints(0.5 * dt);

        // Velocity update (O)
        OStep(dt);
        ResolveVelocityConstraints(dt);

        // Position half step (A)
        PositionStep(0.5 * dt);
        ResolvePositionConstraints(0.5 * dt);
        ResolveVelocityConstraints(0.5 * dt);

        system->UpdateForces();
        ApplyForceRescaling(dt);

        // Velocity half-step (B)
        VelocityStep(0.5 * dt);
        ResolveVelocityConstraints(0.5 * dt);
    }
    else if (method == LANGEVIN_METHOD_ABOBA)
    {
        // Position half step (A)
        PositionStep(0.5 * dt);
        ResolvePositionConstraints(0.5 * dt);
        ResolveVelocityConstraints(0.5 * dt);

        system->UpdateForces();
        ApplyForceRescaling(dt);

        // Velocity half-step (B)
        VelocityStep(0.5 * dt);
        ResolveVelocityConstraints(0.5 * dt);

        // Velocity update (O)
        OStep(dt);
        ResolveVelocityConstraints(dt);

        // Velocity half-step (B)
        VelocityStep(0.5 * dt);
        ResolveVelocityConstraints(0.5 * dt);

        // Position half step (A)
        PositionStep(0.5 * dt);
        ResolvePositionConstraints(0.5 * dt);
        ResolveVelocityConstraints(0.5 * dt);
    }

    // write into weight file
    if (step % period == 0)
        // if(rank == 0)
        WriteObservableWeights();

    step++;
}

/**
 * Write all the observable weights to file for calculating later
 */
void ISST::WriteObservableWeights()
{
    // calculate the weights and write to array
    System *s = system;
    double V = s->GetPotentialEnergy();
    double *weight = new double[n_interpolation];

#pragma omp parallel for default(none) shared(V, weight)
    for (int i = 0; i < n_interpolation; i++)
    {
        double z_i = partition_estimate[i].average;
        double b_i = beta[i];
        double integral = 0.0;

        for (unsigned j = 0; j < n_interpolation; j++)
        {
            double w_j = beta_weight[j];
            double b_j = beta[j];
            double g_j = gauss_weight[j];

            integral += g_j * w_j * exp(-(b_j - b_i) * V);
        }

        weight[i] = 1.0 / (z_i * integral);
    }

    // print step to file
    outfile << step << " ";
    for (unsigned i = 0; i < n_interpolation; i++)
        outfile << weight[i] << " ";

    // end line
    outfile << std::endl;

    delete[] weight;
}

/**
 * Updates the beta weights
 */
void ISST::UpdateBetaWeights(const double &dt)
{
    // dereference the learning timestep
    double h = tau * dt;
    double V = system->GetPotentialEnergy();
    double beta_bar_denum_integral = 0.0;

    // calculate the denumerator of the beta_bar integral
    for (unsigned i = 0; i < n_interpolation; i++)
    {
        double b_i = beta[i];
        double w_i = beta_weight[i];
        double g_i = gauss_weight[i];

        beta_bar_denum_integral += g_i * w_i * exp(-b_i * V);
    }

    // calculate the partiion estimate and update the weights
    for (unsigned i = 0; i < n_interpolation; i++)
    {
        double b_i = beta[i];

        partition_estimate[i].add_observation(exp(-b_i * V) /
                                              beta_bar_denum_integral);

        double w_in = beta_weight[i];
        double w_inp1 = 0.0;
        double z_in = partition_estimate[i].average;

        w_inp1 = (1.0 - h) * w_in + h / z_in;
        beta_weight[i] = w_inp1;
    }

    // make sure that the weights are normalised
    NormaliseBetaWeights();
}

/**
 * Updates the force rescaling
 */
void ISST::UpdateForceRescaling()
{
    force_rescaling = 0.0;
    double V = system->GetPotentialEnergy();

    double beta_bar_num_integral = 0.0;
    double beta_bar_denum_integral = 0.0;

    for (unsigned i = 0; i < n_interpolation; i++)
    {
        double b_i = beta[i];
        double w_i = beta_weight[i];
        double g_i = gauss_weight[i];

        beta_bar_num_integral += g_i * b_i * w_i * exp(-b_i * V);
        beta_bar_denum_integral += g_i * w_i * exp(-b_i * V);
    }

    force_rescaling = kbt * beta_bar_num_integral / beta_bar_denum_integral;
}

/**
 * Overrides VelocityStep() from Integrator to include a force rescaling:
 * v(i) = v(i) + force_rescaling * F(i) / m(i) * dt. Uses OpenMP to
 * update the velocities if this is available.
 * @param[in] dt The integration time step.
 */
void ISST::VelocityStep(double dt)
{
    Vector3 v, f;
    double one_over_m;
    System *s = system;
    int n = s->GetNumParticles();

#pragma omp parallel for default(none) private(v, f, one_over_m)               \
    shared(s, dt, n)
    for (int i = 0; i < n; i++)
    {
        one_over_m = s->GetInverseMass(i);
        v = s->GetVelocity(i);
        f = s->GetForce(i);
        // v = v + scaling factor * dt / m * f
        v = v + Vector3::Scale(force_rescaling * dt * one_over_m, f);
        s->SetVelocity(i, v);
    }
}
