/*******************************************************************************
*                                                                              *
*             MIST - Molecular Integration Simulation Toolkit                  *
*         A library of integration algorithms for Molecular Dynamics           *
*                                                                              *
*            Copyright (c) 2014-2020, The University of Edinburgh              *
*                            All rights reserved.                              *
*                     List of contributors in AUTHORS                          *
*                                                                              *
*            MIST is free software.  See LICENSE for details                   *
*                                                                              *
*******************************************************************************/

/**
 * @file strcasecmp.h
 * @brief A compatibility wrapper for strcasecmp on Windows
 *
 * strcasecmp is provided by POSIX. On Windows, we wrap a Win32 function.
 * Workaround provided by Neil Spenley
 */

#ifndef MIST_STRCASECMP_H
#define MIST_STRCASECMP_H

#ifdef _WIN32
#include "windows.h"

inline int strcasecmp(char *string1, char *string2)
{
    return _stricmp(string1, string2);
}

#endif

#endif
