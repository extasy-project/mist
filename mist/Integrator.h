/*******************************************************************************
*                                                                              *
*             MIST - Molecular Integration Simulation Toolkit                  *
*         A library of integration algorithms for Molecular Dynamics           *
*                                                                              *
*            Copyright (c) 2014-2020, The University of Edinburgh              *
*                            All rights reserved.                              *
*                     List of contributors in AUTHORS                          *
*                                                                              *
*            MIST is free software.  See LICENSE for details                   *
*                                                                              *
*******************************************************************************/

/**
 * @file Integrator.h
 * @brief Declaration of general integrator base class (an abstract class)
 */

#ifndef MIST_INTEGRATOR_H
#define MIST_INTEGRATOR_H

/**
 * @defgroup MASS_REQUIREMENTS_FLAGS MIST integrator mass requirements flags
 *
 * @brief combinations of these flags may be returned by GetMassRequirements()
 *
 * MIST Integrators make use of particle masses, or derived properties
 * which can be precomputed once at the beginning of the simulation, when
 * the MD code calls MIST_SetMassPtr().
 * Depending on the flags set in the integrator's constructor, the
 * System::SetMassPtr() function will do any computation that is required.
 * @{
 */

/** @brief Integrator does not require masses */
#define MIST_NOMASS_REQD 0
/** @brief Integrator requires particle masses */
#define MIST_MASS_REQD 1
/** @brief Integrator requires particle inverse masses */
#define MIST_INV_MASS_REQD 2
/** @brief Integrator requires particle inverse square-root masses */
#define MIST_INVSQRT_MASS_REQD 4
/** @} */

extern "C"
{
#include "params.h"
}

#include "mist.h"

class ConstraintSolver;
class System;

/**
 * @brief Declaration of the Integrator abstract base class, which it the
 * all other integrators inherit from.
 *
 * This is the interface that all integrators must implement. It provides an
 * abstract Step() function which will be called by the host application to
 * integrate the system forward in time.
 *
 * Several convenience functions are also provided that are commonly needed by
 * subclasses.
 */
class Integrator
{
  public:
    /**
     * @brief Default Constructor
     *
     * @param p a pointer to the parameters
     * @param name the display name of the integrator
     */
    Integrator(Param *p, const char *name);

    /**
     * @brief Default Destructor
     */
    virtual ~Integrator();

    /** @brief Computes an integration step from t to t+dt
     *
     * This function must be implemented by any subclasses, and computes a
     * single integration step from the current time (t) to (t+dt).
     *
     * @param dt the timestep (in internal units)
     */
    virtual void Step(double dt) = 0;

    /**
     * @brief Setter for the system pointer
     *
     * @param s pointer to an instance of the System class
     */
    virtual void SetSystem(System *s);

    /**
     * @brief Getter for the system pointer
     *
     * @return pointer to an instance of the System class
     */
    System *GetSystem() const;

    /**
     * @brief Getter for feature flags
     *
     * @return binary OR'ed combination of feature flags required by the
     * Integrator
     */
    int GetFeatures() const;

    /**
     * @brief Getter for mass required flags
     *
     * @return binary OR'ed combination of masses required by the
     * Integrator
     */
    int GetMassRequirements() const;

    /**
     * @brief Getter for the total number of constraints
     *
     * @return total number of constraints
     */
    int GetNumConstraints() const;

    /**
     * @brief Test if this integrator conserves momentum or not
     *
     * @return TRUE if the integrator does conserve momentum, FALSE if not
     */
    bool ConservesMomentum() const;

    /**
     * @brief Test if this integrator can enforce constraints or not
     *
     * @return TRUE if the integrator does support constraints, FALSE if not
     */
    bool SupportsConstraints() const;

    /**
     * @brief Helper function to read in a double parameter
     *
     * Attempts to read a parameter with the provided key from the
     * data structure.  If present, sets the variable, otherwise
     * returns it unchanged.
     *
     * @param p the parameter data structure
     * @param var the variable to set
     * @param key the name of the parameter to read
     * @param units string with the parameter's units (may be empty)
     */
    static void ReadDoubleParam(Param *p, double *var, const char *key, const char *units);

    /**
     * @brief Helper function to read in a int parameter
     *
     * Attempts to read a parameter with the provided key from the
     * data structure.  If present, sets the variable, otherwise
     * returns it unchanged.
     *
     * @param p the parameter data structure
     * @param var the variable to set
     * @param key the name of the parameter to read
     * @param units string with the parameter's units (may be empty)
     */
    static void ReadIntParam(Param *p, int *var, const char *key, const char *units);

    /**
     * @brief Helper function to read in a boolean parameter
     *
     * Attempts to read a parameter with the provided key from the
     * data structure.  If present, sets the variable, otherwise
     * returns it unchanged.
     *
     * @param p the parameter data structure
     * @param var the variable to set
     * @param key the name of the parameter to read
     */
    static void ReadBoolParam(Param *p, bool *var, const char *key);

  protected:
    /**
     * @brief Integrates all particle positions by dt
     *
     * @param dt the time interval to integrate over
     */
    void PositionStep(double dt);

    /**
     * @brief Integrates all particle velocities by dt
     *
     * @param dt the time interval to integrate over
     */
    virtual void VelocityStep(double dt);

    /**
     * @brief Resolves any position constraints that exist
     *
     */
    void ResolvePositionConstraints(double dt);

    /**
     * @brief Resolves any velocity constraints that exist
     *
     */
    void ResolveVelocityConstraints(double dt);

    Param *params;  /**< @brief Pointer to the user's parameters */
    System *system; /**< @brief Pointer to the System to be integrated */
    ConstraintSolver
        *constraintSolver; /**< @brief Pointer to the constraint solver */
    int features; /**< @brief Binary OR'ed combination of feature flags */
    int massrequirements; /**< @brief Binary OR'ed combination of mass flags */
    bool conservesMomentum; /**< @brief If this integrator conserves momentum */
    bool supportsConstraints; /**< @brief If this integrator supports
                                 constraints */
};

#endif
