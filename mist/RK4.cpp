/*******************************************************************************
*                                                                              *
*             MIST - Molecular Integration Simulation Toolkit                  *
*         A library of integration algorithms for Molecular Dynamics           *
*                                                                              *
*            Copyright (c) 2014-2020, The University of Edinburgh              *
*                            All rights reserved.                              *
*                     List of contributors in AUTHORS                          *
*                                                                              *
*            MIST is free software.  See LICENSE for details                   *
*                                                                              *
*******************************************************************************/

/**
 * @file RK4.cpp
 * @brief Implementation of a Runge-Kutta fourth-order constant energy
 * integrator
 */

#include "RK4.h"
#include "System.h"
#include <stddef.h>

/**
 * After superclass constructor, prints a message.
 * Positions and velocities are integrated from t -> t + dt
 * Storage arrays are nullified
 */
RK4Integrator::RK4Integrator(Param *p) : Integrator(p, "Runge-Kutta 4th Order")
{
    features |= MIST_FEATURE_POS_VEL_NO_OFFSET;

    massrequirements = MIST_INV_MASS_REQD;

    conservesMomentum = true;

    oldPos = NULL;
    oldVel = NULL;
    l1 = NULL;
    l2 = NULL;
    l3 = NULL;
    l4 = NULL;
    k1 = NULL;
    k2 = NULL;
    k3 = NULL;
    k4 = NULL;

    arrLen = 0;
}

/**
 * Cleans up storage arrays if needed
 */
RK4Integrator::~RK4Integrator()
{
    if (oldPos != NULL)
    {
        delete[] oldPos;
        oldPos = NULL;
    }

    if (oldVel != NULL)
    {
        delete[] oldVel;
        oldVel = NULL;
    }

    if (l1 != NULL)
    {
        delete[] l1;
        l1 = NULL;
    }

    if (l2 != NULL)
    {
        delete[] l1;
        l2 = NULL;
    }

    if (l3 != NULL)
    {
        delete[] l1;
        l3 = NULL;
    }

    if (l4 != NULL)
    {
        delete[] l1;
        l4 = NULL;
    }

    if (k1 != NULL)
    {
        delete[] l1;
        k1 = NULL;
    }

    if (k2 != NULL)
    {
        delete[] l1;
        k2 = NULL;
    }

    if (k3 != NULL)
    {
        delete[] l1;
        k3 = NULL;
    }

    if (k4 != NULL)
    {
        delete[] l1;
        k4 = NULL;
    }
}
/**
 * Computes a full-step update
 * following the scheme described in
 * http://physics.bu.edu/~py502/
 */
void RK4Integrator::Step(double dt)
{
    int i, n;
    double m_inv;
    const double one_sixth = 1.0 / 6.0;

    n = system->GetNumParticles();

    // Resize storage arrays if needed

    if (n > arrLen)
    {
        if (oldPos != NULL)
        {
            delete[] oldPos;
        }
        if (oldVel != NULL)
        {
            delete[] oldVel;
        }
        if (l1 != NULL)
        {
            delete[] l1;
        }
        if (l2 != NULL)
        {
            delete[] l2;
        }
        if (l3 != NULL)
        {
            delete[] l3;
        }
        if (l4 != NULL)
        {
            delete[] l4;
        }
        if (k1 != NULL)
        {
            delete[] k1;
        }
        if (k2 != NULL)
        {
            delete[] k2;
        }
        if (k3 != NULL)
        {
            delete[] k3;
        }
        if (k4 != NULL)
        {
            delete[] k4;
        }
        oldPos = new Vector3[n];
        oldVel = new Vector3[n];
        l1 = new Vector3[n];
        l2 = new Vector3[n];
        l3 = new Vector3[n];
        l4 = new Vector3[n];
        k1 = new Vector3[n];
        k2 = new Vector3[n];
        k3 = new Vector3[n];
        k4 = new Vector3[n];

        arrLen = n;
    }

    // Store original Positions and Velocities

    for (i = 0; i < system->GetNumParticles(); i++)
    {
        oldPos[i] = system->GetPosition(i);
        oldVel[i] = system->GetVelocity(i);
    }

    // Compute k1 and l1 based on initial forces

    for (i = 0; i < system->GetNumParticles(); i++)
    {
        m_inv = system->GetInverseMass(i);
        k1[i] = system->GetForce(i) * dt * m_inv;
        l1[i] = oldVel[i] * dt;
        system->SetPosition(i, oldPos[i] + l1[i] * 0.5);
    }

    // Compute k2 and l2 based on first estimate to the forces at t+dt/2

    system->UpdateForces();

    for (i = 0; i < system->GetNumParticles(); i++)
    {
        m_inv = system->GetInverseMass(i);
        k2[i] = system->GetForce(i) * dt * m_inv;
        l2[i] = (oldVel[i] + k1[i] * 0.5) * dt;
        system->SetPosition(i, oldPos[i] + l2[i] * 0.5);
    }

    // Compute k3 and l3 based on second estimate to the forces at t+dt/2

    system->UpdateForces();

    for (i = 0; i < system->GetNumParticles(); i++)
    {
        m_inv = system->GetInverseMass(i);
        k3[i] = system->GetForce(i) * dt * m_inv;
        l3[i] = (oldVel[i] + k2[i] * 0.5) * dt;
        system->SetPosition(i, oldPos[i] + l3[i]);
    }

    // Compute k4 and l4 based on estimate to the forces at t+dt
    // and set final positions and velocities

    system->UpdateForces();

    for (i = 0; i < system->GetNumParticles(); i++)
    {
        m_inv = system->GetInverseMass(i);
        k4[i] = system->GetForce(i) * dt * m_inv;
        l4[i] = (oldVel[i] + k3[i]) * dt;
        system->SetPosition(
            i, oldPos[i] + (l1[i] + l2[i] * 2 + l3[i] * 2 + l4[i]) * one_sixth);
        system->SetVelocity(
            i, oldVel[i] + (k1[i] + k2[i] * 2 + k3[i] * 2 + k4[i]) * one_sixth);
    }

    system->UpdateForces();
}
