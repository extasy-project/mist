/*******************************************************************************
*                                                                              *
*             MIST - Molecular Integration Simulation Toolkit                  *
*         A library of integration algorithms for Molecular Dynamics           *
*                                                                              *
*            Copyright (c) 2014-2020, The University of Edinburgh              *
*                            All rights reserved.                              *
*                     List of contributors in AUTHORS                          *
*                                                                              *
*            MIST is free software.  See LICENSE for details                   *
*                                                                              *
*******************************************************************************/

/**
 * @file RK4.h
 * @brief Declaration of a Runge-Kutta 4th order Integrator class
 */

#ifndef MIST_RK4_H
#define MIST_RK4_H

#include "Integrator.h"
#include "Vector3.h"

/**
 * @brief Declaration of a Runge-Kutta 4th order Integrator class
 *
 * Algorithm based on scheme at http://physics.bu.edu/~py502/
 *
 * Requires 4 force updates per timestep, as well as several intermediate
 *  values to be stored.
 */
class RK4Integrator : public Integrator
{
  public:
    /**
     * @brief Default Constructor
     *
     * @param p a pointer to the parameters
     */
    RK4Integrator(Param *p);

    /**
     * @brief Default Destructor
     */
    virtual ~RK4Integrator();

    /**
     * @brief Integrate positions and velocities over time dt
     *
     * @param dt the timestep in internal units
     */
    void Step(double dt);

  protected:
    Vector3 *oldPos; /**< @brief array of positions at start of step */
    Vector3 *oldVel; /**< @brief array of velocities at start of step */
    Vector3 *l1;     /**< @brief first increment of the position */
    Vector3 *l2;     /**< @brief second increment of the position */
    Vector3 *l3;     /**< @brief third increment of the position */
    Vector3 *l4;     /**< @brief fourth increment of the position */
    Vector3 *k1;     /**< @brief first increment of the velocity */
    Vector3 *k2;     /**< @brief second increment of the velocity */
    Vector3 *k3;     /**< @brief third increment of the velocity */
    Vector3 *k4;     /**< @brief fourth increment of the velocity */
    int arrLen;      /**< @brief number of entries in the temporary arrays */
};

#endif
