/*******************************************************************************
*                                                                              *
*             MIST - Molecular Integration Simulation Toolkit                  *
*         A library of integration algorithms for Molecular Dynamics           *
*                                                                              *
*            Copyright (c) 2014-2020, The University of Edinburgh              *
*                            All rights reserved.                              *
*                     List of contributors in AUTHORS                          *
*                                                                              *
*            MIST is free software.  See LICENSE for details                   *
*                                                                              *
*******************************************************************************/

/**
 * @file ContinuousTempering_TAMD.cpp
 * @brief Implementation of the ContinuousTempering_TAMD Integrator
 * This introduces additional degrees of freedom coupled to the collective
 *variables
 * (here: backbone dihedral angles). The additional degrees of freedom are
 *evolved
 * at the (high) temperature tamd_temp, have (large) masses tamd_mass and are
 *coupled
 * to the system variables via stiff springs with spring constant tamd_kappa.
 *
 * The additional degrees of freedom are then heated with Continuous Tempering.
 *
 * This is a useful method for sampling free energy landscapes. The timeseries
 *of the collective variables and the forces acting on them are written in
 * tamd_out.dat with frequency save_freq. This can be used to reconstruct the
 *free
 * energy. For more information see "Heating and flooding: A unified approach
 *for rapid generation of free energy surfaces", M. Chen et. al., Journal of
 *Chemical Physics
 *
 * currently only 2D free energy surfaces with backbone dihedral angles
 *specified in mist.params are supported.
 *
 * @author RB
 */

#include "ContinuousTempering_TAMD.h"
#include "IO.h"
#include "System.h"

#include <cmath>
#include <fstream>
#include <iomanip>
#include <stdlib.h>

#include <math.h>

#include <Eigen/Dense>
using namespace Eigen;

#define TWO_TIMES_PI 2 * M_PI /**< @brief the constant 2*pi */
#define BOLTZMANN_KJoverMOL                                                    \
    0.0083144621 /**< @brief Conversion constant Kb in KJ/mol */

typedef Matrix<double, 5, 5>
    ParamMatrix; /**< @brief a 5x5 double-precision matrix */
typedef Matrix<double, 5, 1>
    ParamVector; /**< @brief a 5 element double-precision vector */

/**
 * After superclass constructor, sets params based on user input
 * or defaults, and initialises the RNG
 */
ContinuousTempering_TAMD::ContinuousTempering_TAMD(Param *p)
    : LangevinIntegrator(p, "Continuous Tempering + TAMD")
{
    features |= MIST_FEATURE_REQUIRES_ENERGY_WITH_FORCES;

    // Default values
    temp_s = 1500.0;
    fric_s = 10.0;
    m_s_inp = 168.0;
    kappa_inp = 1163.0;
    Tfact = 0.7;
    fric_xi = 10;
    m_xi = 0.004;
    Delta = 50;
    Delta2 = 100;
    hills_height = 0.08;
    sigma = 3.05;
    meta_dep = 100;
    read_bias_file = 0; // this should be transformed into bool
    coupl = 0;
    d_coupl = 0;
    n_approx = 1000;

    // Read in parameters from mist.params file
    ReadDoubleParam(p, &temp_s, "tamd_temp", "K");
    ReadDoubleParam(p, &fric_s, "tamd_friction", "ps^-1");
    ReadDoubleParam(p, &m_s_inp, "tamd_mass", "u*angstrom^2");
    ReadDoubleParam(p, &kappa_inp, "tamd_kappa", "kJ/mol");
    ReadDoubleParam(p, &Tfact, "temp_fact", "");
    ReadDoubleParam(p, &m_xi, "mass_xi", "");
    ReadDoubleParam(p, &fric_xi, "langfriction_xi", "ps^-1");
    ReadDoubleParam(p, &Delta, "delta", "angstrom");
    ReadDoubleParam(p, &Delta2, "delta2", "angstrom");
    ReadDoubleParam(p, &hills_height, "hills-height", "kJ/mol");
    ReadDoubleParam(p, &sigma, "hills-width", "UNITS ??");
    ReadIntParam(p, &meta_dep, "metadyn-pace", "");
    // this must be changed in order to read a bool value in input
    ReadIntParam(p, &read_bias_file, "read-bias-file", "");

    // read atom indices for dihedrals
    ReadIntParam(p, &phi_ind[0], "phi1", "");
    ReadIntParam(p, &phi_ind[1], "phi2", "");
    ReadIntParam(p, &phi_ind[2], "phi3", "");
    ReadIntParam(p, &phi_ind[3], "phi4", "");
    ReadIntParam(p, &psi_ind[0], "psi1", "");
    ReadIntParam(p, &psi_ind[1], "psi2", "");
    ReadIntParam(p, &psi_ind[2], "psi3", "");
    ReadIntParam(p, &psi_ind[3], "psi4", "");

}

ContinuousTempering_TAMD::~ContinuousTempering_TAMD() {}

/**
 * Computes Boltzmann's constant * temperature, based on the host code
 * units system
 */
void ContinuousTempering_TAMD::SetSystem(System *s)
{
    LangevinIntegrator::SetSystem(s);
    // scale masses and spring constant to the right units
    kappa = kappa_inp * system->Boltzmann() / BOLTZMANN_KJoverMOL;
    m_s = m_s_inp * system->AMU() * pow(system->Angstrom(), 2);
    sqrt_ms_inv = sqrt(1.0 / m_s);
    // the mass of the additional dofs needs to have unit [mL^2] to balance the
    // fact that the s variables are dimensionless.
    kbt_s = temp_s * system->Boltzmann();
    // set stuff for tempering variables
    m_xi = m_xi * system->AMU() * pow(system->Angstrom(), 2);
    sqrt_mxi_inv = sqrt(1.0 / m_xi);
    hills_height = hills_height * system->Boltzmann() / BOLTZMANN_KJoverMOL;
}

/**Position step for TAMD variables
 */
void ContinuousTempering_TAMD::TAMDvars_PositionStep(double dt)
{
    // position step for additional dofs.
    s_phi += dt * v_phi;
    s_psi += dt * v_psi;

    // take into account periodicity of angular variables. If outside of
    // [-pi,pi],
    // we push s_phi and s_psi back in
    if (s_phi > M_PI)
        s_phi -= TWO_TIMES_PI;
    if (s_phi < (-1 * M_PI))
        s_phi += TWO_TIMES_PI;
    if (s_psi > M_PI)
        s_psi -= TWO_TIMES_PI;
    if (s_psi < (-1 * M_PI))
        s_psi += TWO_TIMES_PI;
}

/**computes difference between dihedral angles phi1 and phi2 modulo 2*pi.
 */
double ContinuousTempering_TAMD::DihedralDiff(double phi1, double phi2)
{
    if (std::abs(phi1 - phi2) <= M_PI)
    {
        // phi1 and phi2 are both in [-pi,pi], nothing needs to be done
        return kappa * (phi1 - phi2);
    }
    else if (phi1 < phi2)
    {
        // translate phi1 by 2*pi.
        return kappa * (phi1 + TWO_TIMES_PI - phi2);
    }
    else
    {
        // translate phi1 by -2*pi.
        return kappa * (phi1 - TWO_TIMES_PI - phi2);
    }
}

/**
 * Langevin BAOAB Scheme (based on Charlie Matthews' NAMD-lite implementation)
 * for details see http://dx.doi.org/10.1063/1.4802990
 */
void ContinuousTempering_TAMD::Step(double dt)
{
    // TAMD algorithm
    // for details see http://dx.doi.org/10.1063/1.4802990
    double m, s1, s2, c1, c2, d;
    static int iter = 0;
    int i, store_ind;
    Vector3 v;
    static ParamMatrix A;
    static ParamVector b, param_vec;
    static VectorXd vector_s_phi(n_approx);
    static VectorXd vector_s_psi(n_approx);
    static VectorXd vector_force_phi(n_approx);
    static VectorXd vector_force_psi(n_approx);
    if (iter == 0)
    {
        // set BAOAB constants for additional dofs
        g_sdt = fric_s / system->Picosecond() * dt;
        c1_s = exp(-g_sdt);
        c3_s = sqrt(kbt_s * (1 - c1_s * c1_s));
        // set BAOAB constants for xi
        g_xidt = fric_xi / system->Picosecond() * dt;
        c1_xi = exp(-g_xidt);
        c3_xi = sqrt(kbt_s * (1 - c1_xi * c1_xi));
        // initialise additional dof positions and velocities
        // additional dof associated to phi
        s_phi = system->ComputeDihedrals(phi_ind);
        v_phi = 0;
        force_phi = 0;
        // additional dof associated to psi
        s_psi = system->ComputeDihedrals(psi_ind);
        v_psi = 0;
        force_psi = 0;
        // tempering dof
        xi = 0;
        v_xi = 0;
        force_xi = 0;
        // initialize parameter matrix
        A = MatrixXd::Zero(5, 5);
        A(0, 0) = n_approx;
        A(1, 1) = n_approx;
        param_vec = MatrixXd::Zero(5, 1);
    }
    //*** end of variable declarations ***

    //*** beginning of iteration ***
    // Velocity half-step (B)
    // velocity step for system vars
    VelocityStep(0.5 * dt);

    // contribution to velocities due to coupling to additional dofs
    for (i = 0; i < 4; i++)
    {
        // the phi coupling contribution to system velocities
        m = system->GetInverseMass(phi_ind[i]);
        v = system->GetVelocity(phi_ind[i]);
        v = v -
            Vector3::Scale(0.5 * dt * m * force_phi,
                           system->ComputeDihedralsJac(phi_ind, phi_ind[i]));
        system->SetVelocity(phi_ind[i], v);
        // the psi coupling contribution to system velocities
        m = system->GetInverseMass(psi_ind[i]);
        v = system->GetVelocity(psi_ind[i]);
        v = v -
            Vector3::Scale(0.5 * dt * m * force_psi,
                           system->ComputeDihedralsJac(psi_ind, psi_ind[i]));
        system->SetVelocity(psi_ind[i], v);
    }

    // velocity step for additional dofs.
    v_phi += dt * 0.5 / m_s * (1.0 - coupl) * force_phi;
    v_psi += dt * 0.5 / m_s * (1.0 - coupl) * force_psi;

    // velocity step for xi
    v_xi += dt * 0.5 / m_xi * force_xi;

    // Position half step (A)
    PositionStep(0.5 * dt);

    // position step for additional dofs.
    TAMDvars_PositionStep(0.5 * dt);

    // position step for xi variable
    xi += dt * 0.5 * v_xi;

    // Velocity update (O)
    OStep(dt);

    // Velocity update (O) for additional dofs
    v_phi = c1_s * v_phi + sqrt_ms_inv * c3_s * r[0]->random_gaussian();
    v_psi = c1_s * v_psi + sqrt_ms_inv * c3_s * r[0]->random_gaussian();

    // Velocity update (O) for xi
    v_xi = c1_xi * v_xi + sqrt_mxi_inv * c3_xi * r[0]->random_gaussian();

#ifdef __MIST_WITH_MPI
    // In parallel, we just take the master's value i.e. v_phi|psi|xi is equal
    // on all processes
    MPI_Bcast(&v_phi, 1, MPI_DOUBLE, 0, system->GetCommunicator());
    MPI_Bcast(&v_psi, 1, MPI_DOUBLE, 0, system->GetCommunicator());
    MPI_Bcast(&v_xi, 1, MPI_DOUBLE, 0, system->GetCommunicator());
#endif

    // Position half step (A)
    PositionStep(0.5 * dt);

    // position step for additional dofs.
    TAMDvars_PositionStep(0.5 * dt);

    // position step for xi variable
    xi += dt * 0.5 * v_xi;

    // force updates

    system->UpdateForces();

    // compute forces due to coupling, taking periodicy into account
    force_phi = DihedralDiff(system->ComputeDihedrals(phi_ind), s_phi);
    force_psi = DihedralDiff(system->ComputeDihedrals(psi_ind), s_psi);

    // store s and force_s variables for quadratic free energy approximation,
    // this is done in such a way that the last n_approx time steps are always
    // in
    // memory.
    store_ind = iter % n_approx;
    vector_s_phi(store_ind) = s_phi;
    vector_s_psi(store_ind) = s_psi;
    vector_force_phi(store_ind) = force_phi;
    vector_force_psi(store_ind) = force_psi;

    if (iter % n_approx == (n_approx - 1)) // solve quadratic programming
                                           // problem
    {
        // params = SolveQuadProg(vector_s_phi, vector_s_psi, vector_force_phi,
        //  vector_force_psi);
        // assemble b
        b(0) = 2 * vector_force_phi.sum();
        b(1) = 2 * vector_force_psi.sum();
        b(2) = 2 * vector_force_phi.dot(vector_s_phi);
        b(3) = 2 * vector_force_phi.dot(vector_s_psi) +
               vector_force_psi.dot(vector_s_phi);
        b(4) = 2 * vector_force_psi.dot(vector_s_psi);
        // assemble A
        s1 = vector_s_phi.sum();
        s2 = vector_s_psi.sum();
        c1 = vector_s_phi.dot(vector_s_phi);
        c2 = vector_s_psi.dot(vector_s_psi);
        d = vector_s_phi.dot(vector_s_psi);
        A(2, 0) = s1;
        A(3, 0) = s2;
        A(3, 1) = s1;
        A(4, 1) = s2;
        A(2, 2) = c1;
        A(3, 3) = c1 + c2;
        A(4, 4) = c2;
        A(3, 2) = d;
        A(4, 3) = d;
        // solver
        param_vec = A.ldlt().solve(b);
    }

    // compute the force force_xi acting on xi
    SS_coupling(&coupl, &d_coupl, xi, Delta, Delta2, Tfact);
    force_xi = 0;
    static_SSforce(&force_xi, s_phi, s_psi, d_coupl, param_vec(0), param_vec(1),
                   param_vec(2), param_vec(3), param_vec(4));
    // compute metadynamics contribution to f_xi
    if (iter % meta_dep == 0)
        metadyn(&force_xi, xi, 1, Delta, Delta2);
    else
        metadyn(&force_xi, xi, 0, Delta, Delta2);

    // Velocity half-step (B)
    VelocityStep(0.5 * dt);

    // contribution to velocities due to coupling to additional dofs
    for (i = 0; i < 4; i++)
    {
        // the phi coupling contribution to system velocities
        m = system->GetInverseMass(phi_ind[i]);
        v = system->GetVelocity(phi_ind[i]);
        v = v -
            Vector3::Scale(0.5 * dt * m * force_phi,
                           system->ComputeDihedralsJac(phi_ind, phi_ind[i]));
        system->SetVelocity(phi_ind[i], v);
        // the psi coupling contribution to system velocities
        m = system->GetInverseMass(psi_ind[i]);
        v = system->GetVelocity(psi_ind[i]);
        v = v -
            Vector3::Scale(0.5 * dt * m * force_psi,
                           system->ComputeDihedralsJac(psi_ind, psi_ind[i]));
        system->SetVelocity(psi_ind[i], v);
    }

    // velocity step for additional dofs.
    v_phi += dt * 0.5 / m_s * (1.0 - coupl) * force_phi;
    v_psi += dt * 0.5 / m_s * (1.0 - coupl) * force_psi;

    // velocity step for xi
    v_xi += dt * 0.5 / m_xi * force_xi;

    // *** end of time step *** //

    // write force out
    static std::ofstream outfile;
    // File initialization
    if (iter == 0)
        outfile.open("dihedrals.dat");
    // write force to file dihedrals.dat
    if (iter % 10 == 0)
    {
        outfile << force_phi << "\t" << force_psi << "\t" << s_phi << "\t"
                << s_psi << std::endl;
        outfile.flush();
    }
    // if (iter % 100 == 0) write_confxyz(coupl, xi);
    iter += 1;
}

void ContinuousTempering_TAMD::SS_coupling(double *cc, double *d_cc, double s,
                                           double D, double D2, double Tf)
{
    if (std::abs(s) <= D)
    {
        *cc = 0;
        *d_cc = 0;
    }
    else if ((D < std::abs(s)) && (std::abs(s) < D + D2))
    {
        *cc = Tf * (3 * pow((std::abs(s) - D) / D2, 2) -
                    2 * pow((std::abs(s) - D) / D2, 3));
        if (s > 0)
            *d_cc =
                6 * Tf *
                (((std::abs(s) - D) / D2) - pow((std::abs(s) - D) / D2, 2)) /
                D2;
        else
            *d_cc =
                -6 * Tf *
                (((std::abs(s) - D) / D2) - pow((std::abs(s) - D) / D2, 2)) /
                D2;
    }
    else
    {
        *cc = Tf;
        *d_cc = 0;
    }
}

void ContinuousTempering_TAMD::static_SSforce(double *fs, double s_phi,
                                              double s_psi, double d_cc,
                                              double a1, double a2, double h11,
                                              double h12, double h22)
{
    // contrib. due to coupling
    // double pe = system->GetPotentialEnergy(); // read potential energy
    Matrix2d H;
    Vector2d c, s_vec;
    H << h11, h12, h12, h22;
    c << a1, a2;
    s_vec << s_phi, s_psi;
    double A_s = c.dot(s_vec) + 0.5 * s_vec.dot(H * s_vec);
    *fs += d_cc * 0; // V_extended_sys=(V-peoff)-cc*(V-peoff)
}

void ContinuousTempering_TAMD::metadyn(double *fs, double s, bool addgauss,
                                       double D, double D2)
{
#ifdef __MIST_WITH_MPI
    // Do everything on the rank 0 in MPI, then broadcast the result
    int rank;
    MPI_Comm_rank(system->GetCommunicator(), &rank);
    if (rank == 0)
    {
#endif
        // how many gaussians are deposited before saving bias potential to
        // file:
        const int metastride = 100;
        const int NG = 9000;
        static double VG[NG + 2] = {0};
        static int it = 0;
        const double smax = 1.03 * (D + D2);
        const double smin = -smax;
        const double dsg = (smax - smin) / (NG - 1);
        const double swp = smax + dsg;
        const double swm = smin - dsg;
        static bool isfirst = 1;

        // read input bias potential
        if (isfirst && (read_bias_file == 1))
        {
            isfirst = 0;
            std::ifstream biasfile;
            biasfile.open("meta.0");
            int count_lines = 0;
            std::string inputline;
            while (getline(biasfile, inputline))
                count_lines++;
            if ((NG + 2) != count_lines)
            {
                IO::Error("Incompatible number of lines in input bias "
                          "potential file meta.0 "
                          "number of lines must equal the size of the internal "
                          "grid which is %d\n",
                          NG + 2);
                exit(0);
            }
            biasfile.clear();
            biasfile.seekg(0);
            for (int ig = 0; ig < NG + 2; ig++)
            {
                double cc;
                biasfile >> cc >> VG[ig];
            }
            biasfile.close();
        }

        // add gaussian
        if (addgauss)
        {
            it = it + 1;
            for (int ig = 1; ig < NG + 1; ig++)
            {
                VG[ig] =
                    VG[ig] +
                    hills_height * exp(-0.5 * pow((s - (swm + (ig - 1) * dsg)) /
                                                      sigma,
                                                  2)) +
                    // adding the same hill on the symmetric position
                    hills_height *
                        exp(-0.5 *
                            pow((-s - (swm + (ig - 1) * dsg)) / sigma,
                                2));
            }
            // write metadyn bias potential
            if (it % metastride == 0)
            {
                char filename[15];
                sprintf(filename, "meta.%d", (it / metastride));
                std::ofstream biasfile;
                biasfile.open(filename);
                for (int ig = 0; ig < NG + 2; ig++)
                {
                    biasfile << std::setw(15) << swm + (ig - 1) * dsg
                             << std::setw(20) << VG[ig] << std::endl;
                }
                biasfile.close();
            }
        }

        // evaluate bias force
        if ((s > swm) && (s < swp))
        {
            // Bin evaluation
            int ig = (int)ceil((s - swm) / dsg);
            // evaluate force
            if ((ig >= 1) && (ig < NG))
                *fs += -(VG[ig + 1] - VG[ig]) / dsg;
        }
        // confining potential term at the border
        if (std::abs(s) > 1.03 * (D + D2))
        {
            *fs += -(s / std::abs(s)) * 25.0 * kbt *
                   (std::abs(s) - 1.03 * (D + D2)) / D;
        }

#ifdef __MIST_WITH_MPI
    }
    // Broadcast the biasing force computed on rank zero
    MPI_Bcast(fs, 1, MPI_DOUBLE, 0, system->GetCommunicator());
#endif
}

void ContinuousTempering_TAMD::write_confxyz(double cc, double xi)
{

#ifdef __MIST_WITH_MPI
    // Do everything on the rank 0 in MPI
    int rank;
    MPI_Comm_rank(system->GetCommunicator(), &rank);
    if (rank != 0)
        return;
#endif

    static bool isfirst = 1;
    static std::ofstream couplingoutfile;

    // output files initialization
    if (isfirst)
    {
        couplingoutfile.open("coupling.dat");
        isfirst = 0;
    }

    // write out position of xi (=s) and value of coupling function (=cc)
    double pe = system->GetPotentialEnergy(); // read potential energy
    couplingoutfile << xi << "\t" << cc << std::endl;
    couplingoutfile.flush();
}
