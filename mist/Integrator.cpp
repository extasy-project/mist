/*******************************************************************************
*                                                                              *
*             MIST - Molecular Integration Simulation Toolkit                  *
*         A library of integration algorithms for Molecular Dynamics           *
*                                                                              *
*            Copyright (c) 2014-2020, The University of Edinburgh              *
*                            All rights reserved.                              *
*                     List of contributors in AUTHORS                          *
*                                                                              *
*            MIST is free software.  See LICENSE for details                   *
*                                                                              *
*******************************************************************************/

/**
 * @file Integrator.cpp
 * @brief Implementation of general integrator base class, contains
 * functionality that may be used by any subclass.
 */

#include "Integrator.h"
#include "ConstraintSolver.h"
#include "IO.h"
#include "System.h"

#include <stddef.h>

/**
 * Creates the integrator, storing the pointer provided to the user's
 * parameters.
 * @param[in] *p Pointer to the user's parameters.
 */
Integrator::Integrator(Param *p, const char *name)
{
    IO::Info("Using %s Integrator\n", name);

    params = p;
    features = MIST_FEATURE_NONE;
    system = NULL;
    constraintSolver = NULL;
    supportsConstraints = false;
    conservesMomentum = false;
}

/**
 * Frees the integrator's allocated storage.
 */
Integrator::~Integrator()
{
    if (system != NULL)
    {
        delete system;
        system = NULL;
    }

    if (constraintSolver != NULL)
    {
        delete constraintSolver;
        constraintSolver = NULL;
    }
}

/**
 * An Integrator requires access to a System class that contains the particle
 * data that is to be integrated.  This is set shortly after the Integrator
 * is constructed.
 * @param[in] *s Pointer to the System being used. This is checked and stored.
 */
void Integrator::SetSystem(System *s)
{
    if (s == NULL)
    {
        IO::Error("No System was defined due to a misconfiguration.  "
                  "Ensure MIST is built with one of the __MIST_WITH_* macros "
                  "defined!\n");
    }
    if (system != NULL)
    {
        IO::Warning("SetSystem() called when a system has "
                    "already been provided.  "
                    "Ignoring it and carrying on!\n");
        return;
    }

    system = s;
    system->SetIntegrator(this);

    // Also set up the constraint solver here.
    constraintSolver = new ConstraintSolver(params, system, this);
}

/**
 * Provides access to the System (sub)class associated with the integrator.
 */
System *Integrator::GetSystem() const { return system; }

/**
 * A convience function to be used by subclasses.  PositionStep updates all
 * particle positions as x(i) = x(i) + v(i) * dt. Uses OpenMP to update the
 * positions if this is available.
 * @param[in] dt The integration time step.
 */
void Integrator::PositionStep(double dt)
{
    // Store the current positions, for use by the constraintSolver
    constraintSolver->StorePositions();

    Vector3 v, p;
    System *s = system;
    int n = s->GetNumParticles();

#pragma omp parallel for default(none) private(v, p) shared(dt, s, n)

    for (int i = 0; i < n; i++)
    {
        v = s->GetVelocity(i);
        p = s->GetPosition(i);
        p = p + Vector3::Scale(dt, v); // p = p + dt * v
        s->SetPosition(i, p);
    }
}

/**
 * A convenience function to be used by subclasses.  VelocityStep updates all
 * particle velocities as v(i) = v(i) + F(i) / m(i) * dt. Uses OpenMP to
 * update the velocities if this is available.
 * @param[in] dt The integration time step.
 */
void Integrator::VelocityStep(double dt)
{
    Vector3 v, f;
    double one_over_m;
    System *s = system;
    int n = s->GetNumParticles();

#pragma omp parallel for default(none) private(v, f, one_over_m)               \
    shared(s, dt, n)
    for (int i = 0; i < n; i++)
    {
        one_over_m = s->GetInverseMass(i);
        v = s->GetVelocity(i);
        f = s->GetForce(i);
        v = v + Vector3::Scale(dt * one_over_m, f); // v = v + dt / m * f
        s->SetVelocity(i, v);
    }
}

/**
 * Calls the constraint solver to resolve position constraints (if any are
 * defined) with RATTLE.
 * @param[in] dt The integration time step.
 */
void Integrator::ResolvePositionConstraints(double dt)
{
    constraintSolver->ResolvePositionConstraints(dt);
}

/**
 * Calls the constraint solver to resolve velocity constraints (if any are
 * defined) with RATTLE.
 * @param[in] dt The integration time step.
 */
void Integrator::ResolveVelocityConstraints(double dt)
{
    constraintSolver->ResolveVelocityConstraints(dt);
}

/**
 * If an integrator requires any specific features from the host application
 * code they will be returned by this function, allowing the host to adapt its
 * behaviour accordingly.
 *
 * @see FEATURE_FLAGS
 */
int Integrator::GetFeatures() const { return features; }

int Integrator::GetMassRequirements() const { return massrequirements; }

int Integrator::GetNumConstraints() const
{
    if (constraintSolver == NULL)
        return 0;
    return constraintSolver->GetNumConstraints();
}

bool Integrator::ConservesMomentum() const { return conservesMomentum; }

bool Integrator::SupportsConstraints() const { return supportsConstraints; }

void Integrator::ReadDoubleParam(Param *p, double *var, const char *key, const char *units)
{
    int err = getParamDoubleValue(p, key, var);
    if (err == ERR_INPUTVALUE)
    {
        IO::Warning("No value provided for '%s' parameter, "
                    "choosing default value: %lf %s\n",
                    key, *var, units);
    }
    else if (err == ERR_OK)
    {
        IO::Info("Value for '%s' is: %lf %s\n", key, *var, units);
    }
    else
    {
        IO::Error("Unknown error for getParamDoubleValue(), called "
                  "for '%s'.\n", key);
    }
}

void Integrator::ReadIntParam(Param *p, int *var, const char *key, const char *units)
{
    int err = getParamIntValue(p, key, var);
    if (err == ERR_INPUTVALUE)
    {
        IO::Warning("No value provided for '%s' parameter, "
                    "choosing default value: %d %s\n",
                    key, *var, units);
    }
    else if (err == ERR_OK)
    {
        IO::Info("Value for '%s' is: %d %s\n", key, *var, units);
    }
    else
    {
        IO::Error("Unknown error for getParamIntValue(), called "
                  "for '%s'.\n", key);
    }
}

void Integrator::ReadBoolParam(Param *p, bool *var, const char *key)
{
    int err = getParamBooleanValue(p, key, (boolean *)var);
    if (err == ERR_INPUTVALUE)
    {
        IO::Warning("No value provided for '%s' parameter, "
                    "choosing default value: %s\n",
                    key, *var ? "T" : "F");
    }
    else if (err == ERR_OK)
    {
        IO::Info("Value for '%s' is: %s\n", key, *var ? "T" : "F");
    }
    else
    {
        IO::Error("Unknown error for getParamBoolValue(), called "
                  "for '%s'.\n", key);
    }
}
