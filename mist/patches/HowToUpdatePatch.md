These instructions are for modifying the MIST/Application interface and
would only be required if you are planning to modify the library.

# The process can be automated using the 'generate_patch' script:

cd $MIST_ROOT/mist/patches
./generate_patch /path/to/original/source/ /path/to/modified/source/ name-of-file-to-generate

# Elena's notes on manual process for how to create/update the patch

For reference see this stack overflow thread: 
[diff for patching without renaming target file](http://stackoverflow.com/questions/9639436/diff-for-patching-without-renaming-target-file).

In what follows:

```
Root_To_Code/MyCode1
```

is a directory that contains the files which you wish to change and:

```
Root_To_Code/MyCode2
```

is a directory with a copy of those files where the actual changes have been made.

1. Apply the changes to the files in `MyCode2` and compile the code.
2. Ensure that any changes you make to any source files follow 
   the following format:

    ```
    //MIST patch

       /* Any code changes that you have made */
  
    //End of MIST patch
    ```
 3. Ensure there are no editor backup files in the directories 
    (i.e. `rm -f *~ ./#* *%` files, make sure there are no spaces otherwise you 
    will delete all your changes!).

 4. In the parent directory of `MyCode1` and `MyCode2` run:
     
    ```
     diff -ruN -x .git -x patches MyCode1 MyCode2 > \
                          This_Code_Version.patch
    ```
 5. To get rid of all the lines in `MyCode_Version.patch` that start with 
    `diff -ruN ` use `sed`:
    
    ```
     sed '/diff -ruN/d' MyCode_Version.patch > tmpnew
     mv tmpnew MyCode_Version.patch
    ```
 6. Strip the leading directory off manually, so you can apply the patch 
    with `patch -p0`, so that the directory starts at `src/..`.

 7. Change any references to your own directory structure in the patch to 
    use one relative to `MIST_PATH`. For example, for the Gromacs patch   
    the `CMakeLists.txt` file is patched to include the location of the 
    mist directory. Change:
    
    * `link_directories(/home/my-directory-structure/mist/mist)`
    
      To:
      
    * `link_directories(MIST_PATH)`

    Ensure this is done for *all* references to your personal directory space 
    so that the code can be patched and compiled on other machines and by 
    different users.

 8. Remove any remaning `*.o` object files.

 9. Copy the patch to the patchin file:
    
    ```
     cp myNewPatch mist/patches/myNewPatch.patchin
    ```


