/*******************************************************************************
*                                                                              *
*             MIST - Molecular Integration Simulation Toolkit                  *
*         A library of integration algorithms for Molecular Dynamics           *
*                                                                              *
*            Copyright (c) 2014-2020, The University of Edinburgh              *
*                            All rights reserved.                              *
*                     List of contributors in AUTHORS                          *
*                                                                              *
*            MIST is free software.  See LICENSE for details                   *
*                                                                              *
*******************************************************************************/

/**
 * @file TAMD.h
 * @brief Declaration of TAMD integrator class.
 * @author RB
 */

#ifndef MIST_TAMD_H
#define MIST_TAMD_H

#include "LangevinIntegrator.h"

/**
 * @brief Declaration of the TAMD Integrator class
 *
 * Langevin Dynamics based on the BAOAB Splitting of Matthews and
 * Leimkuhler for details see http://dx.doi.org/10.1063/1.4802990
 */
class TAMD : public LangevinIntegrator
{
  public:
    /**
     * @brief Default Constructor
     *
     * @param p a pointer to the parameters
     */
    TAMD(Param *p);

    /**
     * @brief Default Destructor
     */
    virtual ~TAMD();

    /**@brief Position step for TAMD variables
     * @param dt time step in host units
     */
    void TAMDvars_PositionStep(double dt);

    /**computes difference between dihedral angles phi1 and phi2 modulo 2*pi.
     * @param phi1 first angle
     * @param phi2 second angle
     *
     * @return difference betwheen two angles, modulo 2*pi
     */
    double DihedralDiff(double phi1, double phi2);

    /**
     * @brief Integrate positions and velocities over time dt using
     * Langevin Dynamics
     *
     * @param dt the timestep in internal units
     */
    void Step(double dt);

    /**
     * @brief Setter for the system pointer
     *
     * @param s pointer to an instance of the System class
     */
    void SetSystem(System *s);

  protected:
    int phi_ind[4];   /**< @brief Atom indices defining dihedral phi **/
    int psi_ind[4];   /**< @brief Atom indices defining dihedral psi **/
    double m_s_inp;   /**< @brief mass of TAMD dofs in unit masses **/
    double kappa_inp; /**< @brief the TAMD coupling constant **/
    double fric_s;    /**< @brief the TAMD friction parameter **/
    double temp_s;    /**< @brief temperature of additional dof in K **/
    double kbt_s;     /**< @brief Boltzmann's constant * temp_s **/
    double m_s;       /**< @brief mass of the additional dof **/
    double kappa;     /**< @brief TAMD spring constant **/
    // additional dof positions, velocities and forces
    double s_phi;     /**< @brief phi dihedral angle **/
    double v_phi;     /**< @brief phi dihedral angular velocity **/
    double force_phi; /**< @brief phi dihedral torque **/
    double s_psi;     /**< @brief psi dihedral angle **/
    double v_psi;     /**< @brief psi dihedral angular velocity **/
    double force_psi; /**< @brief psi dihedral torque **/
    // BAOAB constraints
    double g_sdt; /**< @brief friction * timestep **/
    double c1_s;  /**< @brief c1 constant for additional dof integration **/
    double c3_s;  /**< @brief c3 constant for additional dof integation **/
    double sqrt_ms_inv; /**< @brief inverse square root of TAMD mass **/
    int save_freq; /**< @brief the frequency with which TAMD output is saved **/
};

#endif
