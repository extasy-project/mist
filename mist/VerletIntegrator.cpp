/*******************************************************************************
*                                                                              *
*             MIST - Molecular Integration Simulation Toolkit                  *
*         A library of integration algorithms for Molecular Dynamics           *
*                                                                              *
*            Copyright (c) 2014-2020, The University of Edinburgh              *
*                            All rights reserved.                              *
*                     List of contributors in AUTHORS                          *
*                                                                              *
*            MIST is free software.  See LICENSE for details                   *
*                                                                              *
*******************************************************************************/

/**
 * @file VerletIntegrator.cpp
 * @brief Implementation of a Verlet Integrator
 */

#include "VerletIntegrator.h"
#include "ConstraintSolver.h"
#include "IO.h"
#include "System.h"

VerletIntegrator::VerletIntegrator(Param *p) : Integrator(p, "Velocity Verlet")
{
    features |= MIST_FEATURE_POS_VEL_NO_OFFSET;

    massrequirements = MIST_MASS_REQD | MIST_INV_MASS_REQD;

    conservesMomentum = true;
    supportsConstraints = true;
}

VerletIntegrator::~VerletIntegrator() {}

void VerletIntegrator::Step(double dt)
{
    // Simple velocity verlet algorithm
    // Velocity half-step
    VelocityStep(0.5 * dt);

    // Position full step
    // Saves positions for constraint solver (only when constraints are on)
    PositionStep(dt);

    // Resolve position & half-step velocity constraints
    ResolvePositionConstraints(dt);

    system->UpdateForces();

    // Velocity half-step
    VelocityStep(0.5 * dt);

    // Resolve full-step velocity constraints
    ResolveVelocityConstraints(dt);

    // write into diagnostic file
    // constraintSolver->WriteCounts(1, 100);
}
