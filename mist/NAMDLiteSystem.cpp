/*******************************************************************************
*                                                                              *
*             MIST - Molecular Integration Simulation Toolkit                  *
*         A library of integration algorithms for Molecular Dynamics           *
*                                                                              *
*            Copyright (c) 2014-2020, The University of Edinburgh              *
*                            All rights reserved.                              *
*                     List of contributors in AUTHORS                          *
*                                                                              *
*            MIST is free software.  See LICENSE for details                   *
*                                                                              *
*******************************************************************************/

/**
 * @file NAMDLiteSystem.cpp
 * @brief Implementation of System interface for NAMD-Lite
 */

#include "step/step_defn.h"
#include "force/fresult.h"
#include "force/defn.h"
#include <cmath>

#include "IO.h"
#include "NAMDLiteSystem.h"
#include <stddef.h>

// Implementation of the NAMD-Lite System class

NAMDLiteSystem::NAMDLiteSystem() : System()
{
    // NAMD-Lite specific conversion factors
    boltzmann = 0.001987191;   // internal units are kcal/mol/K
    picosecond = 1000.0;       // internal units are fs
    amu = 2390.05736137667304; // internal units are amu, but stored masses
    // carry a factor of MD_FORCE_CONST (to obtain a consistent
    // set of units for integration)
    angstrom = 1.0;            // internal units are angstrom
    atm = 1 / 0.987;           // internal units are bar
}

NAMDLiteSystem::~NAMDLiteSystem()
{
    // Don't free masses, it's a pointer to a NAMD-Lite array

    if (invmasses != NULL)
    {
        delete[] invmasses;
        invmasses = NULL;
    }

    if (invsqrtmasses != NULL)
    {
        delete[] invsqrtmasses;
        invsqrtmasses = NULL;
    }
}

Vector3 NAMDLiteSystem::GetVelocity(int i) const
{
    double x = ((double *)velocities)[3 * i];
    double y = ((double *)velocities)[3 * i + 1];
    double z = ((double *)velocities)[3 * i + 2];
    return Vector3(x, y, z);
}

void NAMDLiteSystem::SetVelocity(int i, Vector3 v)
{
    ((double *)velocities)[3 * i] = v.x;
    ((double *)velocities)[3 * i + 1] = v.y;
    ((double *)velocities)[3 * i + 2] = v.z;
}

Vector3 NAMDLiteSystem::GetPosition(int i) const
{
    double x = ((double *)positions)[3 * i];
    double y = ((double *)positions)[3 * i + 1];
    double z = ((double *)positions)[3 * i + 2];
    return Vector3(x, y, z);
}

void NAMDLiteSystem::SetPosition(int i, Vector3 p)
{
    ((double *)positions)[3 * i] = p.x;
    ((double *)positions)[3 * i + 1] = p.y;
    ((double *)positions)[3 * i + 2] = p.z;
}

double NAMDLiteSystem::GetPressure() const
{
    if (pbc != true)
        return 0.0; // Pressure is not defined for non-periodic systems

    // TODO check
    return *(double *)pressure;
}

double NAMDLiteSystem::GetPotentialEnergy() const
{
    return GetPotentialEnergy(MIST_FORCE_ALL);
}

double NAMDLiteSystem::GetPotentialEnergy(force_component_t t) const
{
    double e;
    switch (t)
    {
    case (MIST_FORCE_ALL):
        e = *(double *)potential_energy;
        break;
    case (MIST_FORCE_BOND):
        e = ((Step *)force_data)->all_forces->u_bond;
        break;
    case (MIST_FORCE_BEND):
        e = ((Step *)force_data)->all_forces->u_angle;
        break;
    case (MIST_FORCE_PROPDIHED):
        e = ((Step *)force_data)->all_forces->u_dihed;
        break;
    case (MIST_FORCE_IMPROPDIHED):
        e = ((Step *)force_data)->all_forces->u_impr;
        break;
    case (MIST_FORCE_NONBONDED):
        e = ((Step *)force_data)->all_forces->u_elec +
            ((Step *)force_data)->all_forces->u_vdw;
        break;
    default:
        e = 0.0;
        break;
    }

    return e;
}

Vector3 NAMDLiteSystem::GetForce(int i) const
{
    return GetForce(i, MIST_FORCE_ALL);
}

Vector3 NAMDLiteSystem::GetForce(int i, force_component_t t) const
{
    double x, y, z;

    switch (t)
    {
    case (MIST_FORCE_ALL):
        x = ((double *)forces)[3 * i];
        y = ((double *)forces)[3 * i + 1];
        z = ((double *)forces)[3 * i + 2];
        break;
    case (MIST_FORCE_BOND):
        x = ((double *)((Step *)force_data)->all_forces->f_bond)[3 * i];
        y = ((double *)((Step *)force_data)->all_forces->f_bond)[3 * i + 1];
        z = ((double *)((Step *)force_data)->all_forces->f_bond)[3 * i + 2];
        break;
    case (MIST_FORCE_BEND):
        x = ((double *)((Step *)force_data)->all_forces->f_angle)[3 * i];
        y = ((double *)((Step *)force_data)->all_forces->f_angle)[3 * i + 1];
        z = ((double *)((Step *)force_data)->all_forces->f_angle)[3 * i + 2];
        break;
    case (MIST_FORCE_PROPDIHED):
        x = ((double *)((Step *)force_data)->all_forces->f_dihed)[3 * i];
        y = ((double *)((Step *)force_data)->all_forces->f_dihed)[3 * i + 1];
        z = ((double *)((Step *)force_data)->all_forces->f_dihed)[3 * i + 2];
        break;
    case (MIST_FORCE_IMPROPDIHED):
        x = ((double *)((Step *)force_data)->all_forces->f_impr)[3 * i];
        y = ((double *)((Step *)force_data)->all_forces->f_impr)[3 * i + 1];
        z = ((double *)((Step *)force_data)->all_forces->f_impr)[3 * i + 2];
        break;
    case (MIST_FORCE_NONBONDED):
        x = ((double *)((Step *)force_data)->all_forces->f_elec)[3 * i] +
            ((double *)((Step *)force_data)->all_forces->f_vdw)[3 * i];
        y = ((double *)((Step *)force_data)->all_forces->f_elec)[3 * i + 1] +
            ((double *)((Step *)force_data)->all_forces->f_vdw)[3 * i + 1];
        z = ((double *)((Step *)force_data)->all_forces->f_elec)[3 * i + 2] +
            ((double *)((Step *)force_data)->all_forces->f_vdw)[3 * i + 2];
        break;
    default:
        x = 0.0;
        y = 0.0;
        z = 0.0;
        break;
    }

    return Vector3(x, y, z);
}

void NAMDLiteSystem::SetMassPtr(void *m, int massrequirements)
{
    int i;

    if (numParticles < 1)
        return;

    if (massrequirements & MIST_MASS_REQD)
    {
        masses = (double *)m;
    }

    if (massrequirements & MIST_INV_MASS_REQD)
    {
        invmasses = new double[numParticles];
        for (i = 0; i < numParticles; i++)
        {
            invmasses[i] = 1. / (((double *)m)[i]);
        }
    }

    if (massrequirements & MIST_INVSQRT_MASS_REQD)
    {
        invsqrtmasses = new double[numParticles];
        for (i = 0; i < numParticles; i++)
        {
            invsqrtmasses[i] = 1. / sqrt(((double *)m)[i]);
        }
    }
}

double NAMDLiteSystem::GetMass(int i) const { return masses[i]; }

double NAMDLiteSystem::GetInverseMass(int i) const { return invmasses[i]; }

double NAMDLiteSystem::GetInverseSqrtMass(int i) const
{
    return invsqrtmasses[i];
}

char *NAMDLiteSystem::GetKind(int i) const
{
    return (char *)(((MD_Atom *)kinds)[i].type);
}

void NAMDLiteSystem::GetCell(Vector3 *a, Vector3 *b, Vector3 *c)
{
    if (pbc != true)
    {
        IO::Error("Called GetCell() when PBC are off!\n");
        return;
    }

    Force_t *f = (Force_t *)cell;
    a->x = f->v1.x;
    a->y = f->v1.y;
    a->z = f->v1.z;

    b->x = f->v2.x;
    b->y = f->v2.y;
    b->z = f->v2.z;

    c->x = f->v3.x;
    c->y = f->v3.y;
    c->z = f->v3.z;
}

void NAMDLiteSystem::GetCellInverse(Vector3 *a, Vector3 *b, Vector3 *c)
{
    if (pbc != true)
    {
        IO::Error("Called GetCellInverse() when PBC are off!\n");
        return;
    }

    Force_t *f = (Force_t *)cell;
    a->x = f->ta1.x;
    a->y = f->ta2.x;
    a->z = f->ta3.x;

    b->x = f->ta1.y;
    b->y = f->ta2.y;
    b->z = f->ta3.y;

    c->x = f->ta1.z;
    c->y = f->ta2.z;
    c->z = f->ta3.z;
}

void NAMDLiteSystem::SetCell(Vector3 a, Vector3 b, Vector3 c)
{
    IO::Error("NAMD-Lite does not support variable cells - ignoring call to "
              "SetCell()!\n");
}
