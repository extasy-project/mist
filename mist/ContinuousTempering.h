/*******************************************************************************
*                                                                              *
*             MIST - Molecular Integration Simulation Toolkit                  *
*         A library of integration algorithms for Molecular Dynamics           *
*                                                                              *
*            Copyright (c) 2014-2020, The University of Edinburgh              *
*                            All rights reserved.                              *
*                     List of contributors in AUTHORS                          *
*                                                                              *
*            MIST is free software.  See LICENSE for details                   *
*                                                                              *
*******************************************************************************/

/**
 * @file ContinuousTempering.h
 * @brief Declaration of class for Continuous Tempering Integrator
 * @author GG
 */

#ifndef MIST_CONTINUOUSTEMPERING_H
#define MIST_CONTINUOUSTEMPERING_H

#include "LangevinIntegrator.h"

/**
 * @brief Declaration of the Continuous Tempering Integrator class
 *
 * See http://dx.doi.org/10.1103/PhysRevE.91.061301 for details
 */
class ContinuousTempering : public LangevinIntegrator
{
  public:
    /**
     * @brief Default Constructor
     *
     * @param p a pointer to the parameters
     */
    ContinuousTempering(Param *p);

    /**
     * @brief Default Destructor
     */
    virtual ~ContinuousTempering();

    /**
     * @brief Integrate positions and velocities over time dt using
     * Continuous Tempering
     *
     * @param dt the timestep in internal units
     */
    void Step(double dt);

  protected:
    /**
     * @brief Computes the value and first derivative of the coupling function
     *
     * This function evaluates the value f(xi) of the coupling function and
     * returns it as 'cc', and
     * it evaluates the derivative d/dxi f(xi) and returns it as 'd_cc'. f(xi)
     * is a piecewise polynomial function which
     * is zero in the interval [-D,D], takes the value 'Tf' if |s|>D2 and
     * smoothly interpolates in between. See
     * the Continuous Tempering paper for more information.
     *
     * @param cc the value of the coupling function
     * @param d_cc the derivative of the coupling function w.r.t. xi
     * @param s the input value xi
     * @param D the range within which the function is 0
     * @param D2 the upper bound above which the function takes the value Tf
     * @param Tf the value of the function beyond D2
     */
    void SS_coupling(double *cc, double *d_cc, double s, double D, double D2,
                     double Tf);

    /**
     * @brief Computes the force acting of the extended variable xi
     *
     * this function evaluates the RHS of the eom for xi, i.e.
     * fs = d/dxi f(xi)*V(q,p) + d/dxi Phi(xi),
     * with f(xi) = coupling function and Phi(xi) = confining potential.
     * @param fs the force (added to by this function)
     * @param cc the coupling function
     * @param d_cc the derivative of the coupling function w.r.t. xi
     * @param peoff offset w.r.t. current potential energy
     */
    void static_SSforce(double *fs, double cc, double d_cc, double peoff);

    /**
     * @brief Computes metadynamics force and updates metadynamics potential
     *
     * This function deals with the metadynamics part of Continuous Tempering.
     * It does three things:
     * (1) The metadynamics biasing force (i.e. the derivative of the biasing
     * potential 'VG') is evaluated and
     * returned as 'fs'. (2) If addgauss==1, then the metadynamics potential
     * 'VG' is modified by adding another
     * gaussian with height 'hills_height' and width 'sigma' at the current
     * position 's' of the xi variable.
     * (3) If this function is called for the first time and read_bias_file==1,
     * then the potential 'VG' is read
     * from the file 'meta.0'.
     *
     * @param fs the force (added to by this function)
     * @param s the input value xi
     * @param addgauss if to add a new gaussian
     * @param D the range within which the function is 0
     * @param D2 the upper bound above which the function takes the value Tf
     */
    void metadyn(double *fs, double s, bool addgauss, double D, double D2);

    /**
     * @brief write the value of xi and the coupling function to file
     *
     * This function writes the position of xi and the value of the coupling
     * function
     * for the current frame into the file 'coupling.dat'. This happens every
     * save_conf frames
     *
     * @param cc the coupling function
     * @param s the current value of xi
     * @param it the current iteration number
     * @param fss the force due to the coupling
     */
    void write_confxyz(double cc, double s, int it, double fss);

    double Tfact;     /**< @brief temperature rescaling factor */
    double pe_offset; /**< @brief potential energy offset */
    double m_ss;      /**< @brief mass of additional d.o.f. */
    double fric_ss;   /**< @brief friction applied to additional d.o.f. */
    double Delta;  /**< @brief absolute value where the coupling function is
                          zero */
    double Delta2; /**< @brief absolute value above which the coupling
                          function takes its maximum value */
    double hills_height; /**< @brief height of hills */
    double sigma;  /**< @brief width of hills */
    int meta_dep;  /**< @brief number of time steps between metadynamics hills
                      deposition */
    int save_conf; /**< @brief number of time steps between saving addition
                      d.o.f. */
    int read_bias_file; /**< @brief read an initial bias from file */
};

#endif
