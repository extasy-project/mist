/*******************************************************************************
*                                                                              *
*             MIST - Molecular Integration Simulation Toolkit                  *
*         A library of integration algorithms for Molecular Dynamics           *
*                                                                              *
*            Copyright (c) 2014-2020, The University of Edinburgh              *
*                            All rights reserved.                              *
*                     List of contributors in AUTHORS                          *
*                                                                              *
*            MIST is free software.  See LICENSE for details                   *
*                                                                              *
*******************************************************************************/

/**
 * @file LAMMPSSystem.cpp
 * @brief Implementation of System interface for LAMMPS
 */

#include <cmath>

#include "IO.h"
#include "LAMMPSSystem.h"
#include <stddef.h>

#include "domain.h"
#include "force.h"
#include "kspace.h"
#include "mist_run.h"

// Implementation of the LAMMPS System class

LAMMPSSystem::LAMMPSSystem() : System()
{
    // LAMMPS specific conversion factors
    // defaults to LJ units from LAMMPS
    boltzmann = 1.0;
    picosecond = 1.0;
    amu = 1.0;
    angstrom = 1.0;
    atm = 101325.0;
}

LAMMPSSystem::~LAMMPSSystem()
{
    if (invmasses != NULL)
    {
        delete[] invmasses;
        invmasses = NULL;
    }

    if (invsqrtmasses != NULL)
    {
        delete[] invsqrtmasses;
        invsqrtmasses = NULL;
    }
}

Vector3 LAMMPSSystem::GetVelocity(int i) const
{
    double x = ((double *)velocities)[3 * i];
    double y = ((double *)velocities)[3 * i + 1];
    double z = ((double *)velocities)[3 * i + 2];

    return Vector3(x, y, z);
}

void LAMMPSSystem::SetVelocity(int i, Vector3 v)
{
    ((double *)velocities)[3 * i] = v.x;
    ((double *)velocities)[3 * i + 1] = v.y;
    ((double *)velocities)[3 * i + 2] = v.z;
}

Vector3 LAMMPSSystem::GetPosition(int i) const
{
    double x = ((double *)positions)[3 * i];
    double y = ((double *)positions)[3 * i + 1];
    double z = ((double *)positions)[3 * i + 2];

    return Vector3(x, y, z);
}

void LAMMPSSystem::SetPosition(int i, Vector3 p)
{
    ((double *)positions)[3 * i] = p.x;
    ((double *)positions)[3 * i + 1] = p.y;
    ((double *)positions)[3 * i + 2] = p.z;
}

double LAMMPSSystem::GetPressure() const
{
    if (pbc != true)
        return 0.0; // Pressure is not defined for non-periodic systems

    // TODO check
    return *(double *)pressure;
}

double LAMMPSSystem::GetPotentialEnergy() const
{
    return GetPotentialEnergy(MIST_FORCE_ALL);
}

double LAMMPSSystem::GetPotentialEnergy(force_component_t t) const
{
    double e;
    switch (t)
    {
    case (MIST_FORCE_ALL):
        e = *(double *)potential_energy;
        break;
    default:
        e = 0.0;
        break;
    }

    return e;
}

Vector3 LAMMPSSystem::GetForce(int i) const
{
    return GetForce(i, MIST_FORCE_ALL);
}

Vector3 LAMMPSSystem::GetForce(int i, force_component_t t) const
{
    double x, y, z;

    switch (t)
    {
    case (MIST_FORCE_ALL):

        x = ((double *)forces)[3 * i];
        y = ((double *)forces)[3 * i + 1];
        z = ((double *)forces)[3 * i + 2];
        break;

    default:
        x = 0.0;
        y = 0.0;
        z = 0.0;
        break;
    }

    return Vector3(x, y, z);
}

void LAMMPSSystem::SetMassPtr(void *m, int massrequirements)
{
    int i;

    if (massrequirements & MIST_MASS_REQD)
    {
        masses = (double *)m;
    }

    if (massrequirements & MIST_INV_MASS_REQD)
    {
        IO::Info("Caching inverse masses\n");
        invmasses = new double[numParticles];
        for (i = 0; i < numParticles; i++)
        {
            invmasses[i] = 1. / (((double *)m)[i]);
        }
    }

    if (massrequirements & MIST_INVSQRT_MASS_REQD)
    {
        IO::Info("Caching inverse sqrt masses\n");
        invsqrtmasses = new double[numParticles];
        for (i = 0; i < numParticles; i++)
        {
            invsqrtmasses[i] = 1. / sqrt(((double *)m)[i]);
        }
    }
}

double LAMMPSSystem::GetMass(int i) const { return masses[i]; }

double LAMMPSSystem::GetInverseMass(int i) const { return invmasses[i]; }

double LAMMPSSystem::GetInverseSqrtMass(int i) const
{
    return invsqrtmasses[i];
}

void LAMMPSSystem::GetCell(Vector3 *a, Vector3 *b, Vector3 *c)
{

    if (pbc != true)
    {
        IO::Error("Called GetCell() when PBC are off!\n");
        return;
    }

    // LAMMPS storage format:
    // Box vectors are
    // a = (xhi-xlo, 0, 0)
    // b = (xy, yhi-ylo, 0)
    // c = (xz, yz, zhi-zlo)

    LAMMPS_NS::Domain *d = ((LAMMPS_NS::cell_data_t *)cell)->d;

    a->x = d->h[0];
    a->y = 0.0;
    a->z = 0.0;

    b->x = d->h[5];
    b->y = d->h[1];
    b->z = 0.0;

    c->x = d->h[4];
    c->y = d->h[3];
    c->z = d->h[2];
}

void LAMMPSSystem::GetCellInverse(Vector3 *a, Vector3 *b, Vector3 *c)
{

    if (pbc != true)
    {
        IO::Error("Called GetCellInverse() when PBC are off!\n");
        return;
    }

    // LAMMPS storage format:
    // Box vectors are
    // a = (xhi-xlo, 0, 0)
    // b = (xy, yhi-ylo, 0)
    // c = (xz, yz, zhi-zlo)

    LAMMPS_NS::Domain *d = ((LAMMPS_NS::cell_data_t *)cell)->d;

    a->x = d->h_inv[0];
    a->y = 0.0;
    a->z = 0.0;

    b->x = d->h_inv[5];
    b->y = d->h_inv[1];
    b->z = 0.0;

    c->x = d->h_inv[4];
    c->y = d->h_inv[3];
    c->z = d->h_inv[2];
}

void LAMMPSSystem::SetCell(Vector3 a, Vector3 b, Vector3 c)
{
    if (pbc != true)
    {
        IO::Error("Called SetCell() when PBC are off!\n");
        return;
    }

    // Check new vectors are valid
    if (a.y != 0.0 || a.z != 0.0 || b.z != 0.0)
    {
        IO::Error("New box vectors are not orthorhombic or triclinic!\n");
        return;
    }

    LAMMPS_NS::Domain *d = ((LAMMPS_NS::cell_data_t *)cell)->d;
    LAMMPS_NS::Force *f = ((LAMMPS_NS::cell_data_t *)cell)->f;

    // Put all atoms into fractional coordinates

    d->x2lamda(numParticles);

    // Rescale the box around the centre of the previous cell

    Vector3 oldmin(d->boxlo[0], d->boxlo[1], d->boxlo[2]);
    Vector3 oldmax(d->boxhi[0], d->boxhi[1], d->boxhi[2]);
    Vector3 centre = Vector3::Scale(0.5, Vector3::Add(oldmin, oldmax));

    d->boxlo[0] = centre.x - a.x * 0.5;
    d->boxlo[1] = centre.y - b.y * 0.5;
    d->boxlo[2] = centre.z - c.z * 0.5;

    d->boxhi[0] = centre.x + a.x * 0.5;
    d->boxhi[1] = centre.y + b.y * 0.5;
    d->boxhi[2] = centre.z + c.z * 0.5;

    d->yz = b.x;
    d->xz = c.x;
    d->xy = c.y;

    // Update data structures

    d->set_global_box();
    d->set_local_box();

    // Put all atoms back into real coordinates

    d->lamda2x(numParticles);

    // Box size has changed, so recompute kspace coeffs

    if (f->kspace != NULL)
        f->kspace->setup();
}

// XXX not implemented
char *LAMMPSSystem::GetKind(int i) const { return (char *)NULL; }
