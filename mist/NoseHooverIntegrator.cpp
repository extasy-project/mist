/*******************************************************************************
*                                                                              *
*             MIST - Molecular Integration Simulation Toolkit                  *
*         A library of integration algorithms for Molecular Dynamics           *
*                                                                              *
*            Copyright (c) 2014-2020, The University of Edinburgh              *
*                            All rights reserved.                              *
*                     List of contributors in AUTHORS                          *
*                                                                              *
*            MIST is free software.  See LICENSE for details                   *
*                                                                              *
*******************************************************************************/

/**
 * @file NoseHooverIntegrator.cpp
 * @brief Implementation of a Verlet Integrator with Nose-Hoover thermostat
 */

#include "NoseHooverIntegrator.h"
#include "ConstraintSolver.h"
#include "System.h"
#include <math.h>
#include <stddef.h>

NoseHooverIntegrator::NoseHooverIntegrator(Param *p)
    : Integrator(p, "Nose-Hoover")
{
    features |= MIST_FEATURE_POS_VEL_NO_OFFSET;
    features |= MIST_FEATURE_MODIFIES_CELL;
    features |= MIST_FEATURE_REQUIRES_PRESSURE_WITH_FORCES;

    massrequirements = MIST_MASS_REQD | MIST_INV_MASS_REQD;

    supportsConstraints = true;

    // Initialisation

    dof = 0;
    kbt = 0.0;
    to_pressure = 0.0;
    first_time = true;

    temp_xi = NULL;
    temp_xi_vel = NULL;
    temp_xi_acc = NULL;
    temp_xi_mass = NULL;

    press_xi = NULL;
    press_xi_vel = NULL;
    press_xi_acc = NULL;
    press_xi_mass = NULL;

    // Default values
    temp_target = 300.0;
    temp_chain_length = 3;
    temp_nsteps_relax = 100;
    press_target = 1.0;
    press_chain_length = 3;
    press_nsteps_relax = 100;
    box_quench = false;

    // Read in parameters from mist.params file
    ReadDoubleParam(p, &temp_target, "temperature", "K");
    ReadIntParam(p, &temp_chain_length, "temp_chain_length", "");
    ReadIntParam(p, &temp_nsteps_relax, "temp_nsteps_relax", "");
    ReadDoubleParam(p, &press_target, "pressure", "Atm");
    ReadIntParam(p, &press_chain_length, "press_chain_length", "");
    ReadIntParam(p, &press_nsteps_relax, "press_nsteps_relax", "");
    ReadBoolParam(p, &box_quench, "box_quench");
}

NoseHooverIntegrator::~NoseHooverIntegrator()
{
    if (temp_xi != NULL)
        delete[] temp_xi;
    if (temp_xi_vel != NULL)
        delete[] temp_xi_vel;
    if (temp_xi_acc != NULL)
        delete[] temp_xi_acc;
    if (temp_xi_mass != NULL)
        delete[] temp_xi_mass;

    if (press_xi != NULL)
        delete[] press_xi;
    if (press_xi_vel != NULL)
        delete[] press_xi_vel;
    if (press_xi_acc != NULL)
        delete[] press_xi_acc;
    if (press_xi_mass != NULL)
        delete[] press_xi_mass;
}

void NoseHooverIntegrator::Step(double dt)
{
    // Barostat NH chain half-step & scale barostat velocity
    BarostatNHC(0.5 * dt);

    // Thermostat half-step & scale particle velocity
    Thermostat(0.5 * dt);

    // Barostat half-step
    BarostatVelocityStep(0.5 * dt);
    BarostatRescaleVelocities(0.5 * dt);

    // Velocity half-step
    VelocityStep(0.5 * dt);

    // Box rescale half-step
    RescaleCell(exp(eps_v * 0.5 * dt));

    // Position full step
    PositionStep(dt);

    // Resolve position & half-step velocity constraints
    ResolvePositionConstraints(dt);

    // Box rescale half-step
    RescaleCell(exp(eps_v * 0.5 * dt));

    system->UpdateForces();

    // Velocity half-step
    VelocityStep(0.5 * dt);

    // Resolve full-step velocity constraints
    ResolveVelocityConstraints(dt);

    // Barostat half-step
    BarostatRescaleVelocities(0.5 * dt);
    BarostatVelocityStep(0.5 * dt);

    // Thermostat half-step & scale particle velocity
    Thermostat(0.5 * dt);

    // Barostat NH chain half-step & scale barostat velocity
    BarostatNHC(0.5 * dt);

}

void NoseHooverIntegrator::Thermostat(double dt)
{
    double tmp; // Temporary variable to save recomputing quantities
    int i;      // Loop iterator

    // First time: allocate and initialise thermostat
    if (temp_xi == NULL)
    {
        temp_xi = new double[temp_chain_length];
        temp_xi_vel = new double[temp_chain_length];
        temp_xi_acc = new double[temp_chain_length];
        temp_xi_mass = new double[temp_chain_length];

        dof = system->GetTotalNumParticles() * 3 - GetNumConstraints();
        kbt = system->Boltzmann() * temp_target;

        tmp = kbt * (double)temp_nsteps_relax * (double)temp_nsteps_relax * dt * dt * 4.0;

        // First element in the chain
        temp_xi_mass[i] = tmp * (double)dof;
        temp_xi[0] = 0.0;
        temp_xi_vel[0] = 0.0;
        temp_xi_acc[0] = 0.0; // Is set at the start of each step based on current
                         // particle velocities

        // Rest of the chain
        for (i = 1; i < temp_chain_length; i++)
        {
            temp_xi_mass[i] = tmp;
            temp_xi[i] = 0.0;
            temp_xi_vel[i] = 0.0;
            temp_xi_acc[i] = -kbt / temp_xi_mass[i];
        }
    }

    // Compute current kinetic energy
    double ke = 0.0;
    Vector3 v;
    for (i = 0; i < system->GetNumParticles(); i++)
    {
        v = system->GetVelocity(i);
        ke += Vector3::Mult(v, v) * system->GetMass(i);
    }

#ifdef __MIST_WITH_MPI
    // Sum up to get global KE
    double local_ke = ke;
    MPI_Allreduce(&local_ke, &ke, 1, MPI_DOUBLE, MPI_SUM,
                  system->GetCommunicator());
#endif

    // Set acceleration of first in the chain
    temp_xi_acc[0] = (ke - dof * kbt) / temp_xi_mass[0];

    // Update velocities from the end of the chain (half-step)
    // Last element in the chain
    temp_xi_vel[temp_chain_length - 1] =
        temp_xi_vel[temp_chain_length - 1] + temp_xi_acc[temp_chain_length - 1] * dt * 0.5;

    // Rest of the chain
    for (i = temp_chain_length - 2; i >= 0; i--)
    {
        tmp = exp(-0.25 * dt * temp_xi_vel[i + 1]);
        temp_xi_vel[i] = (temp_xi_vel[i] * tmp + temp_xi_acc[i] * dt * 0.5) * tmp;
    }

    // Rescale particle velocities
    tmp = exp(-dt * temp_xi_vel[0]);
    for (i = 0; i < system->GetNumParticles(); i++)
    {
        v = system->GetVelocity(i);
        system->SetVelocity(i, Vector3::Scale(tmp, v));
    }
    ke *= tmp * tmp;

    // Reset acceleration of first element of the chain based on updated KE
    temp_xi_acc[0] = (ke - dof * kbt) / temp_xi_mass[0];

    // Update positions of each element in the chain
    for (i = 0; i < temp_chain_length; i++)
    {
        temp_xi[i] = temp_xi[i] + dt * temp_xi_vel[i];
    }

    // Update velocities from the start of the chain (half-step)
    // First element in the chain
    tmp = exp(-0.25 * dt * temp_xi_vel[1]);
    temp_xi_vel[0] = (temp_xi_vel[0] * tmp + temp_xi_acc[0] * dt * 0.5) * tmp;

    // Middle of the chain
    for (i = 1; i < temp_chain_length - 1; i++)
    {
        tmp = exp(-0.25 * dt * temp_xi_vel[i + 1]);
        temp_xi_vel[i] = temp_xi_vel[i] * tmp;
        temp_xi_acc[i] =
            (temp_xi_mass[i - i] * temp_xi_vel[i - 1] * temp_xi_vel[i - 1] - kbt) / temp_xi_mass[i];
        temp_xi_vel[i] = (temp_xi_vel[i] + temp_xi_acc[i] * dt * 0.5) * tmp;
    }

    // Last element in the chain
    temp_xi_acc[temp_chain_length - 1] =
        (temp_xi_mass[temp_chain_length - 2] * temp_xi_vel[temp_chain_length - 2] *
             temp_xi_vel[temp_chain_length - 2] -
         kbt) /
        temp_xi_mass[temp_chain_length - 1];
    temp_xi_vel[temp_chain_length - 1] =
        temp_xi_vel[temp_chain_length - 1] + temp_xi_acc[temp_chain_length - 1] * dt * 0.5;
}

void NoseHooverIntegrator::BarostatNHC(double dt)
{
    double tmp; // Temporary variable to save recomputing quantities
    int i;      // Loop iterator

    // First time: allocate and initialise barostat
    if (first_time)
    {
        if (press_chain_length > 0)
        {
            press_xi = new double[press_chain_length];
            press_xi_vel = new double[press_chain_length];
            press_xi_acc = new double[press_chain_length];
            press_xi_mass = new double[press_chain_length];
        }

        kbt = system->Boltzmann() * temp_target;
        to_pressure = 136.259600296 / system->Boltzmann() * system->Angstrom() * system->Angstrom() * system->Angstrom() * system->Atmosphere();

        // Barostat
        eps_mass = (double)(system->GetTotalNumParticles() + 1) * kbt * (double)press_nsteps_relax * (double)press_nsteps_relax * dt * dt * 4.0;
        eps_v = 0.0;
        eps_acc = 0.0;

        tmp = kbt * (double)press_nsteps_relax * (double)press_nsteps_relax * dt * dt * 4.0;

        if (press_chain_length > 0)
        {
            // First element in the chain
            press_xi_mass[0] = tmp;
            press_xi[0] = 0.0;
            press_xi_vel[0] = 0.0;
            // Is set at the start of each step based on current pressure
            press_xi_acc[0] = 0.0;

            // Rest of the chain
            for (i = 1; i < press_chain_length; i++)
            {
                press_xi_mass[i] = tmp;
                press_xi[i] = 0.0;
                press_xi_vel[i] = 0.0;
                press_xi_acc[i] = -kbt / press_xi_mass[i];
            }
        }
        first_time = false;
    }

    // Compute current barostat kinetic energy
    double ke = eps_mass * eps_v * eps_v;

    if (press_chain_length > 0)
    {
        // Set acceleration of first element in the chain
        press_xi_acc[0] = (ke - 3.0 * kbt) / press_xi_mass[0];

        // Update velocities from the end of the chain (half-step)
        // Last element in the chain
        press_xi_vel[press_chain_length - 1] =
            press_xi_vel[press_chain_length - 1] +
            press_xi_acc[press_chain_length - 1] * dt * 0.5;

        // Rest of the chain
        for (i = press_chain_length - 2; i >= 0; i--)
        {
            tmp = exp(-0.25 * dt * press_xi_vel[i + 1]);
            press_xi_vel[i] = (press_xi_vel[i] * tmp + press_xi_acc[i] * dt * 0.5) * tmp;
        }

        // Rescale velocity of barostat
        tmp = exp(-dt * press_xi_vel[0]);
        eps_v *= tmp;
        ke *= tmp * tmp;

        // Reset acceleration of first element in the chain based on updated KE
        press_xi_acc[0] = (ke - 3.0 * kbt) / press_xi_mass[0];

        // Update positions of each element in the chain
        for (i = 0; i < press_chain_length; i++)
        {
            press_xi[i] = press_xi[i] + dt * press_xi_vel[i];
        }

        // Update velocities from the start of the chain (half-step)
        // First element in the chain
        tmp = exp(-0.25 * dt * press_xi_vel[1]);
        press_xi_vel[0] = (press_xi_vel[0] * tmp + press_xi_acc[0] * dt * 0.5) * tmp;

        // Middle of the chain
        for (i = 1; i < press_chain_length - 1; i++)
        {
            tmp = exp(-0.25 * dt * press_xi_vel[i + 1]);
            press_xi_vel[i] = press_xi_vel[i] * tmp;
            press_xi_acc[i] =
                (press_xi_mass[i - i] * press_xi_vel[i - 1] * press_xi_vel[i - 1] - kbt) / press_xi_mass[i];
            press_xi_vel[i] = (press_xi_vel[i] + press_xi_acc[i] * dt * 0.5) * tmp;
        }

        // Last element in the chain
        press_xi_acc[press_chain_length - 1] =
            (press_xi_mass[press_chain_length - 2] * press_xi_vel[press_chain_length - 2] *
                 press_xi_vel[press_chain_length - 2] -
             kbt) /
            press_xi_mass[press_chain_length - 1];
        press_xi_vel[press_chain_length - 1] =
            press_xi_vel[press_chain_length - 1] + press_xi_acc[press_chain_length - 1] * dt * 0.5;
    }
}

void NoseHooverIntegrator::RescaleCell(double scale)
{
    Vector3 a, b, c;

    system->GetCell(&a, &b, &c);
    system->SetCell(a*scale, b*scale, c*scale);
}

void NoseHooverIntegrator::BarostatVelocityStep(double dt)
{
    Vector3 a,b,c;
    system->GetCell(&a, &b, &c);
    double vol = Vector3::Det3x3(a,b,c);

    // Set barostat force
    eps_acc = (system->GetPressure() - press_target*system->Atmosphere()) * vol / (eps_mass * to_pressure);

    // Update barostat velocity
    eps_v += dt * eps_acc;

    // Apply Ackland's box quenching scheme
    // http://dx.doi.org/10.1557/adv.2016.382
    if (box_quench && eps_v * eps_acc < 0.0) eps_v = 0.0;
}

void NoseHooverIntegrator::BarostatRescaleVelocities(double dt)
{
    double tmp = exp(-dt * eps_v);
    int i;
    Vector3 v;
    for (i = 0; i < system->GetNumParticles(); i++)
    {
        v = system->GetVelocity(i);
        system->SetVelocity(i, Vector3::Scale(tmp, v));
    }
}
