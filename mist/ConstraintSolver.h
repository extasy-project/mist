/*******************************************************************************
*                                                                              *
*             MIST - Molecular Integration Simulation Toolkit                  *
*         A library of integration algorithms for Molecular Dynamics           *
*                                                                              *
*            Copyright (c) 2014-2020, The University of Edinburgh              *
*                            All rights reserved.                              *
*                     List of contributors in AUTHORS                          *
*                                                                              *
*            MIST is free software.  See LICENSE for details                   *
*                                                                              *
*******************************************************************************/

/**
 * @file ConstraintSolver.h
 * @brief Declaration of a constraint solver class
 * @author RB
 */

#ifndef MIST_CONSTRAINTSOLVER_H
#define MIST_CONSTRAINTSOLVER_H

extern "C"
{
#include "params.h"
}

#include "System.h"

/**
 * @brief Implements constraint solvers for use in MIST Integrators
 *
 * This class provides functions for resolving rigid bond constraints, either
 * using Rattle or Symmetric Newton iterations (SNIP)
 */
class ConstraintSolver
{
  public:
    /**
     * @brief Selects what to constrain
     *
     * MIST can constain either all interatomic bonds, or only those to
     * hydrogen atoms, or no bonds.
     * This enum selects which.
     */
    enum constraints_t
    {
        CONSTRAINTS_OFF,
        CONSTRAINTS_H_BONDS_ONLY,
        CONSTRAINTS_ALL_BONDS
    };

    /**
     * @brief Selects which method to use to resolve constraints
     *
     * MIST implements both the RATTLE and Symmetric Newton iteration method
     * (SNIP) This enum selects which to use to resolve the constraints
     */
    enum method_t
    {
        CONSTRAINTS_METHOD_RATTLE,
        CONSTRAINTS_METHOD_SNIP
    };

    /**
     * @brief Default Constructor
     *
     * @param p a pointer to the parameters
     * @param s a pointer to the system which will be constrained
     * @param i a pointer to the integrator which may use the constraint solver
     */
    ConstraintSolver(Param *p, System *s, Integrator *i);

    /**
     * @brief Default Destructor
     */
    virtual ~ConstraintSolver();

    /**
     * @brief handles position constraints, calls down to method specified
     *
     * @param dt timestep in internal units
     * @return number of constraints that did not converge
     */
    int ResolvePositionConstraints(double dt);

    /**
     * @brief handles velocity constraints, calls down to method specified
     *
     * @param dt timestep in internal units
     * @return number of constraints that did not converge
     */
    int ResolveVelocityConstraints(double dt);

    /**
     * @brief Stores the present particle positions in array 'stored_positions'
     *
     */
    void StorePositions();

    /**
     * @brief Write to file the number of iterations taken to converge
     *constraints
     *
     *This function should be called at the end of the timestep in the
     * integrator if output of iteration counts and energies in 'counts.dat'
     *file is required.
     *
     * @param n_rattlePerStep number of times the constraint solver is called
     *per step
     * @param write_freq how many steps between outputs
     */
    void WriteCounts(int n_rattlePerStep, int write_freq);

    /**
     * @brief This function returns whether any constraints are set (true) or
     *not (false)
     *
     * @return if any constraints are defined
     */
    bool IsConstraintsOn() const;

    /**
     * @brief Gets the total number of constraints
     *
     * @return total number of constraints
     */
    int GetNumConstraints() const;

  protected:
    /**
     * @brief Initialises everything that can't be done in the constructor
     */
    void Initializer();

    /**
     * @brief Wrapper function to access the positions of particle i stored in
     * 'stored_positions'
     *
     * @param i the particle index
     * @return the stored position
     */
    Vector3 GetOldPosition(int i);

    /**
     * @brief Wrapper function for accessing iterated position of particle i
     *
     * @param i the particle index
     * @return the current position
     */
    Vector3 GetIterPosition(int i);

    /**
     * @brief Wrapper function for accessing iterated velocity of particle i
     *
     * @param i the particle index
     * @return the current velocity
     */
    Vector3 GetIterVelocity(int i);

    /**
     * @brief Wrapper function for modifying iterated position of particle i
     *
     * @param i the particle index
     * @param Q the current position
     */
    void SetIterPosition(int i, Vector3 Q);

    /**
     * @brief Wrapper function for modifying iterated velocity of particle i
     * @param i the particle index
     * @param V the current velocity
     */
    void SetIterVelocity(int i, Vector3 V);

    /**
     * @brief Solves position constraints with Symmetric Newton iteration (SNIP)
     *
     * @param dt timestep in internal units
     * @return number of constraints that did not converge
     */
    int SNIP_pos(double dt);

    /**
     * @brief The companion of SNIP_pos. Solves velocity constraints.
     *
     * @param dt timestep in internal units
     * @return number of constraints that did not converge
     */
    int SNIP_vel(double dt);

    /**
     * @brief Adjusts positions and half-step velocities subject to the defined
     * constraints (first half of RATTLE algorithm)
     *
     * @param dt timestep in internal units
     * @return number of constraints that did not converge
     */
    int Rattle_pos(double dt);

    /**
     * @brief Adjusts full-step velocities subject to the defined constraints
     * (second half of RATTLE algorithm)
     *
     * @param dt timestep in internal units
     * @return number of constraints that did not converge
     */
    int Rattle_vel(double dt);

    /**
     * @brief Build the sparse matrix R.
     */
    void BuildSparseR();

    /**
     * @brief A pointer to a container for particle positions (to be allocated
     * in the constructor)
     */
    double *stored_positions;

    /**
     * @brief A pointer to a container for the particle positions we want to
     * iterate over (to be allocated in the constructor)
     */
    double *iter_pos;

    /**
     * @brief A pointer to a container for the particle velocities we want to
     * iterate over (to be allocated in the constructor)
     */
    double *iter_vel;

    /**
     * @brief A pointer to a list of constrained bonds (to be allocated
     * in the constructor)
     */
    int *constraint_list;

    /**
     * @brief A pointer to a list that stores connectivity information of bonds
     */
    int *topology_list;

    /**
     * @brief The number of active constraints, used i.e. as iteration bound
     */
    int n_constr;

    int pos_iteration_count; /**< @brief number of iterations resolving
                                positions */
    int vel_iteration_count; /**< @brief number of iterations resolving
                                velocities */
    int n_calls; /**< @brief number of calls to the constraint solver */
    int n_list;  /**< @brief size of the topology list */

    /**
     * @brief this is 'true' once sparse matrix R has been assembled for
     * the first time (we need to do less work the following times)
     */
    bool R_ready;
    bool on_constraint; /**< @brief this is true if we are on the constraint
                           manifold */
    bool rebuild_R;     /**< @brief this is true if R needs rebuilding */

    Param *params; /**< @brief Pointer to the user's parameters */
    double TOL;    /**< @brief The numerical tolerance for stopping iteration
                      (constraint res) */
    double TOL_lambda; /**< @brief The numerical tolerance stopping iteration
                          (Delta_Lambda)*/
    System *system;    /**< @brief Pointer to the System to be constrained */
    constraints_t constraints; /**< @brief The level of constraints selected */
    method_t method;  /**< @brief The method used for solving constraints */
    int Num_iter_max; /**< @brief Maximum number of constraint iteratations used
                         by RATTLE */
};

#endif
