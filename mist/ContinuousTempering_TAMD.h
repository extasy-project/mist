/*******************************************************************************
*                                                                              *
*             MIST - Molecular Integration Simulation Toolkit                  *
*         A library of integration algorithms for Molecular Dynamics           *
*                                                                              *
*            Copyright (c) 2014-2020, The University of Edinburgh              *
*                            All rights reserved.                              *
*                     List of contributors in AUTHORS                          *
*                                                                              *
*            MIST is free software.  See LICENSE for details                   *
*                                                                              *
*******************************************************************************/

/**
 * @file ContinuousTempering_TAMD.h
 * @brief Declaration of ContinuousTempering_TAMD integrator class.
 * @author RB
 */

#ifndef MIST_CONTINUOUSTEMPERINGTAMD_H
#define MIST_CONTINUOUSTEMPERINGTAMD_H

#include "LangevinIntegrator.h"

/**
 * @brief Declaration of the Continuous Tempering + TAMD Integrator class
 *
 */
class ContinuousTempering_TAMD : public LangevinIntegrator
{
  public:
    /**
     * @brief Default Constructor
     *
     * @param p a pointer to the parameters
     */
    ContinuousTempering_TAMD(Param *p);

    /**
     * @brief Default Destructor
     */
    virtual ~ContinuousTempering_TAMD();

    /**@brief Position step for TAMD variables
     * @param dt time step in host units
     */
    void TAMDvars_PositionStep(double dt);

    /**computes difference between dihedral angles phi1 and phi2 modulo 2*pi.
     * @param phi1 first angle
     * @param phi2 second angle
     *
     * @return difference betwheen two angles, modulo 2*pi
     */
    double DihedralDiff(double phi1, double phi2);

    /**
     * @brief Integrate positions and velocities over time dt using
     * Langevin Dynamics
     *
     * @param dt the timestep in internal units
     */
    void Step(double dt);

    /**
     * @brief Setter for the system pointer
     *
     * @param s pointer to an instance of the System class
     */
    void SetSystem(System *s);

  protected:
    /**
     * @brief Computes the value and first derivative of the coupling function
     *
     * This function evaluates the value f(xi) of the coupling function and
     * returns it as 'cc', and
     * it evaluates the derivative d/dxi f(xi) and returns it as 'd_cc'. f(xi)
     * is a piecewise polynomial function which
     * is zero in the interval [-D,D], takes the value 'Tf' if |s|>D2 and
     * smoothly interpolates in between. See
     * the Continuous Tempering paper for more information.
     *
     * @param cc the value of the coupling function
     * @param d_cc the derivative of the coupling function w.r.t. xi
     * @param s the input value xi
     * @param D the range within which the function is 0
     * @param D2 the upper bound above which the function takes the value Tf
     * @param Tf the value of the function beyond D2
     */
    void SS_coupling(double *cc, double *d_cc, double s, double D, double D2,
                     double Tf);
    /**
     * @brief Computes the force acting of the extended variable xi
     *
     * Increments the force acting on xi
     * @param fs the force (added to by this function)
     * @param s_phi the dihedral angle phi
     * @param s_psi the dihedral angle psi
     * @param d_cc the derivative of the coupling function w.r.t. xi
     * @param a1 ...
     * @param a2 ...
     * @param h11 ...
     * @param h12 ...
     * @param h22 ...
     */
    void static_SSforce(double *fs, double s_phi, double s_psi, double d_cc,
                        double a1, double a2, double h11, double h12,
                        double h22);

    /**
     * @brief Computes metadynamics force and updates metadynamics potential
     *
     * This function deals with the metadynamics part of Continuous Tempering.
     * It does three things:
     * (1) The metadynamics biasing force (i.e. the derivative of the biasing
     * potential 'VG') is evaluated and
     * returned as 'fs'. (2) If addgauss==1, then the metadynamics potential
     * 'VG' is modified by adding another
     * gaussian with height 'hills_height' and width 'sigma' at the current
     * position 's' of the xi variable.
     * (3) If this function is called for the first time and read_bias_file==1,
     * then the potential 'VG' is read
     * from the file 'meta.0'.
     *
     * @param fs the force (added to by this function)
     * @param s the input value xi
     * @param addgauss if to add a new gaussian
     * @param D the range within which the function is 0
     * @param D2 the upper bound above which the function takes the value Tf
     */
    void metadyn(double *fs, double s, bool addgauss, double D, double D2);

    /**
     * @brief write the value of xi and the coupling function to file
     *
     * This function writes the position of xi and the value of the coupling
     * function
     * for the current frame into the file 'coupling.dat'. This happens every
     * save_conf frames
     *
     * @param cc the coupling function
     * @param xi the current value of xi
     */

    void write_confxyz(double cc, double xi);

    int phi_ind[4];   /**< @brief Atom indices defining dihedral phi **/
    int psi_ind[4];   /**< @brief Atom indices defining dihedral psi **/
    double m_s_inp;   /**< @brief mass of TAMD dofs in unit masses **/
    double kappa_inp; /**< @brief the TAMD coupling constant **/
    double fric_s;    /**< @brief the TAMD friction parameter **/
    double temp_s;    /**< @brief temperature of additional dof in K **/
    double kbt_s;     /**< @brief Boltzmann's constant * temp_s **/
    double m_s;       /**< @brief mass of the additional dof **/
    double kappa;     /**< @brief TAMD spring constant **/
    // additional dof positions, velocities and forces
    double s_phi;     /**< @brief phi dihedral angle **/
    double v_phi;     /**< @brief phi dihedral angular velocity **/
    double force_phi; /**< @brief phi dihedral torque **/
    double s_psi;     /**< @brief psi dihedral angle **/
    double v_psi;     /**< @brief psi dihedral angular velocity **/
    double force_psi; /**< @brief psi dihedral torque **/
    // BAOAB constraints
    double g_sdt; /**< @brief friction * timestep **/
    double c1_s;  /**< @brief c1 constant for additional dof integration **/
    double c3_s;  /**< @brief c3 constant for additional dof integration **/
    double sqrt_ms_inv; /**< @brief inverse square root of TAMD mass **/
    // tempering dof positions, velocities and forces
    double xi;           /**< @brief tempering position variable */
    double v_xi;         /**< @brief tempering velocity variable */
    double force_xi;     /**< @brief tempering force variable */
    double coupl;        /**< @brief value of the coupling function */
    double d_coupl;      /**< @brief derivative of the coupling function */
    double g_xidt;       /**< @brief tempering friction * timestep **/
    double c1_xi;        /**< @brief c1 constant for tempering integration **/
    double c3_xi;        /**< @brief c3 constant for tempering integration **/
    double sqrt_mxi_inv; /**< @brief inverse square root of tempering mass **/
    // continuous tempering service variables
    double Tfact;   /**< @brief temperature rescaling factor */
    double m_xi;    /**< @brief mass of the tempering variable */
    double fric_xi; /**< @brief friction for the tempering variable */
    double Delta;   /**< @brief absolute value below which the coupling function
                       is 0 */
    double Delta2;  /**< @brief absolute value at which the coupling function
                       reaches its maximum */
    double hills_height;    /**< @brief height of the hills for metadynamics */
    double sigma; /**< @brief width of the hills for metadynamics */
    int meta_dep; /**< @brief number of steps between hill deposition */
    // this should be transformed into bool
    int read_bias_file; /**< @brief read an initial bias from file */
    int n_approx; /**< @brief the window we use for quadratic approximation of
                     free energy */
};

#endif
