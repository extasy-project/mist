/*******************************************************************************
*                                                                              *
*             MIST - Molecular Integration Simulation Toolkit                  *
*         A library of integration algorithms for Molecular Dynamics           *
*                                                                              *
*            Copyright (c) 2014-2020, The University of Edinburgh              *
*                            All rights reserved.                              *
*                     List of contributors in AUTHORS                          *
*                                                                              *
*            MIST is free software.  See LICENSE for details                   *
*                                                                              *
*******************************************************************************/

/*
 * @file ContinuousTempering.cpp
 * @brief Implementation of Continuous Tempering Integrator
 * @author GG, RB
 */

#include "ContinuousTempering.h"
#include "ConstraintSolver.h"
#include "IO.h"
#include "System.h"

#include <cmath>
#include <fstream>
#include <iomanip>
#include <stdlib.h>

#define BOLTZMANN_KJoverMOL                                                    \
    0.0083144621 /**< @brief Boltzmann's constant in kj/mol */

ContinuousTempering::ContinuousTempering(Param *p)
    : LangevinIntegrator(p, "Continuous Tempering")
{
    // Default values for input parameters
    Tfact = 0.5;
    pe_offset = 0.0;
    m_ss = 0.004;
    fric_ss = 1.0;
    Delta = 50;
    Delta2 = 100;
    hills_height = 7.5;
    sigma = 3.05;
    meta_dep = 1000;
    save_conf = 500;
    read_bias_file = 0;

    // Only need to set this if we call GetForce for a particular component,
    // otherwise leave it turned off to save work in the force routines
    // features |= MIST_FEATURE_FORCE_COMPONENTS;
    features |= MIST_FEATURE_REQUIRES_ENERGY_WITH_FORCES;

    // Read in parameters from mist.params file
    ReadDoubleParam(p, &Tfact, "temp_fact", "");
    ReadDoubleParam(p, &m_ss, "mass_xi", "");
    ReadDoubleParam(p, &fric_ss, "langfriction_xi", "ps^-1");
    ReadDoubleParam(p, &Delta, "delta", "angstrom");
    ReadDoubleParam(p, &Delta2, "delta2", "angstrom");
    ReadDoubleParam(p, &hills_height, "hills-height", "kJ/mol");
    ReadDoubleParam(p, &sigma, "hills-width", "UNITS ??");
    ReadIntParam(p, &meta_dep, "metadyn-pace", "");
    ReadIntParam(p, &save_conf, "save-config-freq", "");
    // this must be changed in order to read a bool value in input
    ReadIntParam(p, &read_bias_file, "read-bias-file", "");
    ReadDoubleParam(p, &pe_offset, "peoffset", "UNITS ??");

    // convert from kj/mol input energy value to MDengine energy units
    pe_offset =
        pe_offset * system->Boltzmann() / BOLTZMANN_KJoverMOL;
    m_ss = m_ss * system->AMU();

    // gaussian height and width:
    hills_height = hills_height * system->Boltzmann() /
                                     BOLTZMANN_KJoverMOL;
}

ContinuousTempering::~ContinuousTempering() {}

void ContinuousTempering::Step(double dt)
{
    // Langevin BAOAB Scheme (from CM's NAMD-lite implementation)
    // additional deg. of freed. (=ss) and other necessary var.
    static int iter = 0;
    static double ss = 0; // 0;
    static double v_ss = 0;
    static double force_ss = 0; // Also this must be dec. as static since it is

    // needed for the first half (B) kick!
    static double coupl = 0,
                  d_coupl = 0; // static for the same reason as force_ss

    // BAOAB xi variables and service variables
    double g_ssdt = fric_ss / system->Picosecond() *
                    dt; // dimensionless friction constant for xi
    double c1_ss = exp(-g_ssdt);
    double c3_ss = sqrt(kbt * (1 - c1_ss * c1_ss));
    double l_char_inv =
        1 / system->Angstrom(); // this is 1/[1 angstrom] in internal units
    int i;
    double m_inv;

    // Velocity half-step with forces rescaled by coupling function (B)
    VelocityStep(0.5 * dt * (1.0 - coupl));
    ResolveVelocityConstraints(0.5 * dt);

    // velocity step for additional dof. For unit consistency, need to divide
    // force_ss by a length scale [in internal units]
    v_ss += dt * 0.5 / m_ss * force_ss * l_char_inv;

    // Position half step (A)
    PositionStep(0.5 * dt);
    ResolvePositionConstraints(0.5 * dt);
    ResolveVelocityConstraints(0.5 * dt);
    ss += l_char_inv * dt * 0.5 * v_ss;

    // Velocity update (O)
    OStep(dt);
    ResolveVelocityConstraints(dt);

    // Velocity update for xi
    v_ss = c1_ss * v_ss + sqrt(1.0 / m_ss) * c3_ss * r[0]->random_gaussian();
#ifdef __MIST_WITH_MPI
    // In parallel, we just take the master's value i.e. v_ss is equal on all
    // processes
    MPI_Bcast(&v_ss, 1, MPI_DOUBLE, 0, system->GetCommunicator());
#endif

    // Position half step (A)
    PositionStep(0.5 * dt);
    ResolvePositionConstraints(0.5 * dt);
    ResolveVelocityConstraints(0.5 * dt);
    ss += l_char_inv * dt * 0.5 * v_ss;

    system->UpdateForces();

    // compute the force force_ss acting on xi
    SS_coupling(&coupl, &d_coupl, ss, Delta, Delta2, Tfact);

    // must be initialized to zero as functions only add contributions
    force_ss = 0;
    static_SSforce(&force_ss, coupl, d_coupl, pe_offset);

    // compute metadynamics contribution to f_ss
    if (iter % meta_dep == 0)
        metadyn(&force_ss, ss, 1, Delta, Delta2);
    else
        metadyn(&force_ss, ss, 0, Delta, Delta2);

    // Velocity half-step with forces rescaled by coupling function (B)
    VelocityStep(0.5 * dt * (1.0 - coupl));
    ResolveVelocityConstraints(0.5 * dt);

    // velocity step for additional dof. For unit consistency, need to divide
    // force_ss by a length scale [in internal units]
    v_ss += dt * 0.5 / m_ss * force_ss * l_char_inv;

    // I think this is necessary to be synchronized with Gromacs when saving
    // conf.
    if (iter % save_conf == save_conf - 1)
        write_confxyz(coupl, ss, iter + 1, force_ss * l_char_inv);
    iter += 1;
}

void ContinuousTempering::SS_coupling(double *cc, double *d_cc, double s,
                                      double D, double D2, double Tf)
{
    if (std::abs(s) <= D)
    {
        *cc = 0;
        *d_cc = 0;
    }
    else if ((D < std::abs(s)) && (std::abs(s) < D + D2))
    {
        *cc = Tf * (3 * pow((std::abs(s) - D) / D2, 2) -
                    2 * pow((std::abs(s) - D) / D2, 3));
        if (s > 0)
            *d_cc =
                6 * Tf *
                (((std::abs(s) - D) / D2) - pow((std::abs(s) - D) / D2, 2)) /
                D2;
        else
            *d_cc =
                -6 * Tf *
                (((std::abs(s) - D) / D2) - pow((std::abs(s) - D) / D2, 2)) /
                D2;
    }
    else
    {
        *cc = Tf;
        *d_cc = 0;
    }
}

void ContinuousTempering::static_SSforce(double *fs, double cc, double d_cc,
                                         double peoff)
{
    // contrib. due to coupling
    double pe = system->GetPotentialEnergy(); // read potential energy
    *fs += d_cc * (pe - peoff); // V_extended_sys=(V-peoff)-cc*(V-peoff)
    // term compensating approximately for temperature change
    *fs += -d_cc * cc / (1 - cc) * 18738.0; // kbt*2500.0*3.2;
}

void ContinuousTempering::metadyn(double *fs, double s, bool addgauss, double D,
                                  double D2)
{
#ifdef __MIST_WITH_MPI
    // Do everything on the rank 0 in MPI, then broadcast the result
    int rank;
    MPI_Comm_rank(system->GetCommunicator(), &rank);
    if (rank == 0)
    {
#endif
        // how many gaussians are deposited before saving bias potential to
        // file:
        const int metastride = 1000;
        const int NG = 9000;
        static double VG[NG + 2] = {0};
        static int it = 0;
        const double smax = 1.03 * (D + D2);
        const double smin = -smax;
        const double dsg = (smax - smin) / (NG - 1);
        const double swp = smax + dsg;
        const double swm = smin - dsg;
        static bool isfirst = 1;

        // read input bias potential
        if (isfirst && (read_bias_file == 1))
        {
            isfirst = 0;
            std::ifstream biasfile;
            biasfile.open("meta.0");
            int count_lines = 0;
            std::string inputline;
            while (getline(biasfile, inputline))
                count_lines++;
            if ((NG + 2) != count_lines)
            {
                IO::Error("Incompatible number of lines in input bias "
                          "potential file meta.0 "
                          "number of lines must equal the size of the internal "
                          "grid which is %d\n",
                          NG + 2);
                exit(0);
            }
            biasfile.clear();
            biasfile.seekg(0);
            for (int ig = 0; ig < NG + 2; ig++)
            {
                double cc;
                biasfile >> cc >> VG[ig];
            }
            biasfile.close();
        }

        // add gaussian
        if (addgauss)
        {
            it = it + 1;
            for (int ig = 1; ig < NG + 1; ig++)
            {
                VG[ig] = VG[ig] +
                         hills_height *
                             exp(-0.5 *
                                 pow((s - (swm + (ig - 1) * dsg)) / sigma, 2)) +
                         // adding the same hill on the symmetric position
                         hills_height *
                             exp(-0.5 *
                                 pow((-s - (swm + (ig - 1) * dsg)) / sigma, 2));
            }
            // write metadyn bias potential
            if (it % metastride == 0)
            {
                char filename[15];
                sprintf(filename, "meta.%d", (it / metastride));
                std::ofstream biasfile;
                biasfile.open(filename);
                for (int ig = 0; ig < NG + 2; ig++)
                {
                    biasfile << std::setw(15) << swm + (ig - 1) * dsg
                             << std::setw(20) << VG[ig] << std::endl;
                }
                biasfile.close();
            }
        }

        // evaluate bias force
        if ((s > swm) && (s < swp))
        {
            // Bin evaluation
            int ig = (int)ceil((s - swm) / dsg);
            // evaluate force
            if ((ig >= 1) && (ig < NG))
                *fs += -(VG[ig + 1] - VG[ig]) / dsg;
        }
        // confining potential term at the border
        if (std::abs(s) > 1.03 * (D + D2))
        {
            *fs += -(s / std::abs(s)) * 25.0 * kbt *
                   (std::abs(s) - 1.03 * (D + D2)) / D;
        }

#ifdef __MIST_WITH_MPI
    }
    // Broadcast the biasing force computed on rank zero
    MPI_Bcast(fs, 1, MPI_DOUBLE, 0, system->GetCommunicator());
#endif
}

void ContinuousTempering::write_confxyz(double cc, double s, int it, double fss)
{
    // this function writes the position of xi and the value of the coupling
    // function
    // for the current frame into the file 'coupling.dat'. This happens every
    // save_conf
    // frames.

#ifdef __MIST_WITH_MPI
    // Do everything on the rank 0 in MPI
    int rank;
    MPI_Comm_rank(system->GetCommunicator(), &rank);
    if (rank != 0)
        return;
#endif

    static bool isfirst = 1;
    static std::ofstream couplingoutfile;

    // output files initialization
    if (isfirst)
    {
        couplingoutfile.open("coupling.dat");
        isfirst = 0;
    }

    // write out position of xi (=s) and value of coupling function (=cc)
    double pe = system->GetPotentialEnergy(); // read potential energy
    couplingoutfile << it << "\t" << s << "\t" << cc << "\t" << pe << "\t"
                    << fss << "\t" << std::endl;
    couplingoutfile.flush();
}
