/*******************************************************************************
*                                                                              *
*             MIST - Molecular Integration Simulation Toolkit                  *
*         A library of integration algorithms for Molecular Dynamics           *
*                                                                              *
*            Copyright (c) 2014-2020, The University of Edinburgh              *
*                            All rights reserved.                              *
*                     List of contributors in AUTHORS                          *
*                                                                              *
*            MIST is free software.  See LICENSE for details                   *
*                                                                              *
*******************************************************************************/

/**
 * @file GromacsSystem.cpp
 * @brief Implementation of System interface for Gromacs
 */

#include "GromacsSystem.h"
#include "IO.h"
#include <cmath>
#include <stddef.h>
#include <strings.h>

// Implementation of the Gromacs System class
// Designed for use with GROMACS 5.0.2

GromacsSystem::GromacsSystem() : System()
{
    // Gromacs specific conversion factors
    boltzmann = 0.0083144621; // internal units are kJ/mol/K
    picosecond = 1.0;         // internal units are ps
    amu = 1.0;                // internal units are AMU
    angstrom = 0.1;           // internal units are nm
    atm = 1.0 / 0.987;        // internal units are bar

    cell_inv[0] = Vector3(0.0, 0.0, 0.0);
    cell_inv[1] = Vector3(0.0, 0.0, 0.0);
    cell_inv[2] = Vector3(0.0, 0.0, 0.0);
}

GromacsSystem::~GromacsSystem()
{
    if (masses != NULL)
    {
        delete[] masses;
        masses = NULL;
    }

    if (invmasses != NULL)
    {
        delete[] invmasses;
        invmasses = NULL;
    }

    if (invsqrtmasses != NULL)
    {
        delete[] invsqrtmasses;
        invsqrtmasses = NULL;
    }
}

Vector3 GromacsSystem::GetVelocity(int i) const
{
    real x = ((real *)velocities)[3 * i];
    real y = ((real *)velocities)[3 * i + 1];
    real z = ((real *)velocities)[3 * i + 2];
    return Vector3(x, y, z);
}

void GromacsSystem::SetVelocity(int i, Vector3 v)
{
    ((real *)velocities)[3 * i] = v.x;
    ((real *)velocities)[3 * i + 1] = v.y;
    ((real *)velocities)[3 * i + 2] = v.z;
}

Vector3 GromacsSystem::GetPosition(int i) const
{
    real x = ((real *)positions)[3 * i];
    real y = ((real *)positions)[3 * i + 1];
    real z = ((real *)positions)[3 * i + 2];
    return Vector3(x, y, z);
}

void GromacsSystem::SetPosition(int i, Vector3 p)
{
    ((real *)positions)[3 * i] = p.x;
    ((real *)positions)[3 * i + 1] = p.y;
    ((real *)positions)[3 * i + 2] = p.z;
}

double GromacsSystem::GetPressure() const
{
    if (pbc != true)
        return 0.0; // Pressure is not defined for non-periodic systems

    // TODO check
    return *(double *)pressure;
}

double GromacsSystem::GetPotentialEnergy() const
{
    return GetPotentialEnergy(MIST_FORCE_ALL);
}

double GromacsSystem::GetPotentialEnergy(force_component_t t) const
// Set variables needed by GROMACS to compute force component
{
    double e;
    switch (t)
    {
    case (MIST_FORCE_ALL):
        e = (real)((gmx_enerdata_t *)potential_energy)->term[F_EPOT];
        break;
    case (MIST_FORCE_BOND):
        e = (real)((gmx_enerdata_t *)potential_energy)->term[F_BONDS] +
            ((gmx_enerdata_t *)potential_energy)->term[F_G96BONDS] +
            ((gmx_enerdata_t *)potential_energy)->term[F_MORSE] +
            ((gmx_enerdata_t *)potential_energy)->term[F_CUBICBONDS] +
            ((gmx_enerdata_t *)potential_energy)->term[F_CONNBONDS] +
            ((gmx_enerdata_t *)potential_energy)->term[F_HARMONIC] +
            ((gmx_enerdata_t *)potential_energy)->term[F_FENEBONDS] +
            ((gmx_enerdata_t *)potential_energy)->term[F_TABBONDS] +
            ((gmx_enerdata_t *)potential_energy)->term[F_TABBONDSNC] +
            ((gmx_enerdata_t *)potential_energy)->term[F_RESTRBONDS];
        break;
    case (MIST_FORCE_BEND):
        e = (real)((gmx_enerdata_t *)potential_energy)->term[F_ANGLES] +
            ((gmx_enerdata_t *)potential_energy)->term[F_G96ANGLES] +
            ((gmx_enerdata_t *)potential_energy)->term[F_RESTRANGLES] +
            ((gmx_enerdata_t *)potential_energy)->term[F_LINEAR_ANGLES] +
            ((gmx_enerdata_t *)potential_energy)->term[F_CROSS_BOND_BONDS] +
            ((gmx_enerdata_t *)potential_energy)->term[F_CROSS_BOND_ANGLES] +
            ((gmx_enerdata_t *)potential_energy)->term[F_UREY_BRADLEY] +
            ((gmx_enerdata_t *)potential_energy)->term[F_QUARTIC_ANGLES] +
            ((gmx_enerdata_t *)potential_energy)->term[F_TABANGLES];
        break;
    case (MIST_FORCE_PROPDIHED):
        e = (real)((gmx_enerdata_t *)potential_energy)->term[F_PDIHS] +
            ((gmx_enerdata_t *)potential_energy)->term[F_RBDIHS] +
            ((gmx_enerdata_t *)potential_energy)->term[F_RESTRDIHS] +
            ((gmx_enerdata_t *)potential_energy)->term[F_CBTDIHS] +
            ((gmx_enerdata_t *)potential_energy)->term[F_FOURDIHS] +
            ((gmx_enerdata_t *)potential_energy)->term[F_CMAP] +
            ((gmx_enerdata_t *)potential_energy)->term[F_TABDIHS];
        break;
    case (MIST_FORCE_IMPROPDIHED):
        e = (real)((gmx_enerdata_t *)potential_energy)->term[F_IDIHS] +
            ((gmx_enerdata_t *)potential_energy)->term[F_PIDIHS];
        break;
    case (MIST_FORCE_NONBONDED):
        e = GetPotentialEnergy(MIST_FORCE_ALL) -
            GetPotentialEnergy(MIST_FORCE_PROPDIHED) -
            GetPotentialEnergy(MIST_FORCE_IMPROPDIHED) -
            GetPotentialEnergy(MIST_FORCE_BEND) -
            GetPotentialEnergy(MIST_FORCE_BOND);
        break;
    default:
        e = 0.0;
        break;
    }

    return e;
}

Vector3 GromacsSystem::GetForce(int i) const
{
    return GetForce(i, MIST_FORCE_ALL);
}

Vector3 GromacsSystem::GetForce(int i, force_component_t t) const
{
    real x, y, z;

    switch (t)
    {
    case (MIST_FORCE_ALL):
        x = ((force_arrays_t *)forces)->f_total[i][0];
        y = ((force_arrays_t *)forces)->f_total[i][1];
        z = ((force_arrays_t *)forces)->f_total[i][2];
        break;
    case (MIST_FORCE_BOND):
        x = ((force_arrays_t *)forces)->f_bond[i][0];
        y = ((force_arrays_t *)forces)->f_bond[i][1];
        z = ((force_arrays_t *)forces)->f_bond[i][2];
        break;
    case (MIST_FORCE_BEND):
        x = ((force_arrays_t *)forces)->f_angle[i][0];
        y = ((force_arrays_t *)forces)->f_angle[i][1];
        z = ((force_arrays_t *)forces)->f_angle[i][2];
        break;
    case (MIST_FORCE_PROPDIHED):
        x = ((force_arrays_t *)forces)->f_propdihed[i][0];
        y = ((force_arrays_t *)forces)->f_propdihed[i][1];
        z = ((force_arrays_t *)forces)->f_propdihed[i][2];
        break;
    case (MIST_FORCE_IMPROPDIHED):
        x = ((force_arrays_t *)forces)->f_impropdihed[i][0];
        y = ((force_arrays_t *)forces)->f_impropdihed[i][1];
        z = ((force_arrays_t *)forces)->f_impropdihed[i][2];
        break;
    case (MIST_FORCE_NONBONDED):
        x = ((force_arrays_t *)forces)->f_nonbonded[i][0];
        y = ((force_arrays_t *)forces)->f_nonbonded[i][1];
        z = ((force_arrays_t *)forces)->f_nonbonded[i][2];
        break;
    default:
        x = 0.0;
        y = 0.0;
        z = 0.0;
        break;
    }

    return Vector3(x, y, z);
}

void GromacsSystem::SetMassPtr(void *m, int massrequirements)
{
    int i;

    if (numParticles < 1)
        return;

    if (massrequirements & MIST_MASS_REQD)
    {
        if (masses != NULL)
        {
            delete[] masses;
        }
        masses = new double[numParticles];
        for (i = 0; i < numParticles; i++)
        {
            masses[i] = 1. / (((real *)m)[i]);
        }
    }

    if (massrequirements & MIST_INV_MASS_REQD)
    {
        if (invmasses != NULL)
        {
            delete[] invmasses;
        }
        invmasses = new double[numParticles];
        for (i = 0; i < numParticles; i++)
        {
            invmasses[i] = (((real *)m)[i]);
        }
    }

    if (massrequirements & MIST_INVSQRT_MASS_REQD)
    {
        if (invsqrtmasses != NULL)
        {
            delete[] invsqrtmasses;
        }
        invsqrtmasses = new double[numParticles];
        for (i = 0; i < numParticles; i++)
        {
            invsqrtmasses[i] = sqrt(double(((real *)m)[i]));
        }
    }
}

double GromacsSystem::GetMass(int i) const { return masses[i]; }

double GromacsSystem::GetInverseMass(int i) const { return invmasses[i]; }

double GromacsSystem::GetInverseSqrtMass(int i) const
{
    return invsqrtmasses[i];
}

/**
 * Loops over the Gromacs topology data structure gmx_mtop_t to identify the
 *i'th atom.  Outer loop over molecule blocks, then molecules within the block,
 *then atoms within the molecule.  Keep a running count of atoms until we find
 *the one we are looking for, then return it's kind label (atomname).
 *
 * Note that this is O(n) so will make loops over atoms calling GetKind() O(n^2)
 *and
 * so should be avoided, or eventually precached.
 */
char *GromacsSystem::GetKind(int i) const
{
    int count = 0;
    t_atoms *tat;
    gmx_mtop_t *top = (gmx_mtop_t *)kinds;

    for (int iblock = 0; iblock < top->nmolblock; iblock++)
    {
        tat = &top->moltype[top->molblock[iblock].type].atoms;
        for (int imol = 0; imol < top->molblock[iblock].nmol; imol++)
        {
            for (int iatom = 0; iatom < tat->nr; iatom++)
            {
                int prevCount = count;
                count += 1; // Number of atoms in this molecule
                if (prevCount == i)
                {
                    return (char *)(*(tat->atomname[iatom]));
                }
            }
        }
    }

    IO::Error("Could not find a kind for atom %d\n", i);

    return NULL;
}

void GromacsSystem::GetCell(Vector3 *a, Vector3 *b, Vector3 *c)
{

    if (pbc != true)
    {
        IO::Error("Called GetCell() when PBC are off!\n");
        return;
    }

    // GROMACS storage format:
    // box[a,b,c][x,y,z] (row major)

    real *box = (real *)cell;

    a->x = (double)box[0];
    a->y = (double)box[1];
    a->z = (double)box[2];

    b->x = (double)box[3];
    b->y = (double)box[4];
    b->z = (double)box[5];

    c->x = (double)box[6];
    c->y = (double)box[7];
    c->z = (double)box[8];
}

void GromacsSystem::GetCellInverse(Vector3 *a, Vector3 *b, Vector3 *c)
{
    if (pbc != true)
    {
        IO::Error("Called GetCellInverse() when PBC are off!\n");
        return;
    }

    // If we don't have the inverse stored,
    // compute it, assuming H is upper-triangular
    if (cell_inv[0].x == 0.0)
    {
        GetCell(a, b, c);
        cell_inv[0].x = 1.0 / a->x;
        cell_inv[1].x = -b->x / (a->x * b->y);
        cell_inv[1].y = 1.0 / b->y;
        cell_inv[2].x = (b->x * c->y - c->x * b->y) / (a->x * b->y * c->z);
        cell_inv[2].y = -c->y / (c->z * b->y);
        cell_inv[2].z = 1.0 / c->z;
    }

    a->x = cell_inv[0].x;
    a->y = cell_inv[0].y;
    a->z = cell_inv[0].z;

    b->x = cell_inv[1].x;
    b->y = cell_inv[1].y;
    b->z = cell_inv[1].z;

    c->x = cell_inv[2].x;
    c->y = cell_inv[2].y;
    c->z = cell_inv[2].z;
}

void GromacsSystem::SetCell(Vector3 a, Vector3 b, Vector3 c)
{
    if (pbc != true)
    {
        IO::Error("Called SetCell() when PBC are off!\n");
        return;
    }

    // Check new vectors are valid
    if (a.y != 0.0 || a.z != 0.0 || b.z != 0.0)
    {
        IO::Error("New box vectors are not orthorhombic or triclinic!\n");
        return;
    }

    Vector3 a_inv, b_inv, c_inv;
    GetCellInverse(&a_inv, &b_inv, &c_inv);

    // Convert to lambda coordinates
    Vector3 p;
    for (int i = 0; i < numParticles; i++)
    {
        p = GetPosition(i);
        p.x = a_inv.x * p.x + b_inv.x * p.y + c_inv.x * p.z;
        p.y = b_inv.y * p.y + c_inv.y * p.z;
        p.z = c_inv.z * p.z;
        SetPosition(i, p);
    }

    // Update the box and recompute the inverse
    real *box = (real *)cell;

    box[0] = (real)a.x;

    box[3] = (real)b.x;
    box[4] = (real)b.y;

    box[6] = (real)c.x;
    box[7] = (real)c.y;
    box[8] = (real)c.z;

    cell_inv[0].x = 1.0 / a.x;
    cell_inv[1].x = -b.x / (a.x * b.y);
    cell_inv[1].y = 1.0 / b.y;
    cell_inv[2].x = (b.x * c.y - c.x * b.y) / (a.x * b.y * c.z);
    cell_inv[2].y = -c.y / (c.z * b.y);
    cell_inv[2].z = 1.0 / c.z;

    // Convert back to real positions
    for (int i = 0; i < numParticles; i++)
    {
        p = GetPosition(i);
        p.x = a.x * p.x + b.x * p.y + c.x * p.z;
        p.y = b.y * p.y + c.y * p.z;
        p.z = c.z * p.z;
        SetPosition(i, p);
    }
}
