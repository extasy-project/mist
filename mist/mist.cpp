/*******************************************************************************
*                                                                              *
*             MIST - Molecular Integration Simulation Toolkit                  *
*         A library of integration algorithms for Molecular Dynamics           *
*                                                                              *
*            Copyright (c) 2014-2020, The University of Edinburgh              *
*                            All rights reserved.                              *
*                     List of contributors in AUTHORS                          *
*                                                                              *
*            MIST is free software.  See LICENSE for details                   *
*                                                                              *
*******************************************************************************/

/**
 * @file mist.cpp
 * @brief Implementation of the MIST library interface
 */

#include "mist.h"
#include "../config.h"
#include "strcasecmp.h"

#include "ContinuousTempering.h"
#include "ContinuousTempering_TAMD.h"
#include "IO.h"
#include "ISST.h"
#include "LangevinIntegrator.h"
#include "LeapfrogIntegrator.h"
#include "NoseHooverIntegrator.h"
#include "RK4.h"
#include "SimulatedTempering.h"
#include "TAMD.h"
#include "Vector3.h"
#include "VerletIntegrator.h"
#include "Yoshida4Integrator.h"
#include "Yoshida8Integrator.h"

#if defined(__MIST_WITH_NAMD_LITE)
#include "NAMDLiteSystem.h"
#elif defined(__MIST_WITH_FORCITE)
#include "ForciteSystem.h"
#elif defined(__MIST_WITH_GROMACS)
#include "GromacsSystem.h"
#elif defined(__MIST_WITH_AMBER)
#include "AmberSystem.h"
#elif defined(__MIST_WITH_LAMMPS)
#include "LAMMPSSystem.h"
#endif

#include <strings.h>

#define MIST_PARAM_FILE                                                        \
    "mist.params" /**< @brief Name of the mist parameters file. **/
#define INTEGRATOR_TYPE_LABEL                                                  \
    "integrator" /**< @brief Label in the params file that selects the         \
                    integrator. **/

// Definition of the global integrator and parameter objects (file scope).
static Integrator *integrator = NULL;
static Param *params = NULL;

#ifdef __MIST_WITH_MPI

int MIST_Init_MPI(MPI_Comm comm)
{
    if (comm == MPI_COMM_NULL)
    {
        IO::Error("Invalid MPI communicator.\n");
        return MIST_INVALID_VALUE_IN;
    }

    // Initialise IO for parallel usa
    IO::SetCommunicator(comm);

    // Do all of the non-MPI-related initialisation
    int retval = MIST_Init();
    if (retval)
        return retval;

    // Store the communicator internally
    integrator->GetSystem()->SetCommunicator(comm);

    IO::Info("Waiting for all processes to call MIST_Init_MPI()...\n");
    MPI_Barrier(comm);

    return 0;
}

int MIST_Init_MPI_Fortran(MPI_Fint fcomm)
{
    MPI_Comm ccomm = MPI_Comm_f2c(fcomm);
    return MIST_Init_MPI(ccomm);
}
#endif

int MIST_Init()
{
    int err;
    char *type;

    if (params == NULL)
    {
        IO::Info("Reading parameters from file: %s\n", MIST_PARAM_FILE);
        // Read and allocate params.
        err = readParams(MIST_PARAM_FILE, &params);

        if (err == ERR_NOFILE)
        {
            IO::Error("No input file mist.params could be found.\n");
            return MIST_NO_SETUP_FILE;
        }
        else if (err != ERR_OK)
        {
            IO::Error("Unknown file open error, type: %d\n", err);
            return MIST_NO_SETUP_FILE;
        }
    }
    else
    {
        IO::Info("Using parameters set by host application\n");
    }

    // Check which integrator type has been selected and initialise it.
    err = getParamStringValue(params, INTEGRATOR_TYPE_LABEL, &type);
    if (err != ERR_OK)
    {
        IO::Error("No integrator type was identified.\n");
        return MIST_INVALID_INTEGRATOR;
    }
    else if (strcasecmp(type, "verlet") == 0)
    {
        integrator = new VerletIntegrator(params);
    }
    else if (strcasecmp(type, "nose-hoover") == 0)
    {
        integrator = new NoseHooverIntegrator(params);
    }
    else if (strcasecmp(type, "leapfrog") == 0)
    {
        integrator = new LeapfrogIntegrator(params);
    }
    else if (strcasecmp(type, "langevin") == 0)
    {
        integrator = new LangevinIntegrator(params);
    }
    else if (strcasecmp(type, "ISST") == 0)
    {
        integrator = new ISST(params);
    }
    else if (strcasecmp(type, "yoshida4") == 0)
    {
        integrator = new Yoshida4Integrator(params);
    }
    else if (strcasecmp(type, "yoshida8") == 0)
    {
        integrator = new Yoshida8Integrator(params);
    }
    else if (strcasecmp(type, "RK4") == 0)
    {
        integrator = new RK4Integrator(params);
    }
    else if (strcasecmp(type, "tempering") == 0)
    {
        integrator = new ContinuousTempering(params);
    }
    else if (strcasecmp(type, "tamd") == 0)
    {
        integrator = new TAMD(params);
    }
    else if (strcasecmp(type, "tempering_tamd") == 0)
    {
        integrator = new ContinuousTempering_TAMD(params);
    }
    else if (strcasecmp(type, "simulated_tempering") == 0)
    {
        integrator = new SimulatedTempering(params);
    }
    else
    {
        IO::Error("Your choice ( %s ) for the integrator is unknown.\n", type);
        return MIST_INVALID_INTEGRATOR;
    }

    if (integrator == NULL)
    {
        IO::Error("Integrator object not allocated.\n");
        return MIST_INVALID_INTEGRATOR;
    }

    // Initialise the system object, attach it to the integrator.
    System *s = NULL;
#if defined(__MIST_WITH_NAMD_LITE)
    s = new NAMDLiteSystem();
#elif defined(__MIST_WITH_FORCITE)
    s = new ForciteSystem();
#elif defined(__MIST_WITH_LAMMPS)
    s = new LAMMPSSystem();
#elif defined(__MIST_WITH_GROMACS)
    s = new GromacsSystem();
#elif defined(__MIST_WITH_AMBER)
    if ((integrator->GetFeatures()) & MIST_FEATURE_FORCE_COMPONENTS)
    {
        IO::Error("The integrator you have selected requires force "
                  "components, but this option is not available in AMBER.\n");
        return MIST_INVALID_OPTION;
    }
    s = new AmberSystem();
#endif

    if (s == NULL)
    {
        IO::Error("System object not allocated.\n");
        return MIST_INVALID_SYSTEM;
    }

    integrator->SetSystem(s);

    IO::Info("Initialised MIST Library [Version " PACKAGE_VERSION
             " (" GITVERSION ")]\n");
    return MIST_OK;
}

int MIST_SetNumParticles(int n)
{
    if (n < 0)
    {
        IO::Error("Negative particle number specified.\n");
        return MIST_INVALID_VALUE_IN;
    }

    if (integrator == NULL)
    {
        IO::Error("Integrator not defined.\n");
        return MIST_INVALID_INTEGRATOR;
    }

    if (integrator->GetSystem() == NULL)
    {
        IO::Error("System object not defined.\n");
        return MIST_INVALID_SYSTEM;
    }
    integrator->GetSystem()->SetNumParticles(n);
    return MIST_OK;
}

int MIST_SetCell(void *cell)
{

    if (integrator == NULL)
    {
        IO::Error("Integrator not defined.\n");
        return MIST_INVALID_INTEGRATOR;
    }

    if (integrator->GetSystem() == NULL)
    {
        IO::Error("System object not defined.\n");
        return MIST_INVALID_SYSTEM;
    }

    if (cell == NULL)
    {
        IO::Warning("cell pointer was NULL!  Simulation will be treated as "
                    "non-periodic.\n");
    }
    integrator->GetSystem()->SetCellPtr(cell);

    return MIST_OK;
}

int MIST_SetPositions(void *positions)
{
    if (positions == NULL)
    {
        IO::Error("positions pointer was NULL.\n");
        return MIST_INVALID_POINTER_IN;
    }

    if (integrator == NULL)
    {
        IO::Error("Integrator not defined.\n");
        return MIST_INVALID_INTEGRATOR;
    }

    if (integrator->GetSystem() == NULL)
    {
        IO::Error("System object not defined.\n");
        return MIST_INVALID_SYSTEM;
    }

    integrator->GetSystem()->SetPositionPtr(positions);
    return MIST_OK;
}

int MIST_SetVelocities(void *velocities)
{

    if (velocities == NULL)
    {
        IO::Error("velocities pointer was NULL.\n");
        return MIST_INVALID_POINTER_IN;
    }

    if (integrator == NULL)
    {
        IO::Error("Integrator not defined.\n");
        return MIST_INVALID_INTEGRATOR;
    }

    if (integrator->GetSystem() == NULL)
    {
        IO::Error("System object not defined.\n");
        return MIST_INVALID_SYSTEM;
    }
    integrator->GetSystem()->SetVelocityPtr(velocities);
    return MIST_OK;
}

int MIST_SetMasses(void *masses)
{
    if (masses == NULL)
    {
        IO::Error("masses pointer was NULL.\n");
        return MIST_INVALID_POINTER_IN;
    }

    if (integrator == NULL)
    {
        IO::Error("Integrator not defined.\n");
        return MIST_INVALID_INTEGRATOR;
    }

    if (integrator->GetSystem() == NULL)
    {
        IO::Error("System object not defined.\n");
        return MIST_INVALID_SYSTEM;
    }

    integrator->GetSystem()->SetMassPtr(masses,
                                        integrator->GetMassRequirements());

    return MIST_OK;
}

int MIST_SetKinds(void *types)
{
    if (types == NULL)
    {
        IO::Error("types pointer was NULL.\n");
        return MIST_INVALID_POINTER_IN;
    }

    if (integrator == NULL)
    {
        IO::Error("Integrator not defined.\n");
        return MIST_INVALID_INTEGRATOR;
    }

    if (integrator->GetSystem() == NULL)
    {
        IO::Error("System object not defined.\n");
        return MIST_INVALID_SYSTEM;
    }

    integrator->GetSystem()->SetKindsPtr(types);
    return MIST_OK;
}

int MIST_SetPotentialEnergy(void *potential_energy)
{

    if (potential_energy == NULL)
    {
        IO::Error("potential_energy pointer was NULL.\n");
        return MIST_INVALID_POINTER_IN;
    }

    if (integrator == NULL)
    {
        IO::Error("Integrator not defined.\n");
        return MIST_INVALID_INTEGRATOR;
    }

    if (integrator->GetSystem() == NULL)
    {
        IO::Error("System object not defined.\n");
        return MIST_INVALID_SYSTEM;
    }

    integrator->GetSystem()->SetPotentialEnergyPtr(potential_energy);
    return MIST_OK;
}

int MIST_SetPressure(void *pressure)
{

    if (pressure == NULL)
    {
        IO::Error("pressure pointer was NULL.\n");
        return MIST_INVALID_POINTER_IN;
    }

    if (integrator == NULL)
    {
        IO::Error("Integrator not defined.\n");
        return MIST_INVALID_INTEGRATOR;
    }

    if (integrator->GetSystem() == NULL)
    {
        IO::Error("System object not defined.\n");
        return MIST_INVALID_SYSTEM;
    }

    integrator->GetSystem()->SetPressurePtr(pressure);
    return MIST_OK;
}

int MIST_SetForces(void *forces)
{
    if (forces == NULL)
    {
        IO::Error("forces pointer was NULL.\n");
        return MIST_INVALID_POINTER_IN;
    }

    if (integrator == NULL)
    {
        IO::Error("Integrator not defined.\n");
        return MIST_INVALID_INTEGRATOR;
    }

    if (integrator->GetSystem() == NULL)
    {
        IO::Error("System object not defined.\n");
        return MIST_INVALID_SYSTEM;
    }

    integrator->GetSystem()->SetForcePtr(forces);
    return MIST_OK;
}

int MIST_SetNumBonds(int n)
{
    if (n < 0)
    {
        IO::Error("Negative number of bonds specified.\n");
        return MIST_INVALID_VALUE_IN;
    }

    if (integrator == NULL)
    {
        IO::Error("Integrator not defined.\n");
        return MIST_INVALID_INTEGRATOR;
    }

    if (integrator->GetSystem() == NULL)
    {
        IO::Error("System object not defined.\n");
        return MIST_INVALID_SYSTEM;
    }
    integrator->GetSystem()->SetNumBonds(n);
    return MIST_OK;
}

int MIST_SetBond(int i, int a, int b, double rab, bool hydrogen)
{
    if (integrator == NULL)
    {
        IO::Error("Integrator not defined.\n");
        return MIST_INVALID_INTEGRATOR;
    }

    if (integrator->GetSystem() == NULL)
    {
        IO::Error("System object not defined.\n");
        return MIST_INVALID_SYSTEM;
    }
    integrator->GetSystem()->SetBond(i, a, b, rab, hydrogen);
    return MIST_OK;
}

int MIST_GetFeatures(int *features)
{
    if (integrator == NULL)
    {
        IO::Error("Integrator not defined.\n");
        return MIST_INVALID_INTEGRATOR;
    }
    *features = integrator->GetFeatures();
    return MIST_OK;
}

int MIST_Step(double dt)
{
    if (integrator == NULL)
    {
        IO::Error("Integrator not defined.\n");
        return MIST_INVALID_INTEGRATOR;
    }
    integrator->Step(dt);
    return MIST_OK;
}

int MIST_Cleanup()
{
    if (integrator != NULL)
    {
        delete integrator;
        integrator = NULL;
    }

    int err = freeParams(params);
    if (err == ERR_FREE)
    {
        IO::Error("Problem freeing parameters.\n");
    }
    else if (err != ERR_OK)
    {
        IO::Error("Unknown error freeing parameters.\n");
    }

    IO::Info("Cleaned up MIST Library.\n");

    return err;
}

int MIST_SetForceCallback(void (*callback)(void *), void *data)
{
    if (callback == NULL)
    {
        IO::Error("ForceCallback function pointer "
                  "was NULL.\n");
        return MIST_INVALID_POINTER_IN;
    }
    if (data == NULL)
    {
        IO::Error("ForceCallback data pointer was NULL.\n");
        return MIST_INVALID_POINTER_IN;
    }
    if (integrator == NULL)
    {
        IO::Error("Integrator not defined.\n");
        return MIST_INVALID_INTEGRATOR;
    }
    if (integrator->GetSystem() == NULL)
    {
        IO::Error("System object not defined.\n");
        return MIST_INVALID_SYSTEM;
    }

    integrator->GetSystem()->SetForceCallback(callback, data);
    return MIST_OK;
}

int MIST_SetParam(const char *key, const char *value)
{
    if (integrator != NULL)
    {
        IO::Error("MIST_SetParams() may only be called before MIST_Init()\n");
        return MIST_INCORRECT_CALL_ORDER;
    }

    // Create a new parameter from this key/value pair
    Param *p = createParamFromTuple(key, value);

    int err = MIST_OK;

    if (params == NULL)
    {
        // This is the first parameter, just store it
        params = p;
    }
    else
    {
        // Add it to the list
        err = addParam(params, p);
    }

    return err;
}

int MIST_GetNDoF(int *ndof)
{
    int n = integrator->GetSystem()->GetTotalNumParticles();
    int c = integrator->GetNumConstraints();

    if (integrator->ConservesMomentum())
    {
        if (n == 0 || n == 1)
        {
            *ndof = 0;
        }
        else if (n == 2)
        {
            *ndof = 1 - c;
        }
        else
        {
            if (integrator->GetSystem()->HasPBC())
            {
                *ndof = (3 * n - c - 3);
            }
            else
            {
                *ndof = (3 * n - c - 6);
            }
        }
    }
    else
    {
        *ndof = (3 * n - c);
    }

    if (*ndof <= 0)
    {
        IO::Error("No degrees of freedom in the system!");
        return MIST_INVALID_SYSTEM;
    }

    return MIST_OK;
}

int MIST_SetUnitSystem(double boltzmann, double picosecond, double amu,
                       double angstrom, double atmosphere)
{
    if (integrator == NULL)
    {
        IO::Error("Integrator not defined.\n");
        return MIST_INVALID_INTEGRATOR;
    }

    if (integrator->GetSystem() == NULL)
    {
        IO::Error("System object not defined.\n");
        return MIST_INVALID_SYSTEM;
    }

    integrator->GetSystem()->SetUnits(boltzmann, picosecond, amu, angstrom, atmosphere);
    return MIST_OK;
}
