/*******************************************************************************
*                                                                              *
*             MIST - Molecular Integration Simulation Toolkit                  *
*         A library of integration algorithms for Molecular Dynamics           *
*                                                                              *
*            Copyright (c) 2014-2020, The University of Edinburgh              *
*                            All rights reserved.                              *
*                     List of contributors in AUTHORS                          *
*                                                                              *
*            MIST is free software.  See LICENSE for details                   *
*                                                                              *
*******************************************************************************/

/**
 * @file Yoshida4Integrator.h
 * @brief Declaration of a Yoshida order-4 Integrator class
 */

#ifndef MIST_YOSHIDA4INTEGRATOR_H
#define MIST_YOSHIDA4INTEGRATOR_H

#include "Integrator.h"

/**
 * @brief Declaration of the Yoshida 4th order constant-energy Integrator class
 *
 */
class Yoshida4Integrator : public Integrator
{
  public:
    /**
     * @brief Default Constructor
     * @param p a pointer to the parameters
     */
    Yoshida4Integrator(Param *p);

    /**
     * @brief Default Destructor
     */
    virtual ~Yoshida4Integrator();

    /**
     * @brief Integrate positions and velocities over time dt using
     * Yoshida's 4th-order algorithm.
     *
     * @param dt the timestep in internal units
     */
    void Step(double dt);
};

#endif
