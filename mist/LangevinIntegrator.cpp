/*******************************************************************************
*                                                                              *
*             MIST - Molecular Integration Simulation Toolkit                  *
*         A library of integration algorithms for Molecular Dynamics           *
*                                                                              *
*            Copyright (c) 2014-2020, The University of Edinburgh              *
*                            All rights reserved.                              *
*                     List of contributors in AUTHORS                          *
*                                                                              *
*            MIST is free software.  See LICENSE for details                   *
*                                                                              *
*******************************************************************************/

/**
 * @file LangevinIntegrator.cpp
 * @brief Implementation of the Langevin Integrator
 */

#include "LangevinIntegrator.h"
#include "ConstraintSolver.h"
#include "IO.h"
#include "System.h"
#include "strcasecmp.h"
#include "time.h"
#include <strings.h>

#include <math.h>
#ifdef _OPENMP
#include "omp.h"
#endif

/**
 * After superclass constructor, sets params based on user input
 * or defaults, and initialises the RNG
 */
LangevinIntegrator::LangevinIntegrator(Param *p, const char *name)
    : Integrator(p, name)
{
    features |= MIST_FEATURE_POS_VEL_NO_OFFSET;

    // Default values
    friction = 1.0;
    temp = 300.0;
    seed = time(NULL);
    method = LANGEVIN_METHOD_BAOAB;

    int err;

    // Read in parameters from mist.params file
    ReadDoubleParam(p, &friction, "langfriction", "ps^-1");
    ReadDoubleParam(p, &temp, "temperature", "K");
    ReadIntParam(p, (int *)&seed, "seed", "");

    char *type;
    err = getParamStringValue(p, "method", &type);
    if (err != ERR_OK)
    {
        IO::Warning("Method not specified, assuming BAOAB.\n");
    }
    else if (strcasecmp(type, "baoab") == 0)
    {
        IO::Info("Using BAOAB splitting method.\n");
        method = LANGEVIN_METHOD_BAOAB;
    }
    else if (strcasecmp(type, "aboba") == 0)
    {
        IO::Info("Using ABOBA splitting method.\n");
        method = LANGEVIN_METHOD_ABOBA;
    }
    else
    {
        IO::Warning("Your choice of method (%s) is not valid. "
                    "Defaulting to BAOAB splitting.\n",
                    type);
    }

    int nthreads = 1;
#ifdef _OPENMP
    nthreads = omp_get_max_threads();
#endif
    r = new MIST_Random *[nthreads];
    for (int i = 0; i < nthreads; i++)
    {
        r[i] = new MIST_Random(seed + i);
    }

    massrequirements = MIST_INVSQRT_MASS_REQD | MIST_INV_MASS_REQD;

    supportsConstraints = true;
}

LangevinIntegrator::LangevinIntegrator(Param *p)
    : LangevinIntegrator(p, "Langevin")
{
}

/**
 * Frees the RNG
 */
LangevinIntegrator::~LangevinIntegrator()
{
    if (r != NULL)
    {
        int nthreads = 1;
#ifdef _OPENMP
        nthreads = omp_get_max_threads();
#endif
        for (int i = 0; i < nthreads; i++)
        {
            delete r[i];
        }
        delete r;
    }
}

/**
 * Computes Boltzmann's constant * temperature, based on the host code
 * units system
 */
void LangevinIntegrator::SetSystem(System *s)
{
    Integrator::SetSystem(s);
    kbt = temp * system->Boltzmann();
}

/**
 * Solve the O part (stochastic step) in the BAOAB scheme
 */

void LangevinIntegrator::OStep(double dt)
{
    double gdt = friction / system->Picosecond() * dt;
    double c1 = exp(-gdt);
    double c3 = sqrt(kbt * (1 - c1 * c1));
    double sqrtinvm;
    Vector3 v;

    int t = 0;
#pragma omp parallel default(none) private(v, t, sqrtinvm) shared(c1, c3)
    {
#ifdef _OPENMP
        t = omp_get_thread_num();
#endif
#pragma omp for
        for (int i = 0; i < system->GetNumParticles(); i++)
        {
            v = system->GetVelocity(i);
            sqrtinvm = system->GetInverseSqrtMass(i);
            v.x = c1 * v.x + sqrtinvm * c3 * r[t]->random_gaussian();
            v.y = c1 * v.y + sqrtinvm * c3 * r[t]->random_gaussian();
            v.z = c1 * v.z + sqrtinvm * c3 * r[t]->random_gaussian();
            system->SetVelocity(i, v);
        }
    }
}

/**
 * Langevin Scheme (based on Charlie Matthews' NAMD-lite implementation)
 * for details see http://dx.doi.org/10.1063/1.4802990
 */
void LangevinIntegrator::Step(double dt)
{
    // Langevin Scheme (from Charlie Matthews' NAMD-lite implementation)
    // for details see http://dx.doi.org/10.1063/1.4802990
    // constraints implemented by Ralf Banisch

    if (method == LANGEVIN_METHOD_BAOAB)
    {
        // Velocity half-step (B)
        VelocityStep(0.5 * dt);
        ResolveVelocityConstraints(0.5 * dt);

        // Position half step (A)
        PositionStep(0.5 * dt);
        ResolvePositionConstraints(0.5 * dt);
        ResolveVelocityConstraints(0.5 * dt);

        // Velocity update (O)
        OStep(dt);
        ResolveVelocityConstraints(dt);

        // Position half step (A)
        PositionStep(0.5 * dt);
        ResolvePositionConstraints(0.5 * dt);
        ResolveVelocityConstraints(0.5 * dt);

        system->UpdateForces();

        // Velocity half-step (B)
        VelocityStep(0.5 * dt);
        ResolveVelocityConstraints(0.5 * dt);
    }
    else if (method == LANGEVIN_METHOD_ABOBA)
    {
        // Position half step (A)
        PositionStep(0.5 * dt);
        ResolvePositionConstraints(0.5 * dt);
        ResolveVelocityConstraints(0.5 * dt);

        system->UpdateForces();

        // Velocity half-step (B)
        VelocityStep(0.5 * dt);
        ResolveVelocityConstraints(0.5 * dt);

        // Velocity update (O)
        OStep(dt);
        ResolveVelocityConstraints(dt);

        // Velocity half-step (B)
        VelocityStep(0.5 * dt);
        ResolveVelocityConstraints(0.5 * dt);

        // Position half step (A)
        PositionStep(0.5 * dt);
        ResolvePositionConstraints(0.5 * dt);
        ResolveVelocityConstraints(0.5 * dt);
    }

    // write into diagnostic file
    // constraintSolver->WriteCounts(5, 100);
}
