/*******************************************************************************
*                                                                              *
*             MIST - Molecular Integration Simulation Toolkit                  *
*         A library of integration algorithms for Molecular Dynamics           *
*                                                                              *
*            Copyright (c) 2014-2020, The University of Edinburgh              *
*                            All rights reserved.                              *
*                     List of contributors in AUTHORS                          *
*                                                                              *
*            MIST is free software.  See LICENSE for details                   *
*                                                                              *
*******************************************************************************/

/**
 * @file IO.cpp
 * @brief Class containing (static) functions for outputting messages, warnings,
 * errors...
 */

#include "IO.h"

#include <stdarg.h>

#ifdef __MIST_WITH_MPI
MPI_Comm IO::communicator;
#endif

void IO::Info(const char *fmt, ...)
{
    va_list args;
    va_start(args, fmt);
    WriteMessage(stdout, true, "MIST", fmt, args);
    va_end(args);
}

void IO::InfoAll(const char *fmt, ...)
{
    va_list args;
    va_start(args, fmt);
    WriteMessage(stdout, false, "MIST", fmt, args);
    va_end(args);
}

void IO::Warning(const char *fmt, ...)
{
    va_list args;
    va_start(args, fmt);
    WriteMessage(stderr, true, "MIST WARNING", fmt, args);
    va_end(args);
}

void IO::WarningAll(const char *fmt, ...)
{
    va_list args;
    va_start(args, fmt);
    WriteMessage(stderr, false, "MIST WARNING", fmt, args);
    va_end(args);
}

void IO::Error(const char *fmt, ...)
{
    va_list args;
    va_start(args, fmt);
    WriteMessage(stderr, false, "MIST ERROR", fmt, args);
    va_end(args);
}

void IO::WriteMessage(FILE *f, bool master_only, const char *prefix,
                      const char *fmt, va_list args)
{

#ifdef __MIST_WITH_MPI
    if (master_only)
    {
        int rank;
        MPI_Comm_rank(communicator, &rank);
        if (rank != 0)
            return;
    }
#endif

    fprintf(f, "%s", prefix);
    fprintf(f, ": ");
    vfprintf(f, fmt, args);
}

#ifdef __MIST_WITH_MPI
void IO::SetCommunicator(MPI_Comm c) { communicator = c; }

MPI_Comm IO::GetCommunicator() { return communicator; }
#endif
