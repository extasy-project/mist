/*******************************************************************************
*                                                                              *
*             MIST - Molecular Integration Simulation Toolkit                  *
*         A library of integration algorithms for Molecular Dynamics           *
*                                                                              *
*            Copyright (c) 2014-2020, The University of Edinburgh              *
*                            All rights reserved.                              *
*                     List of contributors in AUTHORS                          *
*                                                                              *
*            MIST is free software.  See LICENSE for details                   *
*                                                                              *
*******************************************************************************/

/**
 * @file Yoshida8Integrator.cpp
 * @brief Implementation of an eighth-order constant energy integrator
 */

#include "Yoshida8Integrator.h"
#include "ConstraintSolver.h"
#include "IO.h"
#include "System.h"

/**
 * After superclass constructor, simply prints a message.
 * Positions and velocities are integrated from t -> t + dt
 */
Yoshida8Integrator::Yoshida8Integrator(Param *p)
    : Integrator(p, "Yoshida 8th Order")
{
    features |= MIST_FEATURE_POS_VEL_NO_OFFSET;

    massrequirements = MIST_INV_MASS_REQD;

    conservesMomentum = true;
}

/**
 * Does nothing
 */
Yoshida8Integrator::~Yoshida8Integrator() {}

/**
 * Computes a full-step update by fifteen intermediate scaled steps,
 * following the scheme described in
 * https://dx.doi.org/10.1016%2F0375-9601%2890%2990092-3
 */
void Yoshida8Integrator::Step(double dt)
{
    // Yoshida 8th order algorithm

    int j;
    double d[8] = {0.104242620869991e1,   0.182020630970714e1,
                   0.157739928123617e0,   0.244002732616735e1,
                   -0.716989419708120e-2, -0.244699182370524e1,
                   -0.161582374150097e1,  -0.17808286265894516e1};

    for (int k = 0; k < 15; k++)
    {
        if (k < 8)
            j = k;
        else
            j = 14 - k;

        // Velocity half-step
        VelocityStep(0.5 * dt * d[j]);

        // Position full step
        PositionStep(dt * d[j]);

        system->UpdateForces();

        // Velocity half-step
        VelocityStep(0.5 * dt * d[j]);
    }
}
