/*******************************************************************************
*                                                                              *
*             MIST - Molecular Integration Simulation Toolkit                  *
*         A library of integration algorithms for Molecular Dynamics           *
*                                                                              *
*            Copyright (c) 2014-2020, The University of Edinburgh              *
*                            All rights reserved.                              *
*                     List of contributors in AUTHORS                          *
*                                                                              *
*            MIST is free software.  See LICENSE for details                   *
*                                                                              *
*******************************************************************************/

/**
 * @file VerletIntegrator.h
 * @brief Declaration of a Verlet Integrator class
 */

#ifndef MIST_VERLETINTEGRATOR_H
#define MIST_VERLETINTEGRATOR_H

#include "Integrator.h"

/**
 * @brief Declaration of the Verlet Integrator class
 *
 * A very simple integrator class which uses the Velocity Verlet algorithm.
 *
 * Mainly useful as a reference for the development of other integrators, and
 * for comparison with native Verlet integrators in host application codes.
 *
 */
class VerletIntegrator : public Integrator
{
  public:
    /**
     * @brief Default Constructor
     *
     * After superclass constructor, simply prints a message.
     *
     * @param p a pointer to the parameters
     */
    VerletIntegrator(Param *p);

    /**
     * @brief Default Destructor
     *
     * Does nothing
     */
    virtual ~VerletIntegrator();

    /**
     * @brief Integrate positions and velocities over time dt using the Velocity
     * Verlet algorithm
     *
     * Computes a half-step update of velocities, a full-step update of
     * positions, updates forces and then a further half-step
     * update of velocities.
     *
     * @param dt the timestep in internal units
     */
    void Step(double dt);
};

#endif
