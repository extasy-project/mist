/*******************************************************************************
*                                                                              *
*             MIST - Molecular Integration Simulation Toolkit                  *
*         A library of integration algorithms for Molecular Dynamics           *
*                                                                              *
*            Copyright (c) 2014-2020, The University of Edinburgh              *
*                            All rights reserved.                              *
*                     List of contributors in AUTHORS                          *
*                                                                              *
*            MIST is free software.  See LICENSE for details                   *
*                                                                              *
*******************************************************************************/

/**
 * @file System.h
 * @brief Declaration of System abstract base class.
 */

#ifndef MIST_SYSTEM_H
#define MIST_SYSTEM_H

#include "Topology.h"
#include "Vector3.h"

#include "Integrator.h"

/**
 * @brief Declaration of a System abstract base class, describing the particles
 *which are to be integrated.
 *
 * This is the API that Integrators use to access and modify the state of the
 *system.  It provides accessors for the system state, unit conversion routines.
 *The API is implemented by a particular sub-class, specific to a host
 *application.
 *
 * Accessors are also provided to set up the system - these are not called
 *directly but are used to implement the MIST library API, which is called by
 *the host application.
 */
class System
{
  public:
    /**
     * @brief Selects components of the forcefield
     *
     * MIST can separate the contributions to the total potential energy / force
     *on a particle
     * returned by GetForce() / GetPotentialEnergy() into several components.
     *This enum selects which component to return.
     */
    enum force_component_t
    {
        MIST_FORCE_ALL,         /**< The complete forcefield */
        MIST_FORCE_BOND,        /**< Bonded terms only */
        MIST_FORCE_BEND,        /**< Angle terms only */
        MIST_FORCE_PROPDIHED,   /**< Proper dihedral terms only */
        MIST_FORCE_IMPROPDIHED, /**< Improper dihedral terms only */
        MIST_FORCE_NONBONDED    /**< Non-bonded terms only */
    };

    /**
     * @brief Default Constructor
     *
     * Does nothing
     */
    System();

    /**
     * @brief Default Destructor
     *
     * Does nothing
     */
    virtual ~System();

    /**
     * @defgroup INTEGRATOR_INTERFACE System API
     *
     * @brief API for Integrators to interact with the host application code.
     *
     * @{
     */

    /**
     * @brief Getter for the (local) number of particles
     *
     * Gets the (local) number of particles, useful as a loop bound.
     *
     * @return the (local) number of particles in the System
     */
    int GetNumParticles() const;

    /**
     * @brief Getter for the global number of particles
     *
     * Gets the number of particles across all processes.
     *
     * @return the global number of particles in the System
     */
    int GetTotalNumParticles() const;

    /**
     * @brief Getter for the number of bonds
     *
     * Gets the number of bonds, useful as a loop bound.
     *
     * @return the number of bonds in the System
     */
    int GetNumBonds() const;

    /**
     * @brief Getter for bonds
     *
     * Gets the Bond with index i.
     *
     * @param i the bond index
     * @return the bond with index i
     */
    Bond GetBond(int i) const;

    /**
     * @brief Getter for particle velocities
     *
     * Gets a Vector3 containing the velocity of particle i.
     *
     * @param i the particle index
     * @return the velocity of particle i
     */
    virtual Vector3 GetVelocity(int i) const = 0;

    /**
     * @brief Setter for particle velocities
     *
     * Sets the velocity of particle i.
     *
     * @param i the particle index
     * @param v the new velocity
     */
    virtual void SetVelocity(int i, Vector3 v) = 0;

    /**
     * @brief Getter for particle positions
     *
     * Gets a Vector3 containing the position of particle i.
     *
     * @param i the particle index
     * @return the position of particle i
     */
    virtual Vector3 GetPosition(int i) const = 0;

    /**
     * @brief Setter for particle positions
     *
     * Sets the position of particle i.
     *
     * @param i the particle index
     * @param p the new position
     */
    virtual void SetPosition(int i, Vector3 p) = 0;

    /**
     * @brief Getter for forces
     *
     * Returns the total force on particle i.
     *
     * @param i the particle index
     * @return the total force on particle i
     */
    virtual Vector3 GetForce(int i) const = 0;

    /**
     * @brief Getter for force components
     *
     * Returns a component of the force on particle i.
     * \see force_component_t
     *
     * @param i the particle index
     * @param t which component of the force-field to select
     * @return the force on particle i from the selected component of the
     *force-field
     */
    virtual Vector3 GetForce(int i, force_component_t t) const = 0;

    /**
     * @brief Getter for particle masses
     *
     * Gets the mass of particle i.
     *
     * @param i the particle index
     * @return the mass of particle i
     */
    virtual double GetMass(int i) const = 0;

    /**
     * @brief Getter for particle inverse masses
     *
     * Gets one over the mass of particle i.
     * Usually more efficient to call this rather than 1/GetMass() as
     * the inverse mass may be precomputed and stored.
     *
     * @param i the particle index
     * @return 1/mass of particle i
     */
    virtual double GetInverseMass(int i) const = 0;

    /**
     * @brief Getter for particle inverse sqrt masses
     *
     * Gets one over the square root of the mass of particle i.
     * Usually more efficient to call this rather than 1/sqrt(GetMass()) as
     * the inverse square root mass may be precomputed and stored.
     *
     * @param i the particle index
     * @return 1/sqrt(mass) of particle i
     */
    virtual double GetInverseSqrtMass(int i) const = 0;

    /**
     * @brief Getter for potential energy
     *
     * Returns the total potential energy
     *
     * @return the potential energy of the system
     */
    virtual double GetPotentialEnergy() const = 0;

    /**
     * @brief Getter for the pressure
     *
     * Returns the pressure (scalar), or zero if
     * pbc are not defined
     *
     * @return the potential energy of the system
     */
    virtual double GetPressure() const = 0;

    /**
     * @brief Getter for potential energy components
     *
     * Returns the potential energy due to the selected component of the
     *force-field
     * \see force_component_t
     *
     * @param t which component of the force-field to select
     * @return the potential energy from the selected component of the
     *force-field
     */
    virtual double GetPotentialEnergy(force_component_t t) const = 0;

    /**
     * @brief Getter for particle kind labels
     *
     * Gets a string representing the kind (species) of particle i.
     * This is usually a label taken from the input geometry (a PDB file or
     * similar), but depends on the host application code.
     *
     * @param i the particle index
     * @return a null-terminated string containing the kind label
     */
    virtual char *GetKind(int i) const = 0;

    /**
     * @brief Update the forces on the system given the current particle
     *positions
     *
     * After modifying particle positions, an Integrator may call
     *UpdateForces(). This makes a callback to the host application's force
     *evaluation routine and computes up-to-date forces and potential energies,
     *which may then be read by calling GetForce() / GetPotentialEnergy()
     */
    void UpdateForces();

    /**
     * @brief Get Boltzmann's constant in internal units
     *
     * MIST Integrators work in whatever internal units are used by the
     * host application code.  To convert input parameters from a specified
     * unit to internal units, this function returns the value of Boltzmann's
     * constant (energy / temperature).
     *
     * @return Boltzmann's constant in internal units
     */
    double Boltzmann() const;

    /**
     * @brief Get one picosecond in internal units
     *
     * MIST Integrators work in whatever internal units are used by the
     * host application code.  To convert input parameters from a specified
     * unit to internal units, this function returns one picosecond (time).
     *
     * @return one picosecond in internal units
     */
    double Picosecond() const;

    /**
     * @brief Get one AMU (Atomic Mass Unit) in internal units
     *
     * MIST Integrators work in whatever internal units are used by the
     * host application code.  To convert input parameters from a specified
     * unit to internal units, this function returns one AMU (Atomic
     * Mass Unit) (mass).
     *
     * @return one AMU in internal units
     */
    double AMU() const;

    /**
     * @brief Get one Angstrom in internal units
     *
     * MIST Integrators work in whatever internal units are used by the
     * host application code.  To convert input parameters from a specified
     * unit to internal units, this function returns one Angstrom (length).
     *
     * @return one Angstrom in internal units
     */
    double Angstrom() const;

    /**
     * @brief Get one Atmosphere in internal units
     *
     * MIST Integrators work in whatever internal units are used by the
     * host application code.  To convert input parameters from a specified
     * unit to internal units, this function returns one Atmosphere (pressure).
     *
     * @return one Atmosphere in internal units
     */
    double Atmosphere() const;

    /**
     * @brief Set the units system to be used
     *
     * Defined the units system of the host MD code in terms of standard units
     * of length, energy, time, mass and pressure.
     *
     * @param boltzmann Boltzmann's constant in internal units
     * @param picosecond one picosecond in internal units
     * @param amu one AMU in internal units
     * @param angstrom one Angstrom in internal units
     * @param atm one Atmosphere in internal units
     */
    void SetUnits(double boltzmann, double picosecond, double amu,
                  double angstrom, double atm);

    /**
     * @brief Getter for the lattice parameters
     *
     * Gets the a,b,c lattice vectors. a,b,c are the column vectors of the
     * cell matrix H.
     *
     * @param a the first lattice vector
     * @param b the second lattice vector
     * @param c the third lattice vector
     */
    virtual void GetCell(Vector3 *a, Vector3 *b, Vector3 *c) = 0;

    /**
     * @brief Getter for the inverse of the lattice parameters
     *
     * Gets a,b,c, the column vectors of the inverse of the cell
     * matrix H^-1.
     *
     * @param a the first inverse lattice vector
     * @param b the second inverse lattice vector
     * @param c the third inverse lattice vector
     */
    virtual void GetCellInverse(Vector3 *a, Vector3 *b, Vector3 *c) = 0;

    /**
     * @brief Setter for the lattice parameters
     *
     * Sets the a,b,c lattice vectors and moves particles so they retain
     * the same fractional coordinates in the new basis.
     *
     * @param a the first lattice vector
     * @param b the second lattice vector
     * @param c the third lattice vector
     */
    virtual void SetCell(Vector3 a, Vector3 b, Vector3 c) = 0;

    /**
     * @ brief Check if the system has periodic boundary conditions
     *
     * @return true if PBC is enabled, false if not
     */
    bool HasPBC();

    /**
     * @}
     */

    // *************************

    /**
     * @brief Setter for number of particles
     *
     * Sets the number of particles to be integrated.  This is used is an upper
     * bound for indexing into the velocity, position, ... data.
     *
     * @param n the number of particles
     */
    void SetNumParticles(int n);

    /**
     * @brief Setter for the cell pointer
     *
     * Stores a pointer to the simulation cell data.  Used for periodic boundary
     * corrections.
     *
     * @param cell pointer to cell data
     */
    virtual void SetCellPtr(void *cell);

    /**
     * @brief Setter for the velocity pointer
     *
     * Stores a pointer to the particle velocity data.
     *
     * @param v pointer to velocity data
     */
    void SetVelocityPtr(void *v);

    /**
     * @brief Setter for the position pointer
     *
     * Stores a pointer to the particle position data.
     *
     * @param p pointer to position data
     */
    virtual void SetPositionPtr(void *p);

    /**
     * @brief Setter for the mass pointer
     *
     * Stores a pointer to the particle mass data.
     *
     * @param m pointer to mass data
     * @param req integer containing mass requirements for the integrator
     */
    virtual void SetMassPtr(void *m, int req);

    /**
     * @brief Setter for the pressure pointer
     *
     * Stores a pointer to the pressure data.
     *
     * @param p pointer to pressure data
     */
    void SetPressurePtr(void *p);

    /**
     * @brief Setter for the potential energy pointer
     *
     * Stores a pointer to the potential energy data.
     *
     * @param pe pointer to potential energy data
     */
    void SetPotentialEnergyPtr(void *pe);

    /**
     * @brief Setter for the force pointer
     *
     * Stores a pointer to the particle force data.
     *
     * @param f pointer to particle force data
     */
    void SetForcePtr(void *f);

    /**
     * @brief Setter for the kind pointer
     *
     * Stores a pointer to the particle kind (species) data.
     *
     * @param k pointer to kind data
     */
    void SetKindsPtr(void *k);

    /**
     * @brief Setter for number of bonds
     *
     * Sets the number of bonds.  This is used is an upper
     * bound for indexing into the bonds array.  Should only
     * be called once, before adding bonds.
     *
     * @param n the number of bonds
     */
    void SetNumBonds(int n);

    /**
     * @brief Sets up a single bond
     *
     * Sets bond i with the given parameters
     *
     * @param i which bond to set
     * @param a first particle index
     * @param b second particle index
     * @param rab distance between the two particles
     * @param hydrogen if the bond connects a hydrogen atom
     */
    void SetBond(int i, int a, int b, double rab, bool hydrogen);

    /**
     * @brief Setter for the integrator
     *
     * Stores a pointer to the integrator.
     *
     * @param i pointer to Integrator
     */
    void SetIntegrator(Integrator *i);

#ifdef __MIST_WITH_MPI
    /**
     * @brief Setter for the MPI communicator
     *
     * @param c the MPI communicator
     */
    void SetCommunicator(MPI_Comm c);

    /**
     * @brief Getter for the MPI communicator
     *
     * @return the MPI communicator
     */
    MPI_Comm GetCommunicator() const;
#endif

    /**
     * @brief Computes dihedral angle between atoms with indices i, j, k, l
     * which are passed in the array phi_ind
     *
     * @param phi_ind array of four atom indices defining the dihedral
     * @return the dihedral angle
     */
    double ComputeDihedrals(int phi_ind[]);

    /**
     * @brief Computes Jacobian of dihedral angle defined by phi_ind = (i,j,k,l)
     * with respect to atom n.
     *
     * @param phi_ind array of four atom indices defining the dihedral
     * @param n the atom index to compute the Jacobian w.r.t.
     * @return the Jacobian vector
     */
    Vector3 ComputeDihedralsJac(int phi_ind[], int n);

    /**
     * @brief Given vectors ai, and aj, compute the difference da = ai - aj in
     *such
     * a way that ai is fixed and aj is chosen according to the minimum periodic
     * image convention. Distances can't be greater then half the cell dimension
     * in each direction.
     *
     * @param ai, aj the input vectors
     * @return the minimum image difference da
     */
    Vector3 MinimumImageDiff(Vector3 ai, const Vector3 aj);

    /**
     * @brief Stores a function pointer to the host application force
     *computation function
     *
     * Called via MIST_SetForceCallback, this stores a function pointer and
     *associated
     * data, which is used by UpdateForces to make a callback to the host code
     *to compute updated forces and (optionally) energies
     * @param f pointer to the callback function
     * @param p pointer to the data which will be passed to the callback
     *function
     */
    void SetForceCallback(void (*f)(void *), void *p);

    // *************************

    // Base class variables
  protected:
    int numParticles; /**< @brief The number of particles to be integrated */
    int totalNumParticles; /**< @brief The total number of particles in the
                             system */
    void *velocities;      /**< @brief The particle velocity pointer */
    void *positions;       /**< @brief The particle position pointer */
    double *masses;        /**< @brief The particle mass pointer */
    double *invmasses;     /**< @brief The particle inverse mass pointer */
    double *invsqrtmasses; /**< @brief The particle inverse sqrt mass pointer */
    void *pressure;        /**< @brief The pressure pointer */
    void *potential_energy; /**< @brief The potential energy pointer */
    void *forces;           /**< @brief The particle force pointer */
    void *kinds;            /**< @brief The particle kind pointer */
    int numBonds;           /**< @brief The number of bonds defined */
    Bond *bonds;            /**< @brief Array of bonds defined in the system */
    void *cell;             /**< @brief The simulation cell data pointer */
    bool pbc; /**< @brief If periodic boundary conditions are used */
    Integrator *integrator; /**< @brief The integrator that will be used */
#ifdef __MIST_WITH_MPI
    MPI_Comm comm; /**< @brief MPI communicator containing all processes that
                      will call MIST */
#endif

    // Conversion factors for each code's units system.
    double boltzmann;  /**< @brief Boltzmann's constant in internal units */
    double picosecond; /**< @brief 1 picosecond in internal units */
    double amu;        /**< @brief 1 atomic mass unit (AMU) in internal units */
    double angstrom;   /**< @brief 1 angstrom in internal units */
    double atm;        /**< @brief 1 atmosphere in internal units */

    void (*force_callback)(
        void *);      /**< @brief Function pointer to compute new forces */
    void *force_data; /**< @brief Parameters passed to the callback function */
};

#endif
