/*******************************************************************************
*                                                                              *
*             MIST - Molecular Integration Simulation Toolkit                  *
*         A library of integration algorithms for Molecular Dynamics           *
*                                                                              *
*            Copyright (c) 2014-2020, The University of Edinburgh              *
*                            All rights reserved.                              *
*                     List of contributors in AUTHORS                          *
*                                                                              *
*            MIST is free software.  See LICENSE for details                   *
*                                                                              *
*******************************************************************************/

/**
 * @file Topology.h
 * @brief Classes to store MIST's representation of a molecular topology
 *
 */

#ifndef MIST_TOPOLOGY_H
#define MIST_TOPOLOGY_H

/**
 * @brief A class which represents a bond
 *
 * Direct access to the members is permitted.
 */
class Bond
{
  public:
    /**
     * @brief Default Constructor
     *
     * Creates and zeros a vector
     */
    Bond() : a(0), b(0), rab(0.0), hydrogen(false){};

    /**
     * @brief Constructor with specified entries
     *
     * Creates a bond and sets it according to the provided data
     *
     * @param a first particle index
     * @param b second particle index
     * @param rab distance between the two particles
     * @param hydrogen if the bond connects a hydrogen atom
     */
    Bond(int a, int b, double rab, bool hydrogen)
        : a(a), b(b), rab(rab), hydrogen(hydrogen){};

    /**
     * @brief checks if a bond has been initialised
     *
     * Returns if a bond has been initialised with valid data
     *
     * @return if the bond has been initialised
     */
    bool IsValid() const { return (a != b && rab > 0.0); }

    int a;         /**< @brief first particle index, publically accessible */
    int b;         /**< @brief second particle index, publically accessible */
    double rab;    /**< @brief distance between the two particles, publically
                      accessible */
    bool hydrogen; /**< @brief if the bond connects a hydrogen atom, publically
                      accessible */
};
#endif
