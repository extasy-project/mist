/*******************************************************************************
*                                                                              *
*             MIST - Molecular Integration Simulation Toolkit                  *
*         A library of integration algorithms for Molecular Dynamics           *
*                                                                              *
*            Copyright (c) 2014-2020, The University of Edinburgh              *
*                            All rights reserved.                              *
*                     List of contributors in AUTHORS                          *
*                                                                              *
*            MIST is free software.  See LICENSE for details                   *
*                                                                              *
*******************************************************************************/

/**
 * @file LeapfrogIntegrator.cpp
 * @brief Implementation of a Leapfrog integrator.
 */

#include "LeapfrogIntegrator.h"
#include "ConstraintSolver.h"
#include "IO.h"
#include "System.h"

LeapfrogIntegrator::LeapfrogIntegrator(Param *p)
    : Integrator(p, "Verlet Leapfrog")
{
    features |= MIST_FEATURE_POS_VEL_OFFSET_PLUS_HALF_DT;

    massrequirements = MIST_INV_MASS_REQD;

    conservesMomentum = true;
}

LeapfrogIntegrator::~LeapfrogIntegrator() {}

void LeapfrogIntegrator::Step(double dt)
{
    // Simple leapfrog algorithm

    // Position full step: x(t) -> x(t + dt)
    PositionStep(dt);

    // Update forces using x(t + dt) and v(t + dt/2)
    system->UpdateForces();

    // Velocity full-step but out of sync with position: v(t + dt/2) -> v(t +
    // 3*dt/2)
    VelocityStep(dt);
}
