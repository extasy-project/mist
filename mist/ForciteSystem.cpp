/*******************************************************************************
*                                                                              *
*             MIST - Molecular Integration Simulation Toolkit                  *
*         A library of integration algorithms for Molecular Dynamics           *
*                                                                              *
*            Copyright (c) 2014-2020, The University of Edinburgh              *
*                            All rights reserved.                              *
*                     List of contributors in AUTHORS                          *
*                                                                              *
*            MIST is free software.  See LICENSE for details                   *
*                                                                              *
*******************************************************************************/

/**
 * @file ForciteSystem.cpp
 * @brief Implementation of System interface for Forcite
 */

#include "ForciteSystem.h"
#include <stddef.h>

// Stub implementation of the Forcite System class
// The real implementation is property of Dassault Systemes BIOVIA.

ForciteSystem::ForciteSystem() : System() {}

ForciteSystem::~ForciteSystem() {}

Vector3 ForciteSystem::GetVelocity(int i) const
{
    return Vector3(0.0, 0.0, 0.0);
}

void ForciteSystem::SetVelocity(int i, Vector3 v) {}

Vector3 ForciteSystem::GetPosition(int i) const
{
    return Vector3(0.0, 0.0, 0.0);
}

void ForciteSystem::SetPosition(int i, Vector3 p) {}

double ForciteSystem::GetPressure() const { return 0.0; }

double ForciteSystem::GetPotentialEnergy() const { return 0.0; }

double ForciteSystem::GetPotentialEnergy(force_component_t t) const
{
    return 0.0;
}

Vector3 ForciteSystem::GetForce(int i) const { return Vector3(0.0, 0.0, 0.0); }

Vector3 ForciteSystem::GetForce(int i, force_component_t t) const
{
    return Vector3(0.0, 0.0, 0.0);
}

void ForciteSystem::SetMassPtr(void *m, int massrequirements) {}

double ForciteSystem::GetMass(int i) const { return 0.0; }

double ForciteSystem::GetInverseMass(int i) const { return 0.0; }

double ForciteSystem::GetInverseSqrtMass(int i) const { return 0.0; }

char *ForciteSystem::GetKind(int i) const { return (char *)NULL; }
