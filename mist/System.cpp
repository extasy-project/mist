/*******************************************************************************
*                                                                              *
*             MIST - Molecular Integration Simulation Toolkit                  *
*         A library of integration algorithms for Molecular Dynamics           *
*                                                                              *
*            Copyright (c) 2014-2020, The University of Edinburgh              *
*                            All rights reserved.                              *
*                     List of contributors in AUTHORS                          *
*                                                                              *
*            MIST is free software.  See LICENSE for details                   *
*                                                                              *
*******************************************************************************/

/**
 * @file System.cpp
 * @brief Implementation of the System base class, accessors only implemented
 */

#include "System.h"
#include "IO.h"

#include <math.h>
#include <stddef.h>

System::System()
{
    numBonds = 0;
    numParticles = 0;
    totalNumParticles = 0;
    bonds = NULL;
    velocities = NULL;
    positions = NULL;
    masses = NULL;
    invmasses = NULL;
    invsqrtmasses = NULL;
    pressure = NULL;
    potential_energy = NULL;
    forces = NULL;
    kinds = NULL;
    pbc = false;
    integrator = NULL;
    cell = NULL;
#ifdef __MIST_WITH_MPI
    comm = MPI_COMM_NULL;
#endif
    force_callback = NULL;
    force_data = NULL;
}

System::~System()
{
    if (bonds != NULL)
    {
        delete[] bonds;
        bonds = NULL;
    }
}

int System::GetNumParticles() const { return numParticles; }

int System::GetTotalNumParticles() const { return totalNumParticles; }

int System::GetNumBonds() const { return numBonds; }

Bond System::GetBond(int i) const
{
    if (i < 0 || i >= numBonds)
    {
        IO::Error("invalid bond index %d\n");
        return Bond();
    }

    return bonds[i];
}

void System::SetNumParticles(int n)
{
    // Local particle count
    numParticles = n;

#ifdef __MIST_WITH_MPI
    // Sum up to get global particle count
    MPI_Allreduce(&numParticles, &totalNumParticles, 1, MPI_INT, MPI_SUM, comm);
#else
    totalNumParticles = numParticles;
#endif
}

bool System::HasPBC() { return pbc; }

void System::SetCellPtr(void *c)
{
    cell = c;

    if (cell != NULL)
    {
        pbc = true;
    }
}

void System::SetVelocityPtr(void *v) { velocities = v; }

void System::SetPositionPtr(void *p) { positions = p; }

void System::SetMassPtr(void *m, int req) { masses = (double *)m; }

void System::SetPressurePtr(void *p) { pressure = p; }

void System::SetPotentialEnergyPtr(void *pe) { potential_energy = pe; }

void System::SetForcePtr(void *f) { forces = f; }

void System::SetKindsPtr(void *k) { kinds = k; }

void System::SetIntegrator(Integrator *i) { integrator = i; }

double System::Boltzmann() const { return boltzmann; }

double System::Picosecond() const { return picosecond; }

double System::AMU() const { return amu; }

double System::Angstrom() const { return angstrom; }

double System::Atmosphere() const { return atm; }

void System::SetUnits(double b, double p, double a, double an, double at)
{
    boltzmann = b;
    picosecond = p;
    amu = a;
    angstrom = an;
    atm = at;
}

void System::SetNumBonds(int n)
{
    // Clean up any prior bond data, with a warning
    if (numBonds != 0)
    {
        IO::Warning("SetNumBonds() called when bonds are "
                    "already defined.  Overwriting older data...\n");
        delete[] bonds;
        bonds = NULL;
    }

    // If > 0 bonds allocate a new array
    if (n > 0)
    {
        bonds = new Bond[n];
    }

    // Save the number of bonds
    numBonds = n;
}

void System::SetBond(int i, int a, int b, double rab, bool hydrogen)
{
    if (i >= numBonds)
    {
        IO::Warning("attempted to add a bond with index %d"
                    " when numBonds is %d - ignoring it!\n",
                    i, numBonds);
        return;
    }

    bonds[i] = Bond(a, b, rab, hydrogen);
}

#ifdef __MIST_WITH_MPI
void System::SetCommunicator(MPI_Comm c) { comm = c; }

MPI_Comm System::GetCommunicator() const { return comm; }
#endif

/**
 * Computes dihedral angle phi in [-pi,pi] between atoms with indices i, j, k,
 * l.
 */
double System::ComputeDihedrals(int phi_ind[])
{
    // compute the dihedral angle defined by the atoms i,j,k,l
    // see Arnaud Blondel and Martin Karplus, 'New Formulation for Derivatives
    // of Torsion Angles and Improper Torsion Angles in Molecular Mechanics:
    // Elimination of Singularities', JCC 1995,
    // http://onlinelibrary.wiley.com/doi/10.1002/.
    int i = phi_ind[0];
    int j = phi_ind[1];
    int k = phi_ind[2];
    int l = phi_ind[3];
    double qb_norm;
    Vector3 qa, qb, qc, n1, n2;
    qa = GetPosition(i) - GetPosition(j);
    qb = GetPosition(j) - GetPosition(k);
    qc = GetPosition(l) - GetPosition(k);
    n1 = Vector3::Cross(qa, qb);
    n2 = Vector3::Cross(qc, qb);
    qb_norm = sqrt(Vector3::Mult(qb, qb));
    return atan2(Vector3::Mult(Vector3::Cross(n2, n1), qb) / qb_norm,
                 Vector3::Mult(n1, n2));
}

/**
 * Computes Jacobian of dihedral angle
 */
Vector3 System::ComputeDihedralsJac(int phi_ind[], int n)
{
    // compute the Jacobian (first derivative) d\phi/dq_n of dihedral angle \phi
    // defined by the atoms i,j,k,l with respect to q_n.
    // see Arnaud Blondel and Martin Karplus, 'New Formulation for Derivatives
    // of Torsion Angles and Improper Torsion Angles in Molecular Mechanics:
    // Elimination of Singularities', JCC 1995,
    // http://onlinelibrary.wiley.com/doi/10.1002/.
    int i = phi_ind[0];
    int j = phi_ind[1];
    int k = phi_ind[2];
    int l = phi_ind[3];
    double qb_norm;
    Vector3 qa, qb, qc, n1, n2;
    if ((n == i) || (n == j) || (n == k) || (n == l))
    {
        qa = GetPosition(i) - GetPosition(j);
        qb = GetPosition(j) - GetPosition(k);
        qc = GetPosition(l) - GetPosition(k);
        n1 = Vector3::Cross(qa, qb);
        n2 = Vector3::Cross(qc, qb);
        n1 = Vector3::Scale(1.0 / Vector3::Mult(n1, n1), n1);
        n2 = Vector3::Scale(1.0 / Vector3::Mult(n2, n2), n2);
        qb_norm = sqrt(Vector3::Mult(qb, qb));
        if (n == i)
        {
            return Vector3::Scale(-1 * qb_norm, n1);
        }
        else if (n == j)
        {
            return Vector3::Scale(qb_norm + (Vector3::Mult(qa, qb) / qb_norm),
                                  n1) -
                   Vector3::Scale(Vector3::Mult(qb, qc) / qb_norm, n2);
        }
        else if (n == k)
        {
            return Vector3::Scale((Vector3::Mult(qb, qc) / qb_norm) - qb_norm,
                                  n2) -
                   Vector3::Scale(Vector3::Mult(qa, qb) / qb_norm, n1);
        }
        else if (n == l)
        {
            return Vector3::Scale(qb_norm, n2);
        }
    }

    // nothing to do, derivative is zero.
    return Vector3(0, 0, 0);
}

Vector3 System::MinimumImageDiff(Vector3 ai, const Vector3 aj)
{
    // this function applies pbc's to a distance vector da = a(i) - a(j) in such
    // a way that a(i) is fixed and a(j) is chosen according to the minimum
    // image convention. Distances can't be greater then half the cell dimension
    // in each direction.
    ai = ai - aj;
    if (!pbc)
        return ai;

    Vector3 a, b, c;
    Vector3 a_inv, b_inv, c_inv;

    GetCell(&a, &b, &c);
    GetCellInverse(&a_inv, &b_inv, &c_inv);

    Vector3 image;
    Vector3 lambda;

    lambda.x = round(a_inv.x * ai.x + b_inv.x * ai.y + c_inv.x * ai.z);
    lambda.y = round(a_inv.y * ai.x + b_inv.y * ai.y + c_inv.y * ai.z);
    lambda.z = round(a_inv.z * ai.x + b_inv.z * ai.y + c_inv.z * ai.z);

    // Subtract off multiples of a
    image.x = ai.x - a.x * lambda.x;
    image.y = ai.y - a.y * lambda.x;
    image.z = ai.z - a.z * lambda.x;

    // Subtract off multiples of b
    image.x = image.x - b.x * lambda.y;
    image.y = image.y - b.y * lambda.y;
    image.z = image.z - b.z * lambda.y;

    // Subtract off multiples of c
    image.x = image.x - c.x * lambda.z;
    image.y = image.y - c.y * lambda.z;
    image.z = image.z - c.z * lambda.z;

    return image;
}

void System::SetForceCallback(void (*c)(void *), void *p)
{
    force_callback = c;
    force_data = p;
}

void System::UpdateForces()
{
    if (force_callback != NULL)
    {
        force_callback(force_data);
    }
}
