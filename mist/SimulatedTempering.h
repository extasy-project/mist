/*******************************************************************************
*                                                                              *
*             MIST - Molecular Integration Simulation Toolkit                  *
*         A library of integration algorithms for Molecular Dynamics           *
*                                                                              *
*            Copyright (c) 2014-2020, The University of Edinburgh              *
*                            All rights reserved.                              *
*                     List of contributors in AUTHORS                          *
*                                                                              *
*            MIST is free software.  See LICENSE for details                   *
*                                                                              *
*******************************************************************************/

/**
 * @file SimulatedTempering.h
 * @brief Declaration of class for Simulated Tempering Integrator
 */

#ifndef MIST_SIMULATEDTEMPERING_H
#define MIST_SIMULATEDTEMPERING_H

#include "LangevinIntegrator.h"

#include <fstream>

/**
 * @brief Declaration of the Simulated Tempering Integrator class
 *
 * See https://dx.doi.org/10.1063/1.4792046 for details of the algorithm
 */
class SimulatedTempering : public LangevinIntegrator
{
  public:
    /**
     * @brief Default Constructor
     *
     * @param p a pointer to the parameters
     */
    SimulatedTempering(Param *p);

    /**
     * @brief Default Destructor
     */
    virtual ~SimulatedTempering();

    /**
     * @brief Integrate positions and velocities over time dt using
     * Simulated Tempering + Langevin Dynamics
     *
     * @param dt the timestep in internal units
     */
    void Step(double dt);

  protected:
    int period;         /**< @brief number of steps between attempts to switch
                           temperature */
    int step;           /**< @brief step counter */
    int state;          /**< @brief which state we are currently in */
    int n_temperatures; /**< @brief number of total states */
    double
        *temperatures; /**< @brief array of temperatures of each state (in K) */
    int *n_steps; /**< @brief number of steps we have computed in each state */
    double *weights; /**< @brief calculated weight for each state */
    double *avg_pe;  /**< @brief average potential energy in each state (in
                        internal units) */
    std::ofstream outfile; /**< @brief output file for ST data */
    int rank;              /**< @brief MPI rank of this process */
};

#endif
