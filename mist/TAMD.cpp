/*******************************************************************************
*                                                                              *
*             MIST - Molecular Integration Simulation Toolkit                  *
*         A library of integration algorithms for Molecular Dynamics           *
*                                                                              *
*            Copyright (c) 2014-2020, The University of Edinburgh              *
*                            All rights reserved.                              *
*                     List of contributors in AUTHORS                          *
*                                                                              *
*            MIST is free software.  See LICENSE for details                   *
*                                                                              *
*******************************************************************************/

/**
 * @file TAMD.cpp
 * @brief Implementation of the Temperature-accelerated MD (TAMD) Integrator.
 * This introduces additional degrees of freedom coupled to the collective
 *variables
 * (here: backbone dihedral angles). The additional degrees of freedom are
 *evolved
 * at the (high) temperature tamd_temp, have (large) masses tamd_mass and are
 *coupled
 * to the system variables via stiff springs with spring constant tamd_kappa.
 * This is a useful method for sampling free energy landscapes. The timeseries
 *of the collective variables and the forces acting on them are written in
 * tamd_out.dat event save_freq steps. This can be used to reconstruct the
 *free
 * energy. For more information see "Heating and flooding: A unified approach
 *for rapid generation of free energy surfaces", M. Chen et. al., Journal of
 *Chemical Physics.
 *
 * currently only 2D free energy surfaces with backbone dihedral angles
 *specified in mist.params are supported.
 *
 * @author RB
 */

#include "TAMD.h"
#include "IO.h"
#include "System.h"

#include <cmath>
#include <fstream>
#include <iostream>
#include <time.h>

#define TWO_TIMES_PI 2 * M_PI /**< @brief the constant 2*pi **/
#define BOLTZMANN_KJoverMOL                                                    \
    0.0083144621 /**< @brief Conversion constant Kb in KJ/mol **/

/**
 * After superclass constructor, sets params based on user input
 * or defaults, and initialises the RNG
 */
TAMD::TAMD(Param *p)
    : LangevinIntegrator(p, "Temperature Accelerated MD (TAMD)")
{
    // Default values
    temp_s = 1500.0;
    fric_s = 10.0;
    m_s_inp = 168.0;
    kappa_inp = 1163.0;
    save_freq = 10;

    // Read in parameters from mist.params file
    ReadDoubleParam(p, &temp_s, "tamd_temp", "K");
    ReadDoubleParam(p, &fric_s, "tamd_friction", "ps^-1");
    ReadDoubleParam(p, &m_s_inp, "tamd_mass", "u*angstrom^2");
    ReadDoubleParam(p, &kappa_inp, "tamd_kappa", "kJ/mol");
    ReadIntParam(p, &save_freq, "tamd_save_freq", "");

    // Read atom indices for dihedrals
    ReadIntParam(p, &phi_ind[0], "phi1", "");
    ReadIntParam(p, &phi_ind[1], "phi2", "");
    ReadIntParam(p, &phi_ind[2], "phi3", "");
    ReadIntParam(p, &phi_ind[3], "phi4", "");
    ReadIntParam(p, &psi_ind[0], "psi1", "");
    ReadIntParam(p, &psi_ind[1], "psi2", "");
    ReadIntParam(p, &psi_ind[2], "psi3", "");
    ReadIntParam(p, &psi_ind[3], "psi4", "");
}

TAMD::~TAMD() {}

/**
 * Computes Boltzmann's constant * temperature, based on the host code
 * units system
 */
void TAMD::SetSystem(System *s)
{
    LangevinIntegrator::SetSystem(s);
    // scale masses and spring constant to the right units
    kappa = kappa_inp * system->Boltzmann() / BOLTZMANN_KJoverMOL;
    m_s = m_s_inp * system->AMU() * pow(system->Angstrom(), 2);
    sqrt_ms_inv = sqrt(1.0 / m_s);
    // the mass of the additional dofs needs to have unit [mL^2] to balance the
    // fact that the s variables are dimensionless.
    kbt_s = temp_s * system->Boltzmann();
}

/**Position step for TAMD variables
 */
void TAMD::TAMDvars_PositionStep(double dt)
{
    // position step for additional dofs.
    s_phi += dt * v_phi;
    s_psi += dt * v_psi;

    // take into account periodicity of angular variables. If outside of
    // [-pi,pi],
    // we push s_phi and s_psi back in
    if (s_phi > M_PI)
        s_phi -= TWO_TIMES_PI;
    if (s_phi < (-1 * M_PI))
        s_phi += TWO_TIMES_PI;
    if (s_psi > M_PI)
        s_psi -= TWO_TIMES_PI;
    if (s_psi < (-1 * M_PI))
        s_psi += TWO_TIMES_PI;
}

/**computes difference between dihedral angles phi1 and phi2 modulo 2*pi.
 */
double TAMD::DihedralDiff(double phi1, double phi2)
{
    if (std::abs(phi1 - phi2) <= M_PI)
    {
        // phi1 and phi2 are both in [-pi,pi], nothing needs to be done
        return kappa * (phi1 - phi2);
    }
    else if (phi1 < phi2)
    {
        // translate phi1 by 2*pi.
        return kappa * (phi1 + TWO_TIMES_PI - phi2);
    }
    else
    {
        // translate phi1 by -2*pi.
        return kappa * (phi1 - TWO_TIMES_PI - phi2);
    }
}

/**
 * Langevin BAOAB Scheme (based on Charlie Matthews' NAMD-lite implementation)
 * for details see http://dx.doi.org/10.1063/1.4802990
 */
void TAMD::Step(double dt)
{
    // TAMD algorithm
    // for details see http://dx.doi.org/10.1063/1.4802990
    double m;
    static int iter = 0;
    int i;
    Vector3 v;
    if (iter == 0)
    {
        // set BAOAB constants for additional dofs
        g_sdt = fric_s / system->Picosecond() * dt;
        c1_s = exp(-g_sdt);
        c3_s = sqrt(kbt_s * (1 - c1_s * c1_s));
        // initialise additional dof positions and velocities
        // additional dof associated to phi
        s_phi = system->ComputeDihedrals(phi_ind);
        v_phi = 0;
        force_phi = 0;
        // additional dof associated to psi
        s_psi = system->ComputeDihedrals(psi_ind);
        v_psi = 0;
        force_psi = 0;
    }
    //*** end of variable declarations ***

    //*** beginning of iteration ***
    // Velocity half-step (B)
    // velocity step for system vars
    VelocityStep(0.5 * dt);

    // contribution to velocities due to coupling to additional dofs
    for (i = 0; i < 4; i++)
    {
        // the phi coupling contribution to system velocities
        m = system->GetInverseMass(phi_ind[i]);
        v = system->GetVelocity(phi_ind[i]);
        v = v -
            Vector3::Scale(0.5 * dt * m * force_phi,
                           system->ComputeDihedralsJac(phi_ind, phi_ind[i]));
        system->SetVelocity(phi_ind[i], v);
        // the psi coupling contribution to system velocities
        m = system->GetInverseMass(psi_ind[i]);
        v = system->GetVelocity(psi_ind[i]);
        v = v -
            Vector3::Scale(0.5 * dt * m * force_psi,
                           system->ComputeDihedralsJac(psi_ind, psi_ind[i]));
        system->SetVelocity(psi_ind[i], v);
    }

    // velocity step for additional dofs.
    v_phi += dt * 0.5 / m_s * force_phi;
    v_psi += dt * 0.5 / m_s * force_psi;

    // Position half step (A)
    PositionStep(0.5 * dt);

    // position step for additional dofs.
    TAMDvars_PositionStep(0.5 * dt);

    // Velocity update (O)
    OStep(dt);

    // Velocity update (O) for additional dofs
    v_phi = c1_s * v_phi + sqrt_ms_inv * c3_s * r[0]->random_gaussian();
    v_psi = c1_s * v_psi + sqrt_ms_inv * c3_s * r[0]->random_gaussian();

    // Position half step (A)
    PositionStep(0.5 * dt);

    // position step for additional dofs.
    TAMDvars_PositionStep(0.5 * dt);

    // force updates

    system->UpdateForces();

    // compute forces due to coupling, taking periodicy into account
    force_phi = DihedralDiff(system->ComputeDihedrals(phi_ind), s_phi);
    force_psi = DihedralDiff(system->ComputeDihedrals(psi_ind), s_psi);

    // Velocity half-step (B)
    VelocityStep(0.5 * dt);

    // contribution to velocities due to coupling to additional dofs
    for (i = 0; i < 4; i++)
    {
        // the phi coupling contribution to system velocities
        m = system->GetInverseMass(phi_ind[i]);
        v = system->GetVelocity(phi_ind[i]);
        v = v -
            Vector3::Scale(0.5 * dt * m * force_phi,
                           system->ComputeDihedralsJac(phi_ind, phi_ind[i]));
        system->SetVelocity(phi_ind[i], v);
        // the psi coupling contribution to system velocities
        m = system->GetInverseMass(psi_ind[i]);
        v = system->GetVelocity(psi_ind[i]);
        v = v -
            Vector3::Scale(0.5 * dt * m * force_psi,
                           system->ComputeDihedralsJac(psi_ind, psi_ind[i]));
        system->SetVelocity(psi_ind[i], v);
    }

    // velocity step for additional dofs.
    v_phi += dt * 0.5 / m_s * force_phi;
    v_psi += dt * 0.5 / m_s * force_psi;

    // *** end of time step *** //

    // write force out
    static std::ofstream outfile;
    // File initialization
    if (iter == 0)
        outfile.open("tamd_out.dat");
    // write force to file dihedrals.dat
    if (iter % save_freq == 0)
    {
        outfile << force_phi << "\t" << force_psi << "\t" << s_phi << "\t"
                << s_psi << std::endl;
        outfile.flush();
    }
    // if (iter % 100 == 0) write_confxyz(coupl, xi);
    iter += 1;
}
