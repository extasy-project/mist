/*******************************************************************************
*                                                                              *
*             MIST - Molecular Integration Simulation Toolkit                  *
*         A library of integration algorithms for Molecular Dynamics           *
*                                                                              *
*            Copyright (c) 2014-2020, The University of Edinburgh              *
*                            All rights reserved.                              *
*                     List of contributors in AUTHORS                          *
*                                                                              *
*            MIST is free software.  See LICENSE for details                   *
*                                                                              *
*******************************************************************************/

/**
 * @file Yoshida4Integrator.cpp
 * @brief Implementation of a fourth-order constant energy integrator
 */

#include "Yoshida4Integrator.h"
#include "ConstraintSolver.h"
#include "IO.h"
#include "System.h"

/**
 * After superclass constructor, simply prints a message.
 * Positions and velocities are integrated from t -> t + dt
 */
Yoshida4Integrator::Yoshida4Integrator(Param *p)
    : Integrator(p, "Yoshida 4th Order")
{
    features |= MIST_FEATURE_POS_VEL_NO_OFFSET;

    massrequirements = MIST_INV_MASS_REQD;

    conservesMomentum = true;
}

/**
 * Does nothing
 */
Yoshida4Integrator::~Yoshida4Integrator() {}

/**
 * Computes a full-step update by three intermediate scaled steps,
 * following the scheme described in
 * https://dx.doi.org/10.1016%2F0375-9601%2890%2990092-3
 */
void Yoshida4Integrator::Step(double dt)
{
    // Yoshida 4th order algorithm

    double d[3] = {1.351207192, -1.702414384, 1.351207192};

    for (int j = 0; j < 3; j++)
    {
        // Velocity half-step
        VelocityStep(0.5 * dt * d[j]);

        // Position full step
        PositionStep(dt * d[j]);

        system->UpdateForces();

        // Velocity half-step
        VelocityStep(0.5 * dt * d[j]);
    }
}
