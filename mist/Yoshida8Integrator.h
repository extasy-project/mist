/*******************************************************************************
*                                                                              *
*             MIST - Molecular Integration Simulation Toolkit                  *
*         A library of integration algorithms for Molecular Dynamics           *
*                                                                              *
*            Copyright (c) 2014-2020, The University of Edinburgh              *
*                            All rights reserved.                              *
*                     List of contributors in AUTHORS                          *
*                                                                              *
*            MIST is free software.  See LICENSE for details                   *
*                                                                              *
*******************************************************************************/

/**
 * @file Yoshida8Integrator.h
 * @brief Declaration of a Yoshida order-8 Integrator class
 */

#ifndef MIST_YOSHIDA8INTEGRATOR_H
#define MIST_YOSHIDA8INTEGRATOR_H

#include "Integrator.h"

/**
 * @brief Declaration of the Yoshida 8th order constant-energy Integrator class
 *
 */
class Yoshida8Integrator : public Integrator
{
  public:
    /**
     * @brief Default Constructor
     * @param p a pointer to the parameters
     */
    Yoshida8Integrator(Param *p);

    /**
     * @brief Default Destructor
     */
    virtual ~Yoshida8Integrator();

    /**
     * @brief Integrate positions and velocities over time dt using
     * Yoshida's 8th-order algorithm.
     *
     * @param dt the timestep in internal units
     */
    void Step(double dt);
};

#endif
