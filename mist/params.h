/*******************************************************************************
*                                                                              *
*             MIST - Molecular Integration Simulation Toolkit                  *
*         A library of integration algorithms for Molecular Dynamics           *
*                                                                              *
*            Copyright (c) 2014-2020, The University of Edinburgh              *
*                            All rights reserved.                              *
*                     List of contributors in AUTHORS                          *
*                                                                              *
*            MIST is free software.  See LICENSE for details                   *
*                                                                              *
*******************************************************************************/

/**
 * @file params.h
 * @brief API for reading parameters from a file
 *
 * This header file describes functions and types that others can use for
 * accessing parameters read from a file.
 *
 * Original code by Nick Brown
 *
 */

#ifndef MIST_PARAM_H
#define MIST_PARAM_H

/**
 * @defgroup PARAM_BOOLEANS Helper booleans
 *
 * @brief Macros for boolean True/False values
 *
 * @{
 */
#define TRUE 1  /**< @brief True */
#define FALSE 0 /**< @brief False (not true) */
                /** @} */

/**
 * @defgroup PARAM_ERRS Parameter error codes
 *
 * @brief Macros for error values which may be returned by parameter functions
 *
 * @{
 */
#define ERR_OK 0         /**< @brief Success */
#define ERR_NOFILE 1     /**< @brief Specified file could not be opened */
#define ERR_INPUTVALUE 3 /**< @brief Specified key could not be found */
#define ERR_FREE 4       /**< @brief Error freeing parameter memory */
#define ERR_NOPARAM 5    /**< @brief A parameter object was NULL */
#define ERR_ARRAY 6      /**< @brief An array was malformed */
/** @} */

typedef unsigned char boolean; /**< @brief Used to represent a true or false
                                  value, quite useful */

/**
 * @defgroup PARAM_FUNCTIONS Parameter functions
 *
 * @brief Functions for dealing with parameters read from file
 *
 * @{
 */

/**
 * @brief A key-value parameter pair
 *
 * A key-value pair of strings representing a single parameter keyword and value
 * from the input file.  Contains pointers to the previous and next parameter,
 * forming a doubly-linked list.
 *
 */
typedef struct paramstructure
{
    char *key;   /**< @brief The parameter name/label (string) **/
    char *value; /**< @brief The parameter value (string) **/
    /** @brief Pointer to the previous parameter **/
    struct paramstructure *prev;
    struct paramstructure *next; /**< @brief Pointer to the next parameter **/
} Param;

/**
 * @brief Raw representation of parameters array
 *
 * An array of null-terminated strings containing the text read from the
 * parameters file
 *
 */
typedef struct
{
    int size;    /**< @brief Length of the string array **/
    char **data; /**< @brief Array of parameters (strings) **/
} ParamArrayPackage;

/**
 * @brief Read params from a file
 *
 * @param filename C string containing filename to be read
 * @param root Pointer to a Param*, will be allocated and returned
 * @return ERR_OK on success, ERR_NOFILE if specified file could not be opened
 */
int readParams(const char *filename, Param **root);

/**
 * @brief Retrieves a specific parameter value when given the corresponding key
 *
 * @param root Pointer to the root of the Param data structure
 * @param key C string containing the key to search for
 * @param result Pointer to a C string which will contain the parameter value
 *
 * @return ERR_OK on success, ERR_INPUTVALUE if the key could not be found
 */
int getParamStringValue(Param *root, const char *key, char **result);

/**
 * @brief Retrieve a specific parameter integer value when given the
 * corresponding key
 *
 * @param root Pointer to the root of the Param data structure
 * @param key C string containing the key to search for
 * @param result Pointer to an int which will contain the parameter value
 *
 * @return ERR_OK on success, ERR_INPUTVALUE if the key could not be found
 */
int getParamIntValue(Param *root, const char *key, int *result);

/**
 * @brief Retrieve a specific parameter boolean value when given the
 * corresponding key
 *
 * @param root Pointer to the root of the Param data structure
 * @param key C string containing the key to search for
 * @param result Pointer to a boolean which will contain the parameter value
 *
 * @return ERR_OK on success, ERR_INPUTVALUE if the key could not be found
 */
int getParamBooleanValue(Param *root, const char *key, boolean *result);

/**
 * @brief Will retrieve a specific double value matching the provided key
 *
 * @param root Pointer to the root of the Param data structure
 * @param key C string containing the key to search for
 * @param result Pointer to a double which will contain the parameter value
 *
 * @return ERR_OK on success, ERR_INPUTVALUE if the key could not be found
 */
int getParamDoubleValue(Param *root, const char *key, double *result);

/**
 * @brief Gets an Param array with corresponding key
 *
 * @param root Pointer to the root of the Param data structure
 * @param key C string containing the key to search for
 * @param result Pointer to a ParamArrayPackage* corresponding to the given key
 *
 * @return ERR_OK on success, ERR_INPUTVALUE if the key could not be found
 */
int getParamArray(Param *root, const char *key, ParamArrayPackage **result);

/**
 * @brief Converts the array package into boolean data
 *
 * @param arrayPackage A pointer to a ParamArrayPackage to be converted
 *
 * @return An array of booleans of the same size as the ParamArrayPackage
 */
boolean *getArrayBooleanValues(ParamArrayPackage *arrayPackage);

/**
 * @brief Converts the array package into integer data
 *
 * @param arrayPackage A pointer to a ParamArrayPackage to be converted
 *
 * @return An array of ints of the same size as the ParamArrayPackage
 */
int *getArrayIntValues(ParamArrayPackage *arrayPackage);

/**
 * @brief Converts the array package into double data
 *
 * @param arrayPackage A pointer to a ParamArrayPackage to be converted
 *
 * @return An array of doubles of the same size as the ParamArrayPackage
 */
double *getArrayDoubleValues(ParamArrayPackage *arrayPackage);

/**
 * @brief Frees all memory associate with an array package
 *
 * @param arrayPackage A pointer to a ParamArrayPackage to be freed
 *
 * @return ERR_OK on success, ERR_FREE on failure
 */
int freeParamArray(ParamArrayPackage *arrayPackage);

/**
 * @brief Frees all memory taken for parameters
 *
 * @return ERR_OK on success, ERR_FREE on failure
 */
int freeParams(Param *);

/**
 * @brief Create a new parameter from key/value strings
 *
 * @param key C string containing the key
 * @param value C string containing the value
 *
 * @return A pointer to a new param
 */
Param *createParamFromTuple(const char *key, const char *value);

/**
 * @brief Add a new parameter
 *
 * @param p the existing parameters
 * @param n the new parameters
 *
 * @return ERR_OK on success, ERR_NOPARAM if either argument was NULL
 */
int addParam(Param *p, Param *n);

/** @} */

#endif /* __PARAM_H */
