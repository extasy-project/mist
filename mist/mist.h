/*******************************************************************************
*                                                                              *
*             MIST - Molecular Integration Simulation Toolkit                  *
*         A library of integration algorithms for Molecular Dynamics           *
*                                                                              *
*            Copyright (c) 2014-2020, The University of Edinburgh              *
*                            All rights reserved.                              *
*                     List of contributors in AUTHORS                          *
*                                                                              *
*            MIST is free software.  See LICENSE for details                   *
*                                                                              *
*******************************************************************************/

/**
 * @file mist.h
 * @brief MIST library interface definition
 *
 * This is the collection of function calls that may be added to a MD code in
 * order for it to use MIST for integration
 */

#ifdef __MIST_WITH_MPI
#include <mpi.h>
#endif

#ifdef __cplusplus
extern "C"
{
#endif

/** @brief The MIST_API attribute marks a function as part of the library API */
#if defined(_WIN32) && defined(__MIST_AS_DLL)
#ifdef __MIST_EXPORTS
#define MIST_API __declspec(dllexport)
#else
#define MIST_API __declspec(dllimport)
#endif
#else
#define MIST_API
#endif

#include <stdbool.h>

/**
 * @defgroup FEATURE_FLAGS MIST features flags
 *
 * @brief combinations of these flags may be returned by MIST_GetFeatures()
 *
 * MIST Integrators may require certain features from the host application,
 * which might be expensive or difficult to implement.  Codes may call
 * MIST_GetFeatures()
 * to determine which features are required by the selected integrator, and
 * either turn in those features in the code, or exit if they are not
 * implemented.
 * @{
 */
/** @brief MIST requires no special features */
#define MIST_FEATURE_NONE 0
/** @brief MIST requires access to components of the force-field */
#define MIST_FEATURE_FORCE_COMPONENTS 1
/** @brief MIST requires positions and velocities at same time (t) at the start
 * of the step */
#define MIST_FEATURE_POS_VEL_NO_OFFSET 2
/** @brief MIST requires positions at time (t) and velocities at (t + dt/2) at
 * the start of the step */
#define MIST_FEATURE_POS_VEL_OFFSET_PLUS_HALF_DT 4
/** @brief MIST requires the computation of the energies at every timestep */
#define MIST_FEATURE_REQUIRES_ENERGY_WITH_FORCES 8
/** @brief MIST requires access to the particle kinds i.e. the integrator calls
 * GetKind() */
#define MIST_FEATURE_REQUIRES_KINDS 16
/** @brief MIST may modify the lattice vectors i.e. the integrator calls
 * SetCell() */
#define MIST_FEATURE_MODIFIES_CELL 32
/** @brief MIST requires the computation of the pressure at every timestep */
#define MIST_FEATURE_REQUIRES_PRESSURE_WITH_FORCES 64
/** @} */

/**
 * @defgroup ERROR_CODES MIST error codes
 *
 * @brief Error codes which may be returned from the MIST library interface:
 * @{
 */
/** @brief MIST library call exited successfully **/
#define MIST_OK 0
/** @brief mist.params file was missing or could not be read **/
#define MIST_NO_SETUP_FILE 10
/** @brief MIST failed to construct a valid System  **/
#define MIST_INVALID_SYSTEM 11
/** @brief MIST was passed a NULL pointer **/
#define MIST_INVALID_POINTER_IN 12
/** @brief MIST does not have a non-NULL pointer to return **/
#define MIST_INVALID_POINTER_OUT 13
/** @brief MIST was passed an invalid parameter (e.g. out of range) **/
#define MIST_INVALID_VALUE_IN 14
/** @brief An option was selected that was not valid **/
#define MIST_INVALID_OPTION 15
/** @brief MIST failed to construct a valid Integrator  **/
#define MIST_INVALID_INTEGRATOR 16
/** @brief A MIST API call was made out-of-sequence **/
#define MIST_INCORRECT_CALL_ORDER 17
    /** @} */

    /**
     * @defgroup MIST_API MIST Library API
     *
     * @brief The MIST library API
     *
     * A C interface to MIST which may be called by host MD applications to make
     *use of MIST integrators.
     * @{
     */

    /**
     * @brief Initialises the MIST library
     *
     * This performs all of initialisation tasks in the MIST library, including
     * reading in the mist.params (if parameters were not already specified by
     * calls to MIST_SetParam().
     *
     * @return zero on success, non-zero otherwise.
     */
    MIST_API int MIST_Init();

#ifdef __MIST_WITH_MPI
    /**
     * @brief Initialises the MIST library in an MPI environment
     *
     * This performs all of initialisation tasks in the MIST library, including
     * reading in the mist.params (if parameters were not already specified by
     * calls to MIST_SetParam().
     *
     * @param comm the MPI communicator to be used by MIST
     *
     * @return zero on success, non-zero otherwise.
     */
    MIST_API int MIST_Init_MPI(MPI_Comm comm);

    /**
     * @brief Initialises the MIST library in a Fortran MPI environment
     *
     * This is just a thin wrapper around MIST_Init_MPI()
     *
     * @param fcomm the MPI communicator to be used by MIST
     *
     * @return zero on success, non-zero otherwise.
     */
    MIST_API int MIST_Init_MPI_Fortran(MPI_Fint fcomm);
#endif

    /**
     * @brief Sets the number of particles to be integrated.
     *
     * This tells the MIST library how many particles exist in the host
     *application, and therefore how many entries in the position, velocity,
     *force arrays to expect.  The number of particles is assumed to be fixed
     *for the duration of the simulation.
     *
     * @param n the number of particles.
     * @return zero on success, non-zero otherwise.
     */
    MIST_API int MIST_SetNumParticles(int n);

    /**
     * @brief Passes MIST a pointer to the simulation cell data.
     *
     * MIST needs to be able to read and modify the simulation cell. This
     *function takes a pointer to cell data in the host application and stores
     *it for later use in MIST.  The data could be an array of 3 lattice
     *vectors, or a more complex data structure.
     *
     * @param cell pointer to the cell data
     * @return zero on success, non-zero otherwise.
     */

    MIST_API int MIST_SetCell(void *cell);

    /**
     * @brief Passes MIST a pointer to the particle positions
     *
     * MIST needs to be able to read and modify the particle positions.  This
     *function takes a pointer to position data in the host application and
     *stores it for later use in MIST.  This is usually an array of 3-vectors,
     *but may be a more complex data structure.  If the location of the data
     *changes for any reason, you must call MIST_SetPositions() again.
     *
     * @param positions pointer to the position data
     * @return zero on success, non-zero otherwise
     */
    MIST_API int MIST_SetPositions(void *positions);

    /**
     * @brief Passes MIST a pointer to the particle velocities
     *
     * MIST needs to be able to read and modify the particle velocities.  This
     *function takes a pointer to velocity data in the host application and
     *stores it for later use in MIST.  This is usually an array of 3-vectors,
     *but may be a more complex data structure.  If the location of the data
     *changes for any reason, you must call MIST_SetVelocities() again.
     *
     * @param velocities pointer to the velocity data
     * @return zero on success, non-zero otherwise
     */
    MIST_API int MIST_SetVelocities(void *velocities);

    /**
     * @brief Passes MIST a pointer to the particle masses
     *
     * MIST needs to be able to read the particle masses.  This function takes a
     *pointer to mass data in the host application and stores it for later use
     *in MIST.  This may be for example an array of masses, an array of inverse
     *masses, or a more complex particle data structure.  If the location of the
     *data changes for any reason, you must call MIST_SetMasses() again.
     *
     * @param masses pointer to the mass data
     * @return zero on success, non-zero otherwise
     */
    MIST_API int MIST_SetMasses(void *masses);

    /**
     * @brief Passes MIST a pointer to the particle kind labels
     *
     * MIST needs to be able to read the particle kind labels, typically short
     *strings containing the element labels (H, C, N ...).  This may be
     *presented as an array of strings or as part of a more complex particle
     *data structure.  If the location of the data changes for any reason, you
     *must call MIST_SetKinds() again.
     *
     * @param kinds pointer to the kind data
     * @return zero on success, non-zero otherwise
     */
    MIST_API int MIST_SetKinds(void *kinds);

    /**
     * @brief Passes MIST a pointer to the potential energies
     *
     * MIST needs to be able to read the potential energy computed by the host
     *application for a given configuration of particles.  As well as the total
     *potential energy, MIST must also be able to access particular components
     *of the force-field - Bond, Angle, Dihedral etc.. The pointer passed to
     *MIST here must allow access to each of these components.  If the location
     *of the data changes, you must call MIST_SetPotentialEnergy() again.
     *
     * @param potential_energy pointer to the potential energy data
     * @return zero on success, non-zero otherwise
     */
    MIST_API int MIST_SetPotentialEnergy(void *potential_energy);

    /**
     * @brief Passes MIST a pointer to the pressure
     *
     * MIST needs to be able to read the (scalar) pressure computed by the host
     *application for a given configuration of particles and box. If the
     *location of the data changes, you must call MIST_SetPressure() again.
     *
     * @param pressure pointer to the pressure data
     * @return zero on success, non-zero otherwise
     */
    MIST_API int MIST_SetPressure(void *pressure);

    /**
     * @brief Passes MIST a pointer to the forces
     *
     * MIST needs to be able to read the forces computed by the host application
     *for a given configuration of particles.  As well as the total force on
     *each particle, MIST must also be able to access particular components of
     *the force-field - Bond, Angle, Dihedral etc. The pointer passed to MIST
     *here must allow access to each of these components.  If the location of
     *the data changes, you must call MIST_SetForces() again.
     *
     * @param forces pointer to the force data
     * @return zero on success, non-zero otherwise
     */
    MIST_API int MIST_SetForces(void *forces);

    /**
     * @brief Sets the number of bonds
     *
     * This tells the MIST library how many bonds exist in the host application,
     *and therefore how many entries in the bonds array will be created.
     *The number of particles is assumed to be fixed for the duration of the
     *simulation.
     *
     * @param n the number of bonds
     * @return zero on success, non-zero otherwise
     */
    MIST_API int MIST_SetNumBonds(int n);

    /**
     * @brief Sets up a bond according to the given parameters
     *
     * Bond i is initialised with the given parameters
     *
     * @param i which bond to set
     * @param a first particle index
     * @param b second particle index
     * @param rab distance between the two particles
     * @param hydrogen if the bond connects a hydrogen atom
     * @return zero on success, non-zero otherwise
     */
    MIST_API int MIST_SetBond(int i, int a, int b, double rab, bool hydrogen);

    /**
     * @brief Returns the application features required by MIST
     *
     * To determine what functionality is needed by the MIST library to perform
     *integration, host application codes may call MIST_GetFeatures().  This
     *will return a logical-OR'ed combination of the MIST feature flags.
     * @sa FEATURE_FLAGS
     *
     * @param features logical-OR'ed combination of the MIST feature flags
     *
     * @return zero on success, non-zero otherwise
     */
    MIST_API int MIST_GetFeatures(int *features);

    /**
     * @brief Makes a single MD step using MIST
     *
     * A call to MIST_Step() hands over control of execution from the host
     *appilcation to the MIST library to perform a single MD step of duration
     *dt. The call will return when the step has been completed.  MIST may make
     *callbacks during the step to update forces.
     *
     * @param dt MD time-step (in host application's units)
     *
     * @return zero on success, non-zero otherwise
     */
    MIST_API int MIST_Step(double dt);

    /**
     * @brief Closes down the MIST library, ready for exit
     *
     * Once the MD run has been completed, the MIST library should be shut down
     *by calling MIST_Cleanup(), to properly deallocate internal state variables
     *ready for application exit
     *
     * @return zero on success, non-zero otherwise
     */
    MIST_API int MIST_Cleanup();

    /**
     * @brief Passes MIST a function pointer from the host application which
     * will be used to compute new forces
     *
     * MIST modifies particle positions and velocities during the call to
     *MIST_Step().  To get updated forces and potential energies, MIST must be
     *able to make a callback to the host application to execute the force
     *calculation. MIST_SetForceCallback registers a function pointer and
     *application data which will be used to execute the callback.
     *
     * @param callback a pointer to the host application function used to
     *calculate forces
     * @param data pointer to a data structure in the host application
     *containing parameters for the force evaluation routines
     * @return zero on success, non-zero otherwise
     */
    MIST_API int MIST_SetForceCallback(void (*callback)(void *), void *data);

    /**
     * @brief Sets MIST integrator parameters
     *
     * By default, MIST will attempt to read integrator parameters from the
     * mist.params file.  It is possible to set parameters instead using this
     * API.  All parameters must be set before calling MIST_Init()
     *
     * @param key A C string containing the name of the parameter to set
     * @param value A C string containing the value of the parameter
     *
     * @return zero on success, non-zero otherwise
     */
    MIST_API int MIST_SetParam(const char *key, const char *value);

    /**
     * @brief Gets the number of degrees of freedom in the system
     *
     * Depending on the properties of the integrator, the number of constraints
     * and whether the system uses PBC or not, the number of degrees of
     * freedom is not necessarily 3*nparticles.  This call can be used by
     * applications to determine the correct value, e.g. for computing
     * the temperature
     *
     * @param ndof pointer to an int, where the number of DOFs will be stored
     *
     * @return zero on success, non-zero otherwise
     */
    MIST_API int MIST_GetNDoF(int *ndof);

    /**
     * @brief Set the units system to be used by MIST
     *
     * The units system of the host MD code is defined in terms of standard
     * units of length, energy, time and mass.  These are then used by MIST to
     * convert input parameters into the correct units system.
     *
     * @param boltzmann Boltzmann's constant in internal units
     * @param picosecond one picosecond in internal units
     * @param amu one AMU in internal units
     * @param angstrom one Angstrom in internal units
     * @param atmosphere one Atmosphere in internal units
     *
     * @return zero on success, non-zero otherwise
     */
    MIST_API int MIST_SetUnitSystem(double boltzmann, double picosecond,
                                    double amu, double angstrom,
                                    double atmosphere);

    /** @} */

#ifdef __cplusplus
}
#endif
