/*******************************************************************************
*                                                                              *
*             MIST - Molecular Integration Simulation Toolkit                  *
*         A library of integration algorithms for Molecular Dynamics           *
*                                                                              *
*            Copyright (c) 2014-2020, The University of Edinburgh              *
*                            All rights reserved.                              *
*                     List of contributors in AUTHORS                          *
*                                                                              *
*            MIST is free software.  See LICENSE for details                   *
*                                                                              *
*******************************************************************************/

/**
 * @file ForciteSystem.h
 * @brief Declaration of specific system class for Forcite
 */

#ifndef MIST_FORCITE_SYSTEM_H
#define MIST_FORCITE_SYSTEM_H
#include "System.h"

/**
 * @brief A System class which provides accessors for particle data inside the
 * Forcite code
 *
 * This is a specialisation of the System interface which is specific to the
 * Forcite code.  It provides access to an Integrator to the particle data
 * stored in various arrays within Forcite, and also the ability to compute
 * updates forces, calling Forcite's force evaluation function by means of a
 * callback.  This implementation is a stub, the real implementation is
 * property of Dassault Systemes BIOVIA.
 *
 */
class ForciteSystem : public System
{
  public:
    /**
     * @brief Default Constructor
     *
     * Sets the Forcite-specific unit conversion factors
     */
    ForciteSystem();

    /**
     * @brief Default Destructor
     *
     * Does nothing
     */
    ~ForciteSystem();

    // Interface to Integrators
    Vector3 GetVelocity(int i) const;
    void SetVelocity(int i, Vector3 v);
    Vector3 GetPosition(int i) const;
    void SetPosition(int i, Vector3 p);
    void SetMassPtr(void *m, int req);
    void GetCell(Vector3 *a, Vector3 *b, Vector3 *c);
    void SetCell(Vector3 a, Vector3 b, Vector3 c);

    Vector3 GetForce(int i) const;
    Vector3 GetForce(int i, force_component_t t) const;
    double GetMass(int i) const;
    double GetInverseMass(int i) const;
    double GetInverseSqrtMass(int i) const;
    double GetPressure() const;
    double GetPotentialEnergy() const;
    double GetPotentialEnergy(force_component_t t) const;
    char *GetKind(int i) const;
};

#endif
