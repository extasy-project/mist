/*******************************************************************************
*                                                                              *
*             MIST - Molecular Integration Simulation Toolkit                  *
*         A library of integration algorithms for Molecular Dynamics           *
*                                                                              *
*            Copyright (c) 2014-2020, The University of Edinburgh              *
*                            All rights reserved.                              *
*                     List of contributors in AUTHORS                          *
*                                                                              *
*            MIST is free software.  See LICENSE for details                   *
*                                                                              *
*******************************************************************************/

/**
 * @file ISST.h
 * @brief Declaration of Infinite Switch Simulated Tempering integrator class.
 */

#ifndef MIST_ISST_H
#define MIST_ISST_H

#include "LangevinIntegrator.h"

#include <fstream>

/**
 * @brief A simple type for storing a running average
 *
 * Stores value in double-precision
 */
struct AverageObservable
{
    double average; /**< @brief stores the running average **/
    double N;       /**< @brief store the number of observations **/
    AverageObservable() : average(0.0), N(0.0) {}
    ~AverageObservable() {}
    /** @brief add a single observation to the running average
     *
     *   @param value the new observation
     */
    void add_observation(const double &value)
    {
        N++;
        average = 1.0 / N * value + (N - 1.0) / N * average;
    }
};

/**
 * @brief Declaration of the Infinite Switch Simulated Tempering integrator
 * class
 *
 * Infinite Switch Simulated tempering algorithm of Martinsson et al
 * https://arxiv.org/abs/1809.05066
 */
class ISST : public LangevinIntegrator
{
  public:
    /**
     * @brief Default Constructor
     *
     * @param p a pointer to the parameters
     */
    ISST(Param *p);

    /**
     * @brief Default Destructor
     */
    virtual ~ISST();

    /**
     * @brief Integrate positions and velocities over time dt using
     * ISST algorithm
     *
     * @param dt the timestep in internal units
     */
    void Step(double dt);

    /**
     * @brief Setter for the system pointer
     *
     * @param s pointer to an instance of the System class
     */
    virtual void SetSystem(System *s);

  protected:
    /**
     * @brief Integrates all particle velocities by dt, with rescaled forces
     *
     * @param dt the time interval to integrate over
     */
    void VelocityStep(double dt);

    /**
     * @brief Compute the weights and update the force rescaling factor
     *
     * @param dt the timestep in internal units
     */
    void ApplyForceRescaling(const double &dt);

    /**
     * @brief Updated the internally stored infinite switch simulated
     * tempering force rescaling
     */
    void UpdateForceRescaling();

    /**
     * @brief Update the infinite switch simulated tempering
     * temperature weights
     *
     * @param dt the timestep in internal units
     */
    void UpdateBetaWeights(const double &dt);

    /**
     * @brief Normalise the internally stored infinite switch simulated
     * tempering reciprocal temperature weights
     */
    void NormaliseBetaWeights();

    /**
     * @brief Calculates and prints all the observable weights to file
     */
    void WriteObservableWeights();

    double temp_min; /**< @brief Temperature min (in K) */
    double temp_max; /**< @brief Temperature max (in K) */

    double beta_min; /**< @brief lower limit inverse temp region */
    double beta_max; /**< @brief upper limit inverse temp region */
    double tau;      /**< @brief scaling of timestep learning partition func. */
    int n_interpolation; /**< @brief number of interpolation temps */

    double *gauss_weight; /**< @brief the weights of the legendre polynomial
                             basis */
    double *beta;         /**< @brief 1/kbT for each temperature */
    double *
        beta_weight; /**< @brief the weights associated with each temperature */

    double force_rescaling; /**< @brief the current rescaling factor for the
                               forces */
    AverageObservable *partition_estimate; /**< @brief current estimate for the
                                              partition function */
    int period; /**< @brief number of steps between attempts to switch
                   temperature */
    int step;   /**< @brief step counter */
    std::ofstream outfile; /**< @brief output file for ISST data */
};

#endif
