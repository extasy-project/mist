/*******************************************************************************
*                                                                              *
*             MIST - Molecular Integration Simulation Toolkit                  *
*         A library of integration algorithms for Molecular Dynamics           *
*                                                                              *
*            Copyright (c) 2014-2020, The University of Edinburgh              *
*                            All rights reserved.                              *
*                     List of contributors in AUTHORS                          *
*                                                                              *
*            MIST is free software.  See LICENSE for details                   *
*                                                                              *
*******************************************************************************/

#include "AmberSystem.h"
#include "IO.h"

#include <cmath>
#include <stddef.h>
#include <stdlib.h>
#include <strings.h>

// Implementation of the Amber System class

AmberSystem::AmberSystem() : System()
{
    // Amber specific conversion factors
    boltzmann = 0.001987191; // in kcal/mol/K
    picosecond = 20.455;     // in internal units
    amu = 1.0;               // in AMU
    angstrom = 1.0;          // in Angstrom
    atm = 1.0 / 0.987;       // in bar

    local_atoms = NULL;
}

AmberSystem::~AmberSystem()
{
    // Don't free masses, it's a pointer to an Amber array

    if (invmasses != NULL)
    {
        delete[] invmasses;
        invmasses = NULL;
    }

    if (invsqrtmasses != NULL)
    {
        delete[] invsqrtmasses;
        invsqrtmasses = NULL;
    }
}

Vector3 AmberSystem::GetVelocity(int i) const
{
    int idx = i;
    if (local_atoms != NULL)
        idx = local_atoms[i] - 1;

    double x = ((double *)velocities)[3 * idx];
    double y = ((double *)velocities)[3 * idx + 1];
    double z = ((double *)velocities)[3 * idx + 2];
    return Vector3(x, y, z);
}

void AmberSystem::SetVelocity(int i, Vector3 v)
{
    int idx = i;
    if (local_atoms != NULL)
        idx = local_atoms[i] - 1;

    ((double *)velocities)[3 * idx] = v.x;
    ((double *)velocities)[3 * idx + 1] = v.y;
    ((double *)velocities)[3 * idx + 2] = v.z;
}

Vector3 AmberSystem::GetPosition(int i) const
{
    int idx = i;
    if (local_atoms != NULL)
        idx = local_atoms[i] - 1;

    double x = ((double *)positions)[3 * idx];
    double y = ((double *)positions)[3 * idx + 1];
    double z = ((double *)positions)[3 * idx + 2];
    return Vector3(x, y, z);
}

void AmberSystem::SetPosition(int i, Vector3 p)
{
    int idx = i;
    if (local_atoms != NULL)
        idx = local_atoms[i] - 1;

    ((double *)positions)[3 * idx] = p.x;
    ((double *)positions)[3 * idx + 1] = p.y;
    ((double *)positions)[3 * idx + 2] = p.z;
}

double AmberSystem::GetPressure() const
{
    if (pbc != true)
        return 0.0; // Pressure is not defined for non-periodic systems

    // TODO check
    return *(double *)pressure;
}

double AmberSystem::GetPotentialEnergy() const
{
    return GetPotentialEnergy(MIST_FORCE_ALL);
}

double AmberSystem::GetPotentialEnergy(force_component_t t) const
{
    typedef struct
    {
        double total;
        double bond;
        double bend;
        double propdihed;
        double impropdihed;
        double nonbond;
    } mist_energy_t;

    double e;

    switch (t)
    {
    case (MIST_FORCE_ALL):
        e = ((mist_energy_t *)potential_energy)->total;
        break;
    case (MIST_FORCE_BOND):
        e = ((mist_energy_t *)potential_energy)->bond;
        break;
    case (MIST_FORCE_BEND):
        e = ((mist_energy_t *)potential_energy)->bend;
        break;
    case (MIST_FORCE_PROPDIHED):
        e = ((mist_energy_t *)potential_energy)->propdihed;
        break;
    case (MIST_FORCE_IMPROPDIHED):
        e = ((mist_energy_t *)potential_energy)->impropdihed;
        break;
    case (MIST_FORCE_NONBONDED):
        e = ((mist_energy_t *)potential_energy)->nonbond;
        break;
    default:
        e = 0.0;
        break;
    }

    return e;
}

Vector3 AmberSystem::GetForce(int i) const
{
    return GetForce(i, MIST_FORCE_ALL);
}

Vector3 AmberSystem::GetForce(int i, force_component_t t) const
{
    double x, y, z;
    int idx = i;
    if (local_atoms != NULL)
        idx = local_atoms[i] - 1;

    switch (t)
    {
    case (MIST_FORCE_ALL):
        x = ((double *)forces)[3 * idx];
        y = ((double *)forces)[3 * idx + 1];
        z = ((double *)forces)[3 * idx + 2];
        break;

    // Access to force components has not been implemented yet
    case (MIST_FORCE_BOND):
        IO::Error("Access to the force components has not been "
                  "implemented yet!\n");
        exit(0);
        break;
    case (MIST_FORCE_BEND):
        IO::Error("Access to the force components has not been "
                  "implemented yet!\n");
        exit(0);
        break;
    case (MIST_FORCE_PROPDIHED):
        IO::Error("Access to the force components has not been "
                  "implemented yet!\n");
        exit(0);
        break;
    case (MIST_FORCE_IMPROPDIHED):
        IO::Error("Access to the force components has not been "
                  "implemented yet!\n");
        exit(0);
        break;
    case (MIST_FORCE_NONBONDED):
        IO::Error("Access to the force components has not been "
                  "implemented yet!\n");
        exit(0);
        break;
    default:
        x = 0.0;
        y = 0.0;
        z = 0.0;
        break;
    }

    return Vector3(x, y, z);
}

void AmberSystem::SetMassPtr(void *m, int massrequirements)
{
    int i;

    if (totalNumParticles < 1)
        return;

    if (massrequirements & MIST_MASS_REQD)
    {
        masses = (double *)m;
    }

    if (massrequirements & MIST_INV_MASS_REQD)
    {
        invmasses = new double[totalNumParticles];
        for (i = 0; i < totalNumParticles; i++)
        {
            invmasses[i] = 1. / (((double *)m)[i]);
        }
    }

    if (massrequirements & MIST_INVSQRT_MASS_REQD)
    {
        invsqrtmasses = new double[totalNumParticles];
        for (i = 0; i < totalNumParticles; i++)
        {
            invsqrtmasses[i] = 1. / sqrt(((double *)m)[i]);
        }
    }
}

double AmberSystem::GetMass(int i) const
{
    int idx = i;
    if (local_atoms != NULL)
        idx = local_atoms[i] - 1;

    return masses[idx];
}

double AmberSystem::GetInverseMass(int i) const
{
    int idx = i;
    if (local_atoms != NULL)
        idx = local_atoms[i] - 1;

    return invmasses[idx];
}

double AmberSystem::GetInverseSqrtMass(int i) const
{
    int idx = i;
    if (local_atoms != NULL)
        idx = local_atoms[i] - 1;

    return invsqrtmasses[idx];
}

char *AmberSystem::GetKind(int i) const
{
    int idx = i;
    if (local_atoms != NULL)
        idx = local_atoms[i] - 1;

    return ((char **)kinds)[idx];
}

void AmberSystem::SetPositionPtr(void *p)
{
    typedef struct
    {
        void *positions;
        void *local_atoms;
    } position_data_t;

    positions = ((position_data_t *)p)->positions;
    local_atoms = (int *)(((position_data_t *)p)->local_atoms);
}

void AmberSystem::SetCellPtr(void *c)
{
    if (c != NULL)
    {
        pbc = true;
    }

    typedef struct
    {
        double *ucell;
        double *recip;
        void (*update_cell_metadata)(void);
    } cell_data_t;

    ucell = ((cell_data_t *)c)->ucell;
    recip = ((cell_data_t *)c)->recip;
    update_cell_metadata = ((cell_data_t *)c)->update_cell_metadata;
}

void AmberSystem::GetCell(Vector3 *a, Vector3 *b, Vector3 *c)
{

    if (pbc != true)
    {
        IO::Error("Called GetCell() when PBC are off!\n");
        return;
    }

    // Amber storage format:
    // ucell(a,b,c)(x,y,z) (column-major order)

    a->x = ucell[0];
    a->y = ucell[3];
    a->z = ucell[6];

    b->x = ucell[1];
    b->y = ucell[4];
    b->z = ucell[7];

    c->x = ucell[2];
    c->y = ucell[5];
    c->z = ucell[8];
}

void AmberSystem::GetCellInverse(Vector3 *a, Vector3 *b, Vector3 *c)
{
    if (pbc != true)
    {
        IO::Error("Called GetCellInverse() when PBC are off!\n");
        return;
    }

    a->x = recip[0];
    a->y = recip[3];
    a->z = recip[6];

    b->x = recip[1];
    b->y = recip[4];
    b->z = recip[7];

    c->x = recip[2];
    c->y = recip[5];
    c->z = recip[8];
}

void AmberSystem::SetCell(Vector3 a, Vector3 b, Vector3 c)
{
    if (pbc != true)
    {
        IO::Error("Called SetCell() when PBC are off!\n");
        return;
    }

    // Check new vectors are valid
    if (a.y != 0.0 || a.z != 0.0 || b.z != 0.0)
    {
        IO::Error("New box vectors are not orthorhombic or triclinic!\n");
        return;
    }

    Vector3 a_inv, b_inv, c_inv;
    GetCellInverse(&a_inv, &b_inv, &c_inv);

    // Convert to lambda coordinates
    Vector3 p;
    for (int i = 0; i < numParticles; i++)
    {
        p = GetPosition(i);
        p.x = a_inv.x * p.x + b_inv.x * p.y + c_inv.x * p.z;
        p.y = b_inv.y * p.y + c_inv.y * p.z;
        p.z = c_inv.z * p.z;
        SetPosition(i, p);
    }

    // Update the box and recompute the inverse
    ucell[0] = a.x;

    ucell[1] = b.x;
    ucell[4] = b.y;

    ucell[2] = c.x;
    ucell[5] = c.y;
    ucell[8] = c.z;

    // Convert back to real positions
    for (int i = 0; i < numParticles; i++)
    {
        p = GetPosition(i);
        p.x = a.x * p.x + b.x * p.y + c.x * p.z;
        p.y = b.y * p.y + c.y * p.z;
        p.z = c.z * p.z;
        SetPosition(i, p);
    }

    // Finally, update Amber's state

    update_cell_metadata();
}
