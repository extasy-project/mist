# Common terms to be used for AMBER, GROMACS, NAMD.

# Common object files
COMMON_OBJS= ConstraintSolver.o\
             ContinuousTempering.o \
             ContinuousTempering_TAMD.o \
             Integrator.o \
             LangevinBAOABIntegrator.o \
             LeapfrogIntegrator.o \
             mist.o \
             MIST_Random.o \
             params.o \
             System.o \
             TAMD.o \
             VerletIntegrator.o \
             Yoshida4Integrator.o \
             Yoshida8Integrator.o

# Common macros.
CXX      ?= g++
CC       ?= gcc
AR        = ar
SED       = sed

# Location of the eigen include directory.
EIGEN_INC = -I../eigen-3.2.8

# Location of the MIST root directory.
MIST_PATH = $(shell pwd)

# Name of the library.
LIB       = libmist.a

# Search path for make.
VPATH     = . patches

# Add own suffices to be used.
.SUFFIXES: .patch .patchin

