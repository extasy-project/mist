/*******************************************************************************
*                                                                              *
*             MIST - Molecular Integration Simulation Toolkit                  *
*         A library of integration algorithms for Molecular Dynamics           *
*                                                                              *
*            Copyright (c) 2014-2020, The University of Edinburgh              *
*                            All rights reserved.                              *
*                     List of contributors in AUTHORS                          *
*                                                                              *
*            MIST is free software.  See LICENSE for details                   *
*                                                                              *
*******************************************************************************/

/**
 * @file LangevinIntegrator.h
 * @brief Declaration of Langevin integrator class.
 */

#ifndef MIST_LANGEVININTEGRATOR_H
#define MIST_LANGEVININTEGRATOR_H

#include "Integrator.h"
#include "MIST_Random.h"

/**
 * @brief Declaration of the Langevin Integrator class
 *
 * Langevin Dynamics based on the BAOAB splitting of Matthews and
 * Leimkuhler for details see http://dx.doi.org/10.1063/1.4802990
 * Optionally, one can use the ABOBA splitting
 */
class LangevinIntegrator : public Integrator
{
  public:
    /**
     * @brief Selects the ordering of the integration steps
     *
     * The integrator implements both the BAOAB (better for configurational
     * averages) and ABOBA splittings.  This enum is used to select one or the
     * other.  BAOAB is the default.
     */
    enum method_t
    {
        LANGEVIN_METHOD_BAOAB,
        LANGEVIN_METHOD_ABOBA
    };

    /**
     * @brief Default Constructor
     *
     * @param p a pointer to the parameters
     */
    LangevinIntegrator(Param *p);

    /**
     * @brief Constructor with name
     *
     * @param p a pointer to the parameters
     * @param name the display name of the integrator
     */
    LangevinIntegrator(Param *p, const char *name);

    /**
     * @brief Default Destructor
     */
    virtual ~LangevinIntegrator();

    /**
     * @brief Integrate positions and velocities over time dt using
     * Langevin Dynamics
     *
     * @param dt the timestep in internal units
     */
    void Step(double dt);

    /**
     * @brief Setter for the system pointer
     *
     * @param s pointer to an instance of the System class
     */
    virtual void SetSystem(System *s);

  protected:
    /**
     * @brief Computes the O part (Ornstein-Uhlenbeck solve) of the BAOAB scheme
     *
     * @param dt the time interval to integrate over
     */
    void OStep(double dt);

    double friction;    /**< @brief Friction parameter (in ps^-1) */
    double temp;        /**< @brief Temperature (in K) */
    unsigned long seed; /**< @brief seed for RNG */
    double kbt;         /**< @brief Boltzmann Temperature factor */
    MIST_Random **r;    /**< @brief the Random Number Generator(s) */
    method_t method;    /**< @brief Which algorithm to use */
};

#endif
