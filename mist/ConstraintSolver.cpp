/*******************************************************************************
*                                                                              *
*             MIST - Molecular Integration Simulation Toolkit                  *
*         A library of integration algorithms for Molecular Dynamics           *
*                                                                              *
*            Copyright (c) 2014-2020, The University of Edinburgh              *
*                            All rights reserved.                              *
*                     List of contributors in AUTHORS                          *
*                                                                              *
*            MIST is free software.  See LICENSE for details                   *
*                                                                              *
*******************************************************************************/

/**
 * @file ConstraintSolver.cpp
 * @brief Implementation of the constraints solver
 * @author RB
 */

#include "ConstraintSolver.h"
#include "IO.h"

#include "strcasecmp.h"
#include <cmath>
#include <fstream>

#include <Eigen/SparseCholesky>
#include <Eigen/SparseCore>

typedef Eigen::SparseMatrix<double>
    SpMat; /**< @brief An Eigen double-precision sparse matrix **/
typedef Eigen::Triplet<double>
    T; /**< @brief An Eigen double-precision triple **/

#include <Eigen/Sparse>
using namespace Eigen;

// below: declares a column-major sparse matrix type of double and a triplet
// type (triplets encode a non-zero entry in the form [row_index, col_index,
// value])
static SpMat R; /** @brief Matrix R stored as an Eigen Sparse matrix **/
static Eigen::SimplicialLDLT<SpMat, Eigen::Upper>
    solver; /** @brief Eigen sparse matrix solver used to factorise R **/

/**
 * Creates the solver, storing the provided pointer to the user's
 * parameters and the system which will be constrainted
 */
ConstraintSolver::ConstraintSolver(Param *p, System *s, Integrator *i)
{
    params = p;
    system = s;

    // Default values
    TOL = 1e-8;
    Num_iter_max = 100;

    int err;
    // Read in level of constraints from mist.params file
    char *type;
    err = getParamStringValue(p, "constraints", &type);
    if (err != ERR_OK)
    {
        IO::Warning("Constraints not specified, assuming none.\n");
        constraints = CONSTRAINTS_OFF;
    }
    else if (strcasecmp(type, "off") == 0)
    {
        IO::Info("Constraints disabled.  All bonds are flexible.\n");
        constraints = CONSTRAINTS_OFF;
    }
    else if (strcasecmp(type, "h-bonds-only") == 0)
    {
        IO::Info("Constraining bonds to hydrogen atoms, the rest are "
                 "flexible.\n");
        constraints = CONSTRAINTS_H_BONDS_ONLY;
    }
    else if (strcasecmp(type, "all-bonds") == 0)
    {
        IO::Info("Constraining all bonds\n");
        constraints = CONSTRAINTS_ALL_BONDS;
    }
    else
    {
        IO::Warning("Your choice of constraints (%s) is not valid. "
                    "Constraints will be turned off.\n",
                    type);
    }

#ifdef __MIST_WITH_MPI
    if (constraints != CONSTRAINTS_OFF)
    {
        // Check if we are running in parallel, if so then turn constraints off
        // (for now)
        int p;

        // Using the IO communicator here is a hack, but this code will
        // be removed once parallel constraints are implemented, so we
        // live with it for now.
        MPI_Comm_size(IO::GetCommunicator(), &p);
        if (p != 1)
        {
            IO::Warning(
                "Constraints are not supported for MPI parallel calculations. "
                "Constraints will be turned off.\n");
            constraints = CONSTRAINTS_OFF;
        }
    }
#endif

    if (constraints != CONSTRAINTS_OFF)
    {
        Integrator::ReadDoubleParam(p, &TOL, "constraints_tol", "");
        Integrator::ReadIntParam(p, &Num_iter_max, "constraints_iter_max", "");

        err = getParamStringValue(p, "constraints_method", &type);
        if (err != ERR_OK)
        {
            IO::Warning("No method specified, using symmetric Newton.\n");
            method = CONSTRAINTS_METHOD_SNIP;
        }
        else if (strcasecmp(type, "snip") == 0)
        {
            IO::Info("Using symmetric Newton to solve constraints.\n");
            method = CONSTRAINTS_METHOD_SNIP;
        }
        else if (strcasecmp(type, "rattle") == 0)
        {
            IO::Info("Use rattle method to solve constraints.\n");
            method = CONSTRAINTS_METHOD_RATTLE;
        }
        else
        {
            IO::Warning("Your choice of constraint method (%s) is not valid. "
                        "Will use symmetric Newton.\n",
                        type);
            method = CONSTRAINTS_METHOD_SNIP;
        }
    }

    if (constraints != CONSTRAINTS_OFF && !i->SupportsConstraints())
    {
        // Warn if this integrator does not support constraints
        IO::Warning(
            "Constraints are not supported for the selected integrator. "
            "Constraints will be turned off.\n");
        constraints = CONSTRAINTS_OFF;
    }

    // below: set tolerance value for norm(Delta_lambda) (lambda has units of
    // mass)
    TOL_lambda = TOL * TOL * system->AMU() * system->AMU();
    // below: set pointer to array for storing 3N particle positions to NULL
    stored_positions = NULL;
    iter_pos = NULL;
    iter_vel = NULL;
    // below: set pointer to array for storing a list of constrained bonds
    constraint_list = NULL;
    topology_list = NULL;
    n_constr = 0;
    // initialise shake and rattle iteration counts. Initialise n_calls.
    pos_iteration_count = 0;
    vel_iteration_count = 0;
    n_calls = 0;
    R_ready = false;
    on_constraint = false;
    rebuild_R = true;
}

/**
 * Free all the memory allocated for storing positions, lists of constraints
 * etc.
 */
ConstraintSolver::~ConstraintSolver()
{
    if (stored_positions != NULL)
    {
        delete[] stored_positions;
        stored_positions = NULL;
    }
    if (iter_pos != NULL)
    {
        delete[] iter_pos;
        iter_pos = NULL;
    }
    if (iter_vel != NULL)
    {
        delete[] iter_vel;
        iter_vel = NULL;
    }
    if (constraint_list != NULL)
    {
        delete[] constraint_list;
        constraint_list = NULL;
    }
    if (topology_list != NULL)
    {
        delete[] topology_list;
        topology_list = NULL;
    }
}

/**
 * This function is called the first time 'StorePositions' is called, and
 * initializes
 * everything that can't be initialized in the constructor.
 */
void ConstraintSolver::Initializer()
{
    // The initializer should only be called once, if not, do nothing

    if (constraint_list != NULL)
        return;

    IO::Info("PBC are %s\n", (system->HasPBC() ? "on" : "off"));
    stored_positions = new double[3 * system->GetNumParticles()];
    iter_pos = new double[3 * system->GetNumParticles()];
    iter_vel = new double[3 * system->GetNumParticles()];

    // Count and report the number of constrained bonds.
    Bond bond, bond2;
    if (constraints == CONSTRAINTS_H_BONDS_ONLY)
    {
        for (int i = 0; i < system->GetNumBonds(); i++)
        {
            bond = system->GetBond(i);
            if (bond.hydrogen == true)
                n_constr += 1;
        }
    }
    else
    {
        n_constr = system->GetNumBonds();
    }

    IO::Info("Number of bond constraints: %d\n", n_constr);

    // Construct list of constrained bonds.
    constraint_list = new int[n_constr];
    int k = 0; // current constraint index
    for (int i = 0; i < system->GetNumBonds(); i++)
    {
        bond = system->GetBond(i);
        if (constraints == CONSTRAINTS_H_BONDS_ONLY)
        {
            if (bond.hydrogen == true)
            {
                constraint_list[k] = i;
                k += 1;
            }
        }
        else
        {
            constraint_list[i] = i;
        }
    }

    // Construct the list of pairs of bonds which share atoms, which will be
    // used to build the sparse R matrix for SNIP.
    if (method == CONSTRAINTS_METHOD_SNIP)
    {
        // resize sparse constraint matrix R to correct size.
        // Rows and columns of R index the constraints
        R.resize(n_constr, n_constr);

        // Count the number (i, j>i) pairs of constrained bonds which
        // share an atom
        n_list = 0;
        for (int i = 0; i < n_constr; i++)
        {
            bond = system->GetBond(constraint_list[i]);
            for (int j = (i + 1); j < n_constr; j++)
            {
                bond2 = system->GetBond(constraint_list[j]);
                // Check if bond and bond2 have common atom
                if (bond.a == bond2.a || bond.a == bond2.b ||
                    bond.b == bond2.a || bond.b == bond2.b)
                {
                    n_list++;
                }
            }
        }

        // Allocate space for the topology list and assemble
        topology_list = new int[n_list * 5];

        // List stores 5-tuples of ints, k = [0..n_list-1]:
        // 5*k   -> constraint index i (between atoms a-b)
        // 5*k+1 -> constraint index j>i (between atoms a-c)
        // 5*k+2 -> atom index a (the shared atom)
        // 5*k+3 -> atom index b (other end of constraint i)
        // 5*k+4 -> atom index c (other end of constraint j)
        k = 0;
        for (int i = 0; i < n_constr; i++)
        {
            bond = system->GetBond(constraint_list[i]);
            for (int j = (i + 1); j < n_constr; j++)
            {
                bond2 = system->GetBond(constraint_list[j]);
                // Check if bond and bond2 have common atom,
                // if so, add it to the list.
                if (bond.a == bond2.a)
                {
                    topology_list[5 * k] = i;
                    topology_list[5 * k + 1] = j;
                    topology_list[5 * k + 2] = bond.a;
                    topology_list[5 * k + 3] = bond.b;
                    topology_list[5 * k + 4] = bond2.b;
                    k++;
                }
                else if (bond.a == bond2.b)
                {
                    topology_list[5 * k] = i;
                    topology_list[5 * k + 1] = j;
                    topology_list[5 * k + 2] = bond.a;
                    topology_list[5 * k + 3] = bond.b;
                    topology_list[5 * k + 4] = bond2.a;
                    k++;
                }
                else if (bond.b == bond2.a)
                {
                    topology_list[5 * k] = i;
                    topology_list[5 * k + 1] = j;
                    topology_list[5 * k + 2] = bond.b;
                    topology_list[5 * k + 3] = bond.a;
                    topology_list[5 * k + 4] = bond2.b;
                    k++;
                }
                else if (bond.b == bond2.b)
                {
                    topology_list[5 * k] = i;
                    topology_list[5 * k + 1] = j;
                    topology_list[5 * k + 2] = bond.b;
                    topology_list[5 * k + 3] = bond.a;
                    topology_list[5 * k + 4] = bond2.a;
                    k++;
                }
            }
        }
    }
}

/**
 * Stores current particle positions in array 'stored_positions'. This is
 * necessary
 * since ResolveConstraints() is called after the unconstrained timestep is
 * applied,
 * but it needs the positions before it was applied as well.
 */
void ConstraintSolver::StorePositions()
{
    Vector3 q, v;
    // only do something if constraints aren't off
    if (constraints != CONSTRAINTS_OFF)
    {
        Initializer();

//*** take a copy of particle positions and store them ***
#pragma omp parallel for default(none) private(q)
        for (int i = 0; i < system->GetNumParticles(); i++)
        {
            q = system->GetPosition(i);
            stored_positions[3 * i] = q.x;
            stored_positions[3 * i + 1] = q.y;
            stored_positions[3 * i + 2] = q.z;
        }
    }
}

/**
 * Wrapper function for accessing old position of particle i
 */
Vector3 ConstraintSolver::GetOldPosition(int i)
{
    return Vector3(stored_positions[3 * i], stored_positions[3 * i + 1],
                   stored_positions[3 * i + 2]);
}

/**
 * Wrapper function for accessing iterated position of particle i
 */
Vector3 ConstraintSolver::GetIterPosition(int i)
{
    return Vector3(iter_pos[3 * i], iter_pos[3 * i + 1], iter_pos[3 * i + 2]);
}

/**
 * Wrapper function for accessing iterated velocity of particle i
 */
Vector3 ConstraintSolver::GetIterVelocity(int i)
{
    return Vector3(iter_vel[3 * i], iter_vel[3 * i + 1], iter_vel[3 * i + 2]);
}

/**
 * Wrapper function for modifying iterated position of particle i
 */
void ConstraintSolver::SetIterPosition(int i, Vector3 Q)
{
    iter_pos[3 * i] = Q.x;
    iter_pos[3 * i + 1] = Q.y;
    iter_pos[3 * i + 2] = Q.z;
}

/**
 * Wrapper function for modifying iterated velocity of particle i
 */
void ConstraintSolver::SetIterVelocity(int i, Vector3 V)
{
    iter_vel[3 * i] = V.x;
    iter_vel[3 * i + 1] = V.y;
    iter_vel[3 * i + 2] = V.z;
}

/**
 * Builds and factorises the sparse matrix R.
 */
void ConstraintSolver::BuildSparseR()
{
    Vector3 Qab, Qa, Qb;
    double value, inv_ma, inv_mb;
    int row_index, col_index;
    Bond bond;
    static std::vector<T> tripletList(n_list + n_constr);

    // build off-diagonal part of R (at the beginning of every timestep)
#pragma omp parallel for default(none) private(                                \
    row_index, col_index, inv_ma, Qab, Qa, Qb, value) shared(tripletList)
    for (int i = 0; i < n_list; i++)
    {
        row_index = topology_list[5 * i];
        col_index = topology_list[5 * i + 1];
        inv_ma = system->GetInverseMass(topology_list[5 * i + 2]);
        Qab = system->GetPosition(topology_list[5 * i + 2]);
        Qa = system->GetPosition(topology_list[5 * i + 3]);
        Qb = system->GetPosition(topology_list[5 * i + 4]);
        value = inv_ma * Vector3::Mult(system->MinimumImageDiff(Qab, Qa),
                                       system->MinimumImageDiff(Qab, Qb));
        tripletList[i] = T(row_index, col_index, value);
    }

    // build diagonal part of R (first time only - doesn't depend on positions)
    if (R_ready == false)
    {
#pragma omp parallel for default(none) private(bond, inv_ma, inv_mb, value)    \
    shared(tripletList)

        for (int i = 0; i < n_constr; i++)
        {
            bond = system->GetBond(constraint_list[i]);
            inv_ma = system->GetInverseMass(bond.a);
            inv_mb = system->GetInverseMass(bond.b);
            value = (inv_ma + inv_mb) * bond.rab * bond.rab;
            tripletList[i + n_list] = T(i, i, value);
        }
    }

    R.setFromTriplets(tripletList.begin(), tripletList.end());

    if (R_ready == false)
    {
        // Only need to do the symbolic decomposition once
        // since the structure doesn't change!
        solver.analyzePattern(R);
        R_ready = true;
    }

    solver.factorize(R); // factorize R
    rebuild_R = false;
}

/**
 * Resolve position constraints. Call down to method specified.
 */
int ConstraintSolver::ResolvePositionConstraints(double dt)
{
    int rc = 0;
    // If no Constraints requested, nothing needs to be done
    if (constraints != CONSTRAINTS_OFF)
    {
        Initializer();

        if (method == CONSTRAINTS_METHOD_RATTLE)
        {
            //*** call Rattle to resolve constraints ***
            rc = Rattle_pos(dt);
        }
        else if (method == CONSTRAINTS_METHOD_SNIP)
        {
            // If we are not on the constraint surface, we can't build R.
            // Call RATTLE instead.
            if (!on_constraint)
            {
                rc = Rattle_pos(dt);

                // the RATTLE step puts us on constraint manifold
                on_constraint = true;
            }
            else
            {
                if (rebuild_R) // check if we need to rebuild R
                {
                    BuildSparseR();
                }

                // call SNIP to resolve constraints
                rc = SNIP_pos(dt);
            }
            // we moved the positions in this step, so next
            // time we need to rebuild R
            rebuild_R = true;
        }
    }

    if (rc != 0)
    {
        IO::WarningAll("Constraint solver could not converge %d bonds!\n", rc);
    }

    return rc;
}

/**
 * Resolve velocity constraints. Call down to method specified.
 */
int ConstraintSolver::ResolveVelocityConstraints(double dt)
{
    int rc = 0;
    // If no Constraints requested, nothing needs to be done
    if (constraints != CONSTRAINTS_OFF)
    {
        n_calls += 1;

        Initializer();

        if (method == CONSTRAINTS_METHOD_RATTLE)
        {
            //*** call Rattle to resolve constraints ***
            rc = Rattle_vel(dt);
        }
        else if (method == CONSTRAINTS_METHOD_SNIP)
        {
            // If we are not on the constraint surface, we can't build R.
            // Call RATTLE instead.
            if (!on_constraint)
            {
                rc = Rattle_vel(dt);
            }
            else
            {
                if (rebuild_R) // check if we need to rebuild R
                {
                    BuildSparseR();
                }

                // call SNIP to resolve constraints
                rc = SNIP_vel(dt);
            }
        }
    }

    if (rc != 0)
    {
        IO::WarningAll("Constraint solver could not converge %d bonds!\n", rc);
    }

    return rc;
}

/**
 * Solve position constraints with Symmetric Newton iteration (SNIP)
 */
int ConstraintSolver::SNIP_pos(double dt)
{
    //**** variable declarations ****
    int k;
    int check = 0; // checks if constraints are satisfied. After iteratngi
    // through all the constraints, check is the number of constraints
    // that are not satisfied.
    Vector3 Qa, Qb, Qab, qa, qb;
    Eigen::VectorXd b(n_constr);
    Eigen::VectorXd lambda(n_constr);
    double inv_ma, inv_mb, dab;
    Bond bond;
//*** end of variable declarations ***
//*** update the particle positions we iterate over.
#pragma omp parallel for default(none)
    for (int i = 0; i < system->GetNumParticles(); i++)
    {
        SetIterPosition(i, system->GetPosition(i));
    }
    //***** start Newton iteration *****
    for (k = 0; k < Num_iter_max; k++) // outer iterates
    {
        check = 0;
// *** fill b and check if constraints are satisfied ***
#pragma omp parallel for default(none) private(bond, Qa, Qb, Qab, dab) shared( \
    b) reduction(+ : check)
        for (int i = 0; i < n_constr; i++) // iterates through constraints
        {
            bond = system->GetBond(constraint_list[i]);
            // below: We read out the atom positions and check if the constraint
            // is satisfied.
            Qa = GetIterPosition(bond.a);
            Qb = GetIterPosition(bond.b);
            Qab = system->MinimumImageDiff(Qa, Qb);
            dab = bond.rab * bond.rab;
            b[i] = 0.5 * (Vector3::Mult(Qab, Qab) - dab);
            if (std::abs(b[i]) > TOL * dab)
                check++;
        }
        // *** solve for lagrange multiplier updates ***
        lambda = solver.solve(b);
        // check for tolerance
        if ((lambda.dot(lambda) < TOL_lambda) && (check == 0))
            break;
//*** update Q's ***
#pragma omp parallel for default(none) private(bond, Qa, Qb, inv_ma, inv_mb,   \
                                               qa, qb, Qab) shared(lambda)
        for (int i = 0; i < n_constr; i++) // iterates through constraints
        {
            bond = system->GetBond(constraint_list[i]);
            Qa = GetIterPosition(bond.a);
            Qb = GetIterPosition(bond.b);
            inv_ma = system->GetInverseMass(bond.a);
            inv_mb = system->GetInverseMass(bond.b);
            qa = GetOldPosition(bond.a);
            qb = GetOldPosition(bond.b);
            Qab = system->MinimumImageDiff(qa, qb);
            // update positions of particle a
            inv_ma = inv_ma * lambda[i];
            SetIterPosition(bond.a, Qa - Vector3::Scale(inv_ma, Qab));
            // update positions of particle b
            inv_mb = inv_mb * lambda[i];
            SetIterPosition(bond.b, Qb + Vector3::Scale(inv_mb, Qab));
        }
    }
// Set particle positions & half-step velocities.
#pragma omp parallel for default(none) private(Qa, qa) shared(dt)
    for (int i = 0; i < system->GetNumParticles(); i++)
    {
        Qa = GetIterPosition(i);
        qa = GetOldPosition(i);
        system->SetVelocity(i, Vector3::Scale(1 / dt, (Qa - qa)));
        system->SetPosition(i, Qa);
    }
    // Increase Position iteration count
    pos_iteration_count += k;

    return check;
}

/**
 * Companion of SNIP_pos. Solves velocity constraints.
 */
int ConstraintSolver::SNIP_vel(double dt)
{
    //**** variable declarations ****
    Vector3 Qa, Qb, Qab, Va, Vb;
    Eigen::VectorXd b(n_constr);
    Eigen::VectorXd mu(n_constr);
    double inv_ma, inv_mb;
    Bond bond;
//*** end of variable declarations ***
// *** fill b ***
#pragma omp parallel for default(none) private(bond, Qa, Qb, Va, Vb) shared(b)
    for (int i = 0; i < n_constr; i++) // iterates through constraints
    {
        bond = system->GetBond(constraint_list[i]);
        // below: We read out the atom positions and check if the constraint
        // is satisfied.
        Qa = system->GetPosition(bond.a);
        Qb = system->GetPosition(bond.b);
        Va = system->GetVelocity(bond.a);
        Vb = system->GetVelocity(bond.b);
        b[i] = Vector3::Mult(system->MinimumImageDiff(Qa, Qb), Va - Vb);
    }
    // *** solve for lagrange multiplier updates ***
    mu = solver.solve(b);
//*** update V's ***
#pragma omp parallel for default(none) private(bond, Qa, Qb, Va, Vb, inv_ma,   \
                                               inv_mb, Qab) shared(mu)
    for (int i = 0; i < n_constr; i++) // iterates through constraints
    {
        bond = system->GetBond(constraint_list[i]);
        Qa = system->GetPosition(bond.a);
        Qb = system->GetPosition(bond.b);
        Va = system->GetVelocity(bond.a);
        Vb = system->GetVelocity(bond.b);
        inv_ma = system->GetInverseMass(bond.a);
        inv_mb = system->GetInverseMass(bond.b);
        Qab = system->MinimumImageDiff(Qa, Qb);
        // update velocities of particle a
        Va = Va - Vector3::Scale(inv_ma * mu[i], Qab);
        system->SetVelocity(bond.a, Va);
        // update velocities of particle b
        Vb = Vb + Vector3::Scale(inv_mb * mu[i], Qab);
        system->SetVelocity(bond.b, Vb);
    }
    // Increase Velocity iteration count
    vel_iteration_count += 1;

    return 0;
}

/**
 * Depending on the level of constraints requested by the user, set up and solve
 * them
 */
int ConstraintSolver::Rattle_pos(double dt)
{
    //**** variable declarations ****
    int k;
    int check = 0; // checks if constraints are satisfied. After iterating
    // through all the constraints, check is the number of constraints
    // that are not satisfied
    Vector3 Qa, Qb, qa, qb, Qab, qab_old, Va, Vb;
    double inv_ma, inv_mb, g_i, dab, lambda;
    Bond bond;
//*** update the particle positions we iterate over. ***
#pragma omp parallel for default(none)
    for (int i = 0; i < system->GetNumParticles(); i++)
    {
        SetIterPosition(i, system->GetPosition(i));
    }
    // **** RATTLE iteration ****
    for (k = 0; k < Num_iter_max; k++) // outer iterates
    {
        check = 0;
#pragma omp parallel for default(none) private(                                \
    bond, Qa, Qb, Qab, dab, g_i, inv_ma, inv_mb, qa, qb, qab_old, lambda)      \
        reduction(+ : check)
        for (int i = 0; i < n_constr; i++) // iterates through constraints
        {
            bond = system->GetBond(constraint_list[i]);
            // below: We read out the atom positions and check if the constraint
            // is satisfied.
            Qa = GetIterPosition(bond.a);
            Qb = GetIterPosition(bond.b);
            Qab = system->MinimumImageDiff(Qa, Qb);
            dab = bond.rab * bond.rab;
            g_i = Vector3::Mult(Qab, Qab) - dab;
            if (std::abs(g_i) > TOL * dab) // check if constraint i is satisfied
            {
                check++; // at least one constraint is not satisfied, so
                         // we need
                // to keep iterating.
                // below: constraint resolution
                inv_ma = system->GetInverseMass(bond.a);
                inv_mb = system->GetInverseMass(bond.b);
                qa = GetOldPosition(bond.a);
                qb = GetOldPosition(bond.b);
                qab_old = system->MinimumImageDiff(qa, qb);
                // compute lagrange multiplier
                lambda =
                    g_i / (2 * (inv_ma + inv_mb) * Vector3::Mult(Qab, qab_old));
                // update positions of particle a
                inv_ma = inv_ma * lambda;
                SetIterPosition(bond.a, Qa - Vector3::Scale(inv_ma, qab_old));
                // update positions of particle b
                inv_mb = inv_mb * lambda;
                SetIterPosition(bond.b, Qb + Vector3::Scale(inv_mb, qab_old));
            }
        }
        // check if tolerance criterion was satisfied for all constraints,
        // if yes we end the SHAKE iteration.
        if (check == 0)
            break;
    }
// Set particle positions & half-step velocities.
#pragma omp parallel for default(none) private(Qa, qa) shared(dt)
    for (int i = 0; i < system->GetNumParticles(); i++)
    {
        Qa = GetIterPosition(i);
        qa = GetOldPosition(i);
        system->SetVelocity(i, Vector3::Scale(1 / dt, (Qa - qa)));
        system->SetPosition(i, Qa);
    }
    // Increase Position iteration count
    pos_iteration_count += k;

    return check;
}

int ConstraintSolver::Rattle_vel(double dt)
{
    //**** variable declarations ****
    int k;
    int check = 0; // checks if constraints are satisfied. After iterating
    // through all the constraints, check is the number of constraints
    // that are not satisfied.
    Vector3 Qa, Qb, Qab, Va, Vb;
    double inv_ma, inv_mb, dg_i, dab, mu;
    Bond bond;
//*** update the particle velocities we iterate over.
#pragma omp parallel for default(none)
    for (int i = 0; i < system->GetNumParticles(); i++)
    {
        SetIterVelocity(i, system->GetVelocity(i));
    }
    // **** RATTLE iteration ****
    for (k = 0; k < Num_iter_max; k++) // outer iterates
    {
        check = 0;
#pragma omp parallel for default(none) private(                                \
    bond, Qa, Qb, Va, Vb, Qab, dab, dg_i, inv_ma, inv_mb, mu) shared(dt)       \
        reduction(+ : check)
        for (int i = 0; i < n_constr; i++) // iterates through constraints
        {
            bond = system->GetBond(constraint_list[i]);
            // below: We read out the atom positions and check if the constraint
            // is satisfied.
            Qa = system->GetPosition(bond.a);
            Qb = system->GetPosition(bond.b);
            Va = GetIterVelocity(bond.a);
            Vb = GetIterVelocity(bond.b);
            Qab = system->MinimumImageDiff(Qa, Qb);
            dab = bond.rab * bond.rab;
            dg_i = Vector3::Mult(Qab, Va - Vb);
            if (dg_i * dt > TOL * dab) // check if constraint i is satisfied
            //(note that dg_i*dt/dab is dimensionless)
            {
                check++; // at least one constraint is not satisfied, so
                         // we need
                // to keep iterating.
                // below: constraint resolution
                inv_ma = system->GetInverseMass(bond.a);
                inv_mb = system->GetInverseMass(bond.b);
                // compute lagrange multiplier
                mu = dg_i / ((inv_ma + inv_mb) * dab);
                // update velocity of particle a
                inv_ma = inv_ma * mu;
                SetIterVelocity(bond.a, Va - Vector3::Scale(inv_ma, Qab));
                // update velocity of particle b
                inv_mb = inv_mb * mu;
                SetIterVelocity(bond.b, Vb + Vector3::Scale(inv_mb, Qab));
            }
        }
        // check if tolerance criterion was satisfied for all constraints,
        // if yes we end the RATTLE iteration.
        if (check == 0)
            break;
    }
// Set particle velocities.
#pragma omp parallel for default(none)
    for (int i = 0; i < system->GetNumParticles(); i++)
    {
        system->SetVelocity(i, GetIterVelocity(i));
    }
    // Increase Velocity iteration count
    vel_iteration_count += k;

    return check;
}

void ConstraintSolver::WriteCounts(int n_rattlePerStep, int write_freq)
{
    // variable declarations
    double m;
    Vector3 V;
    static std::ofstream outfile;
    // File initialization
    if (constraints != CONSTRAINTS_OFF)
    {
        if (n_calls == n_rattlePerStep)
            outfile.open("counts.dat");
        // write iteration counts, kinetic and potential energy to file
        // count.dat
        if (n_calls % (write_freq * n_rattlePerStep) == 0)
        {
            double pe = system->GetPotentialEnergy();
            double Te = 0;
            for (int i = 0; i < system->GetNumParticles(); i++)
            {
                m = system->GetMass(i);
                V = system->GetVelocity(i);
                Te += 0.5 * m * Vector3::Mult(V, V);
            }
            outfile << n_calls / n_rattlePerStep << "\t" << pos_iteration_count
                    << "\t" << vel_iteration_count << "\t" << pe << "\t" << Te
                    << std::endl;
            outfile.flush();
        }
    }
}

bool ConstraintSolver::IsConstraintsOn() const
{
    if (constraints == CONSTRAINTS_OFF)
    {
        return false;
    }
    else
    {
        return true;
    }
}

int ConstraintSolver::GetNumConstraints() const
{
    int n = 0;
    if (constraints == CONSTRAINTS_H_BONDS_ONLY)
    {
        for (int i = 0; i < system->GetNumBonds(); i++)
        {
            if (system->GetBond(i).hydrogen)
                n++;
        }
    }
    else if (constraints == CONSTRAINTS_ALL_BONDS)
    {
        n = system->GetNumBonds();
    }

    return n;
}
