/*******************************************************************************
*                                                                              *
*             MIST - Molecular Integration Simulation Toolkit                  *
*         A library of integration algorithms for Molecular Dynamics           *
*                                                                              *
*            Copyright (c) 2014-2020, The University of Edinburgh              *
*                            All rights reserved.                              *
*                     List of contributors in AUTHORS                          *
*                                                                              *
*            MIST is free software.  See LICENSE for details                   *
*                                                                              *
*******************************************************************************/

/**
 * @file GromacsSystem.h
 * @brief Declaration of specific system class for Gromacs
 */

#ifndef MIST_GROMACS_SYSTEM_H
#define MIST_GROMACS_SYSTEM_H
#include "System.h"
#include "gromacs/legacyheaders/types/mist_forcecallback.h"

/**
 * @brief A System class which provides accessors for particle data inside the
 * Gromacs code
 *
 * This is a specialisation of the System interface which is specific to the
 * Gromacs code.  It provides access to an Integrator to the particle data
 * stored in various arrays within Gromacs, and also the ability to compute
 * updates forces, calling Gromacs force evaluation function by means of a
 * callback.
 *
 */
class GromacsSystem : public System
{
  public:
    /**
     * @brief Default Constructor
     *
     * Sets the Gromacs-specific unit conversion factors:
     * - Energy in kJ/mol
     * - Time in ps
     * - Mass in AMU
     * - Length in Angstrom
     *
     */
    GromacsSystem();

    /**
     * @brief Default Destructor
     *
     * Does nothing
     */
    ~GromacsSystem();

    // Interface to Integrators
    Vector3 GetVelocity(int i) const;
    void SetVelocity(int i, Vector3 v);
    Vector3 GetPosition(int i) const;
    void SetPosition(int i, Vector3 p);
    void SetMassPtr(void *m, int req);
    void GetCell(Vector3 *a, Vector3 *b, Vector3 *c);
    void GetCellInverse(Vector3 *a, Vector3 *b, Vector3 *c);
    void SetCell(Vector3 a, Vector3 b, Vector3 c);

    Vector3 GetForce(int i) const;
    Vector3 GetForce(int i, force_component_t t) const;

    double GetMass(int i) const;
    double GetInverseMass(int i) const;
    double GetInverseSqrtMass(int i) const;
    double GetPressure() const;
    double GetPotentialEnergy() const;
    double GetPotentialEnergy(force_component_t t) const;
    /** @copydoc System::GetKind() */
    char *GetKind(int i) const;

  protected:
    Vector3
        cell_inv[3]; /**< @brief Inverse of the box matrix (column vectors) */
};

#endif
