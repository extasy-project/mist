/*******************************************************************************
*                                                                              *
*             MIST - Molecular Integration Simulation Toolkit                  *
*         A library of integration algorithms for Molecular Dynamics           *
*                                                                              *
*            Copyright (c) 2014-2020, The University of Edinburgh              *
*                            All rights reserved.                              *
*                     List of contributors in AUTHORS                          *
*                                                                              *
*            MIST is free software.  See LICENSE for details                   *
*                                                                              *
*******************************************************************************/

/**
 * @file IO.h
 * @brief Declaration of a class for IO
 */

#ifndef MIST_IO_H
#define MIST_IO_H

#include <stdbool.h>
#include <stdio.h>

#ifdef __MIST_WITH_MPI
#include <mpi.h>
#endif

/**
 * @brief Class for printing messages to stdout/err from within MIST
 *
 * Prints information, warning and error messages to stdout or stderr,
 * either on the master only or from all ranks.
 */
class IO
{
  public:
    /**
     * @brief Write an information message on the master only
     * @param fmt a printf-style format string
     */
    static void Info(const char *fmt, ...);

    /**
     * @brief Write an information message on all ranks
     * @param fmt a printf-style format string
     */
    static void InfoAll(const char *fmt, ...);

    /**
     * @brief Write a warning message on the master only
     * @param fmt a printf-style format string
     */
    static void Warning(const char *fmt, ...);

    /**
     * @brief Write a warning message on all ranks
     * @param fmt a printf-style format string
     */
    static void WarningAll(const char *fmt, ...);

    /**
     * @brief Write an error message (always all ranks)
     * @param fmt a printf-style format string
     */
    static void Error(const char *fmt, ...);
#ifdef __MIST_WITH_MPI

    /**
     * @brief Store a communicator used to identify ranks
     * @param c the communicator
     */
    static void SetCommunicator(MPI_Comm c);

    /**
     * @brief Get the communicator used to identify ranks
     * @return the communicator
     */
    static MPI_Comm GetCommunicator();
#endif

  protected:
    /**
     * @brief Format and write the a message to the provided file pointer
     *
     * @param f the file to output to
     * @param master_only write message from master rank only
     * @param prefix string to prefix the message with
     * @param fmt a printf-style format string
     * @param args the list of arguments to format for output
     */
    static void WriteMessage(FILE *f, bool master_only, const char *prefix,
                             const char *fmt, va_list args);
#ifdef __MIST_WITH_MPI
    static MPI_Comm
        communicator; /**< @brief the communicator used to identify ranks */
#endif
};

#endif
