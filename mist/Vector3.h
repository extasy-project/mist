/*******************************************************************************
*                                                                              *
*             MIST - Molecular Integration Simulation Toolkit                  *
*         A library of integration algorithms for Molecular Dynamics           *
*                                                                              *
*            Copyright (c) 2014-2020, The University of Edinburgh              *
*                            All rights reserved.                              *
*                     List of contributors in AUTHORS                          *
*                                                                              *
*            MIST is free software.  See LICENSE for details                   *
*                                                                              *
*******************************************************************************/

/**
 * @file Vector3.h
 * @brief Declaration of a lightweight 3D vector class
 *
 */

#ifndef MIST_VECTOR3_H
#define MIST_VECTOR3_H

/**
 * @brief A lightweight 3D vector class
 *
 * A 3D double-precision vector class.  Currently no additional operations are
 * supported.
 * Direct access to the members x,y,z is permitted.
 */
class Vector3
{
  public:
    /**
     * @brief Default Constructor
     *
     * Creates and zeros a vector
     */
    Vector3() : x(0.0), y(0.0), z(0.0){};

    /**
     * @brief Constructor with specified entries
     *
     * Creates a vector and sets it according to the provided x,y,z values
     *
     * @param x 1st entry in the vector
     * @param y 2nd entry in the vector
     * @param z 3rd entry in the vector
     */
    Vector3(double x, double y, double z) : x(x), y(y), z(z){};

    double x; /**< @brief 1st component of the vector, publically accessible */
    double y; /**< @brief 2nd component of the vector, publically accessible */
    double z; /**< @brief 3rd component of the vector, publically accessible */

    /**
     * @brief Element-wise addition of two vectors
     *
     * Given two vectors v and w returns v + w
     *
     * @param v 1st vector
     * @param w 2nd vector
     *
     * @return sum of the two vectors
     */
    static inline Vector3 Add(const Vector3 &v, const Vector3 &w)
    {
        return Vector3(v.x + w.x, v.y + w.y, v.z + w.z);
    }

    /**
     * @brief Element-wise subtraction of two vectors
     *
     * Given two vectors v and w returns v - w
     *
     * @param v 1st vector
     * @param w 2nd vector
     *
     * @return difference of the two vectors
     */
    static inline Vector3 Sub(const Vector3 &v, const Vector3 &w)
    {
        return Vector3(v.x - w.x, v.y - w.y, v.z - w.z);
    }

    /**
     * @brief Dot (inner) product of two vectors
     *
     * Given two vectors v and w returns u = v . w
     *
     * @param v 1st vector
     * @param w 2nd vector
     *
     * @return dot product of the two vectors
     */
    static inline double Mult(const Vector3 &v, const Vector3 &w)
    {
        return (v.x * w.x + v.y * w.y + v.z * w.z);
    }

    /**
     * @brief Cross product of two vectors
     *
     * Given two vectors v and w returns u = v x w
     *
     * @param v 1st vector
     * @param w 2nd vector
     *
     * @return dot product of the two vectors
     */
    static inline Vector3 Cross(const Vector3 &v, const Vector3 &w)
    {
        return Vector3(v.y * w.z - v.z * w.y, v.z * w.x - v.x * w.z,
                       v.x * w.y - v.y * w.x);
    }

    /**
     * @brief Product of a vector with a scalar
     *
     * Given a vector v and scalar a returns u = a * v
     *
     * @param a scalar
     * @param v vector
     *
     * @return the scaled vector
     */
    static inline Vector3 Scale(const double a, const Vector3 &v)
    {
        return Vector3(v.x * a, v.y * a, v.z * a);
    }

    /**
     * @brief Addition operator for vectors
     *
     * Add a vector v to the current vector, returning a new vector
     *
     * @param v the vector to be added
     *
     * @return the sum of v and the current vector
     */
    inline Vector3 operator+(const Vector3 &v)
    {
        return Vector3(this->x + v.x, this->y + v.y, this->z + v.z);
    }

    /**
     * @brief Subtraction operator for vectors
     *
     * Subtract a vector v from the current vector, returning a new vector
     *
     * @param v the vector to be subtracted
     *
     * @return the difference of v and the current vector
     */
    inline Vector3 operator-(const Vector3 &v)
    {
        return Vector3(this->x - v.x, this->y - v.y, this->z - v.z);
    }

    /**
     * @brief Scale operator for vectors
     *
     * Given a vector v and scalar a returns u = a * v
     *
     * @param a the scalar
     *
     * @return the current vector scaled by a
     */
    inline Vector3 operator*(const double a)
    {
        return Vector3(this->x * a, this->y * a, this->z * a);
    }

    /**
     * @brief Determinant of a 3 x 3 matrix
     *
     * Given three vectors u, v and w - the column vectors
     * of a 3x3 matrix, returns the determinant of the matrix
     *
     * @param u 1st vector
     * @param v 2nd vector
     * @param w 3rd vector
     *
     * @return determinant of the matrix
     */
    static inline double Det3x3(const Vector3 &u, const Vector3 &v, const Vector3 &w)
    {
        return u.x * (v.y * w.z - v.z * w.y) -
               v.x * (u.y * w.z - u.z * w.y) +
               w.x * (u.y * v.z - u.z * v.y);
    }
};
#endif
