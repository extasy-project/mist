# Integrator Templates

These template files can be used to add your own integrators to MIST. 
For instructions on how to do this please go to the [MIST wiki](https://bitbucket.org/extasy-project/mist/wiki/Adding%20Integrators%20to%20MIST).

