# From Template to RK4

[TOC]

Taking note of the things that need to be changed in order to 
convert an integrator template into an actual integration 
routine.

## Rename `MyIntegrator.h` to `RK4.h`

Edit RK4.h and change:

* Change: `MIST_MYINTEGRATOR` -> `MIST_RK4_H`
* Change: `MyIntegrator` -> `RK4Integrator`
* The Constructors and Destructor need to be renamed, i.e.:
    
```c++
     public:
       MyIntegrator(Param *p);
       virtual ~MyIntegrator();
```

   to become:
    
```c++
     public:
       RK4Integrator(Param *p);
       virtual ~RK4Integrator();
```

## Rename `MyIntegrator.cpp` to `RK4.cpp` 
Edit `RK4.cpp`:

* Change:
    
```C++
      #include “MyIntegrator.h"
```

   to

```C++
      #include “RK4.h"
```

* Change:
    
```C++
       MyIntegrator::MyIntegrator(Param *p) : Integrator(p)
       {
          std::cout << "MIST: Using MyIntegrator" << std::endl;
       }
```

   to
   
```c++
       RK4Integrator::RK4(Param *p) : Integrator(p)
      {
         std::cout << "MIST: RK4 integrator" << std::endl;
      }
```

* Rename the destructor:
    
```c++
         MyIntegrator::~MyIntegrator() {}
```

   to
      
```c++
         RK4Integrator::~RK4Integrator() {}
```

 * Change:
    
```c++
        void MyIntegrator::Step(double dt)
```

   to
      
```c++
        void RK4Integrator::Step(double dt)
```

## Implementing the algorithm

For the 4th order RK4 (from these
[slides](http://physics.bu.edu/~py502/slides/l07.pdf)) we have:

```
 k1 = dt*a(tn,xn)		  
 l1 = dt*vn			  
 k2 = dt*a(tn+dt/2,xn+(dt/2)*l1)	  
 l2 = dt*(vn+k1/2)		  
 k3 = dt*a(tn+dt/2,xn+(dt/2)*l2)   
 l3 = dt*(vn+k2/2)		  
 k4 = dt*a(tn+dt,xn+dt*k3)	  
 l4 = dt*(vn+k3)			  
                                   
 tn+1 = tn+dt			  
 xn+1 = xn + dt/6*(l1+2*l2+2*l3+l4)
 vn+1 = vn + dt/6*(k1+2*k2+2*k3+k4)

```

## Other changes
* Need to add integrator to the `mist.cpp` file in the `MIST_Init()` routine.


# Questions

* How do FEATURES work? Need to document on the wiki.
