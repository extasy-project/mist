/*******************************************************************************
*                                                                              *
*             MIST - Molecular Integration Simulation Toolkit                  *
*         A library of integration algorithms for Molecular Dynamics           *
*                                                                              *
*            Copyright (c) 2014-2020, The University of Edinburgh              *
*                            All rights reserved.                              *
*                     List of contributors in AUTHORS                          *
*                                                                              *
*            MIST is free software.  See LICENSE for details                   *
*                                                                              *
*******************************************************************************/

#include “MyIntegrator.h"
#include "System.h"
#include <iostream>

MyIntegrator::MyIntegrator(Param *p) : Integrator(p)
{
    std::cout << "MIST: Using MyIntegrator" << std::endl;
}

MyIntegrator::~MyIntegrator() {}

void MyIntegrator::Step(double dt)
{
    // Add in your integrator algorithm here

    // Ensure you update velocity, position, and use updated forces

    //VelocityStep(double dt);
    //VelocityStep updates the velocity using v(t+dt)=v(t)+a(t)*dt
    //To change your timestep size, pass VelocityStep fractions of dt (e.g. 0.5*dt)

    //PositionStep(double dt);
    //PositionStep updates the position using x(t+dt)=x(t)+v(t)*dt
    //To change your timestep size, pass PositionStep fractions of dt (e.g. 0.5*dt)

    //Compute and update your forces using the following:
    system->UpdateForces();

}
