/*******************************************************************************
*                                                                              *
*             MIST - Molecular Integration Simulation Toolkit                  *
*         A library of integration algorithms for Molecular Dynamics           *
*                                                                              *
*            Copyright (c) 2014-2020, The University of Edinburgh              *
*                            All rights reserved.                              *
*                     List of contributors in AUTHORS                          *
*                                                                              *
*            MIST is free software.  See LICENSE for details                   *
*                                                                              *
*******************************************************************************/

#ifndef MIST_MYINTEGRATOR_H
#define MIST_MYINTEGRATOR_H

#include "Integrator.h"

class MyIntegrator : public Integrator
{
  public:
    MyIntegrator(Param *p);
    virtual ~MyIntegrator();

    void Step(double dt);
};

#endif
