/*******************************************************************************
*                                                                              *
*             MIST - Molecular Integration Simulation Toolkit                  *
*         A library of integration algorithms for Molecular Dynamics           *
*                                                                              *
*            Copyright (c) 2014-2020, The University of Edinburgh              *
*                            All rights reserved.                              *
*                     List of contributors in AUTHORS                          *
*                                                                              *
*            MIST is free software.  See LICENSE for details                   *
*                                                                              *
*******************************************************************************/

Package Contents
================

* README - This file, containing general instructions
* LICENSE - BSD License for MIST
* AUTHORS - List of contributing developers
* VERSION - MIST library version
* configure, configure.ac, Makefile.in, Makefile.am, aclocal.m4, compile,
  config.h.in, depcomp, install-sh, missing, m4/ - configuration and build files
* bitbucket-pipelines.yml - definition of Continuous Integration pipeline
* mist/ - library source code
* tests/ - input files and scripts for testing MIST
* examples/ - example input files for use with each supported MD code
* templates/ - minimal Integrator class files as an example to developers
* doc/ - Doxygen documentation and diagrams
* eigen-3.2.10/ - sparse linear algebra library

Installation
============

MIST currently supports five Molecular Dynamics (MD) packages:

   * Amber 14
   * GROMACS 5.0.2
   * NAMD-Lite 2.0.3
   * LAMMPS
   * Forcite

The configure framework has been adopted to facilitate the production
of a Makefile that is suitable for the package that you are trying to
link MIST with. You will still have to compile specific parts of the
package so there will be instructions that vary according to which
MD package you are using. You MUST have the source code for the specific
MD package you will use MIST with available.

The MIST library is configured and linked with the particular MD
package you are using.  To generate a Makefile you will need to run
one of the following options:

    ./configure --with-amber=SRCPATH
    ./configure --with-gromacs=SRCPATH
    ./configure --with-namd_lite=SRCPATH
    ./configure --with-lammps=SRCPATH
    ./configure --with-forcite=SRCPATH

where SRCPATH is the path to the root directory of the package you wish
to use with MIST. You MUST specify the corresponding SRCPATH for the
package that you are trying to install and can only specify one option.

If the configure scripts finds the required dependencies it will
print out specific instructions on how to do the remainder of the
compilation for the MD package that you are trying to use.

Prerequisites for each supported package are described below.

NAMD-Lite
=========

Download and unpack source of NAMD-lite from http://www.ks.uiuc.edu/Development/MDTools/namdlite/:

  wget http://www.ks.uiuc.edu/Development/MDTools/namdlite/files/mdx_pre2.0.3.tar.gz
  tar -xzvf mdx_pre2.0.3.tar.gz

GROMACS
=======

Download and unpack source of gromacs-5.0.2.tar.gz from http://www.gromacs.org/Downloads:

  wget ftp://ftp.gromacs.org/pub/gromacs/gromacs-5.0.2.tar.gz
  tar -xzvf gromacs-5.0.2.tar.gz

Amber
=====

Download and unpack source of Amber (Amber requires a license, see http://ambermd.org/GetAmber.php)

  tar jxvf AmberTools15.tar.bz2
  tar jxvf Amber14.tar.bz2

LAMMPS
======

Obtain a version of LAMMPS containing the USER-MIST package, for example:

  git clone --single-branch --depth 1 -b mist https://github.com/ibethune/lammps.git

Forcite
=======

Forcite is available as part of the 3DS BIOVIA Materials Studio product
