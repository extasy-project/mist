# MIST test suite README


## Running the complete test suite

To run the complete test suite in this directory execute: 
```
     ./run_tests.sh
```
This will create a directory consisting of `test_` appended by the
current date and time, `test_$now`, into which everything from the
`builds` and `tests` directories is copied.

## Build MIST library and MD codes

Each subdirectory in the `builds` directory contains a `build.sh` script which makes
a copy of the MIST sources, builds the library and then the MD code is
linked with the MIST library. Each of these build scripts is executed
in turn. If a build fails, you will see: 
```
FAIL: Build failed
``` 
and the script will terminate.

## Test results against reference output

Each subdirectory in the `tests` directory contains a `run.sh` script, a set of input
files and reference output. `runs.sh` executes an MD simulation
using MIST and compares its output with the reference results. If a
comparison fails, you will get a:
```
FAIL: Test failed
``` 
message and the script will exit. If this happens to you please let us know.

## If all goes well

If all of the above finishes successfully without any fails, you
will see:
```
All done, tests successful!
```
and you are ready to procced.




